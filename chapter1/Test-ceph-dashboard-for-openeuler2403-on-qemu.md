# 操作手册：Test Ceph Dashboard for openEuler2403 on QEMU

---

## 1 目标

Ceph作为一种广泛应用于存储的分布式文件系统，在openEuler2309版本并未得到好的支持，在之前的实验中其Dashboard并不可用。

随着openEuler2403LTS版本的发布，本教程将验证openeuler riscv的系统环境是否达到运行Ceph的要求，Ceph Dashboard是否可用。

## 2 系统环境

参考[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part A）](./Test-kubernetes-for-openeuler-2403-on-qemu-a.md)

## 3 准备工作

考虑到安装ceph需要较大的磁盘空间，需要对现有的虚拟机镜像进行扩容。具体扩容方法可以参考[操作手册：Enable git-lfs support and image extension for openEuler2403 qemu image](./Enable-git-lfs-support-and-image-extension-for-openEuler2403-qemu-image.md)

考虑到后续ceph需要挂载osd，所以需要准备用于ceph挂载的img。

```bash
qemu-img create sd.img 512m
```

在start_vm_xfce_ueif.sh中加入如下两行，通过qemu加载sd.img

```bash
  -drive file=sd.img,format=raw,id=hd1,if=none
  -device virtio-blk-device,drive=hd1 
```

在start_vm_xfce_ueif.sh启动系统后，查看附加的磁盘。

```bash
[root@openeuler-riscv64 ~]# ls /dev/vdb
/dev/vdb
```

查看磁盘信息

```bash
[root@openeuler-riscv64 ~]# parted /dev/vdb
GNU Parted 3.5
Using /dev/vdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) print                                                            
Error: /dev/vdb: unrecognised disk label
Model: Virtio Block Device (virtblk)                                      
Disk /dev/vdb: 537MB
Sector size (logical/physical): 512B/512B
Partition Table: unknown
Disk Flags: 
(parted) quitr                 
......
(parted) quit
```

## 4 检查系统环境

查看ntp已安装和时间同步。

```bash
dnf list ntp
timedatectl 
```

## 5 安装与配置

### 5.1 安装

考虑到openeuler repo中没有ceph-deploy，无法自动化地完成 Ceph 集群的部署、升级、扩容、故障恢复等任务，只能手动部署。

使用ifconfig查询主机的IP地址，并解析主机名，openeuler-riscv64

```bash
vi /etc/hosts
```

加入

```bash
10.0.2.15 openeuler-riscv64
```

安装ceph

```bash
dnf install -y ceph ceph-radosgw
```

检查安装情况，查看版本为18.2.2，安装成功。

```bash
[root@openeuler-riscv64 ~]# ceph --version
ceph version 18.2.2 (531c0d11a1c5d39fbfe6aa8a521f023abf3bf3e2) reef (stable)
```

查看相关的软件包安装情况

```bash
[root@openeuler-riscv64 ~]# dnf list snappy lvm2
Last metadata expiration check: 0:15:05 ago on Mon 09 Dec 2024 10:40:54 AM CST.
Installed Packages
lvm2.riscv64                                             8:2.03.21-9.oe2403                                           @OS
snappy.riscv64                                           1.1.10-2.oe2403                                              @OS
```

### 5.2 配置

生成uuid

```bash
[root@openeuler-riscv64 ~]# uuidgen
97ccd2d1-43cd-4ee3-b89e-0d71229e5cd9
```

创建配置文件

```bash
sudo vim /etc/ceph/ceph.conf
```

加入以下内容。

```bash
[global]
fsid = 97ccd2d1-43cd-4ee3-b89e-0d71229e5cd9
mon_initial_members = openeuler-riscv64
mon_host = 10.0.2.15
public_network = 10.0.2.0/24
cluster network = 10.0.2.0/24
auth_cluster_required = cephx
auth_service_required = cephx
auth_client_required = cephx
osd_pool_default_size = 1
osd_pool_default_min_size = 1
osd_crush_chooseleaf_type = 0
```

创建mon的key信息（参考官网）

```bash
chmod 777 -R /tmp
sudo ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
sudo ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
sudo ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
sudo ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
sudo ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
sudo chown ceph:ceph /tmp/ceph.mon.keyring
```

创建mon的map信息

```bash
monmaptool --create --add openeuler-riscv64 10.0.2.15 --fsid 97ccd2d1-43cd-4ee3-b89e-0d71229e5cd9 /tmp/monmap
```

创建mon的目录信息

```bash
sudo mkdir /var/lib/ceph/mon/ceph-openeuler-riscv64
sudo chown -R ceph:ceph /var/lib/ceph/mon/ceph-openeuler-riscv64
chmod 777 -R /var/lib/ceph/mon/ceph-openeuler-riscv64
```

创建mon

```bash
sudo -u ceph ceph-mon --mkfs -i openeuler-riscv64 --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
```

## 6 测试ceph集群的mon

启动mon

```bash
sudo systemctl start ceph-mon@openeuler-riscv64
```

查看运行状态，看起来启动运行成功了。

```bash
sudo systemctl -l --no-page status ceph-mon@openeuler-riscv64
```

![Image](./c01.png)

设置开机启动

```bash
sudo systemctl enable ceph-mon@openeuler-riscv64
```

观察mon进程

```bash
ps axu|grep mon
```

查看ceph状态。

```bash
ceph -s
```

![Image](./c02.png)

我们可以看到服务已经启动了，openEuler2403原生的ceph包相较openEuler2309原生的ceph包有了进步，不用额外编辑ceph-mon@.service的内容了。

## 7 测试ceph集群的osd

检查lvm设备

```bash
[root@openeuler-riscv64 ~]# ceph-volume lvm list
No valid Ceph lvm devices found
```

采用SHORT FORM的方式，即BLUESTORE的方式，添加osd

```bash
sudo ceph-volume lvm create --data /dev/vdb
```

![Image](./c04.png)

再次检查lvm设备

```bash
[root@openeuler-riscv64 ~]# ceph-volume lvm list
====== osd.0 =======
  [block]       /dev/ceph-c9f99220-982c-4952-8008-9c42aaf1fec7/osd-block-09ccf287-c225-406a-bbe1-38197c671816
      block device              /dev/ceph-c9f99220-982c-4952-8008-9c42aaf1fec7/osd-block-09ccf287-c225-406a-bbe1-38197c671816
      block uuid                zhwAlc-QFta-c8vV-iuuj-LTLY-iGkx-0EwNKG
      cephx lockbox secret
      cluster fsid              97ccd2d1-43cd-4ee3-b89e-0d71229e5cd9
      cluster name              ceph
      crush device class
      encrypted                 0
      osd fsid                  09ccf287-c225-406a-bbe1-38197c671816
      osd id                    0
      osdspec affinity
      type                      block
      vdo                       0
      devices                   /dev/vdb
```

![Image](./c03.png)

查看ceph集群状态，发现osd已启动成功，即osd已经有1个up起来。这里openEuler2403原生的ceph包相较openEuler2309原生的ceph包有了进步，不用额外执行ceph-osd -i 0的命令了。

```bash
ceph -s
```

![Image](./c05.png)

## 8 测试ceph集群的mgr

去除1 monitors have not enabled msgr2的health警告

```bash
ceph mon enable-msgr2
ceph -s
```

![Image](./c06.png)

安装mgr，设置mgr节点。

查看mgr目录，为空

```bash
ls /var/lib/ceph/bootstrap-mgr/
ls /var/lib/ceph/mgr/
```

查看key的信息，所有角色的启动key都已准备好

```bash
[root@openeuler-riscv64 ~]# ceph auth list
osd.0
	key: AQDESFVn+9+WFhAAp0vVEbHkk98KqRtcsm9cSw==
	caps: [mgr] allow profile osd
	caps: [mon] allow profile osd
	caps: [osd] allow *
client.admin
	key: AQAKRFVngDdbKhAA1dusDCMz1JvOokZoTzhu0g==
	caps: [mds] allow *
	caps: [mgr] allow *
	caps: [mon] allow *
	caps: [osd] allow *
client.bootstrap-mds
	key: AQCVRFVn1WzkDxAA/KIW7fOnaquVJVFbBeAkEA==
	caps: [mon] allow profile bootstrap-mds
client.bootstrap-mgr
	key: AQCVRFVnwBblDxAARFPD0u2/3KePdpltqJZihg==
	caps: [mon] allow profile bootstrap-mgr
client.bootstrap-osd
	key: AQAURFVns8WKLBAAIrmp70+ct0v26pOL9tWQ1A==
	caps: [mgr] allow r
	caps: [mon] profile bootstrap-osd
client.bootstrap-rbd
	key: AQCVRFVnedXnDxAAFBqDtzR5T95rW62zLBdbVg==
	caps: [mon] allow profile bootstrap-rbd
client.bootstrap-rbd-mirror
	key: AQCVRFVn7XnoDxAAoHoQt7LGIfUrYGangFQ92A==
	caps: [mon] allow profile bootstrap-rbd-mirror
client.bootstrap-rgw
	key: AQCVRFVnaAzpDxAAe7t/4WqV3y6hHKo1Lj/e9w==
	caps: [mon] allow profile bootstrap-rgw
```

将key导入到mgr启动目录

```bash
ceph auth get client.bootstrap-mgr -o /var/lib/ceph/bootstrap-mgr/ceph.keyring
```

查看keyring

```bash
[root@openeuler-riscv64 ~]# cat /var/lib/ceph/bootstrap-mgr/ceph.keyring
[client.bootstrap-mgr]
	key = AQCVRFVnwBblDxAARFPD0u2/3KePdpltqJZihg==
	caps mon = "allow profile bootstrap-mgr"
```

建立mgr数据目录

```bash
mkdir /var/lib/ceph/mgr/ceph-openeuler-riscv64
```

建立对应的授权key文件

```bash
ceph auth get-or-create mgr.openeuler-riscv64 mon 'allow profile mgr' osd 'allow *' mds 'allow *'
```

查看key信息，可以看到mgr.openeuler-riscv64的key已添加上。

```bash
[root@openeuler-riscv64 ~]# ceph auth list
osd.0
	key: AQDESFVn+9+WFhAAp0vVEbHkk98KqRtcsm9cSw==
	caps: [mgr] allow profile osd
	caps: [mon] allow profile osd
	caps: [osd] allow *
client.admin
	key: AQAKRFVngDdbKhAA1dusDCMz1JvOokZoTzhu0g==
	caps: [mds] allow *
	caps: [mgr] allow *
	caps: [mon] allow *
	caps: [osd] allow *
client.bootstrap-mds
	key: AQCVRFVn1WzkDxAA/KIW7fOnaquVJVFbBeAkEA==
	caps: [mon] allow profile bootstrap-mds
client.bootstrap-mgr
	key: AQCVRFVnwBblDxAARFPD0u2/3KePdpltqJZihg==
	caps: [mon] allow profile bootstrap-mgr
client.bootstrap-osd
	key: AQAURFVns8WKLBAAIrmp70+ct0v26pOL9tWQ1A==
	caps: [mgr] allow r
	caps: [mon] profile bootstrap-osd
client.bootstrap-rbd
	key: AQCVRFVnedXnDxAAFBqDtzR5T95rW62zLBdbVg==
	caps: [mon] allow profile bootstrap-rbd
client.bootstrap-rbd-mirror
	key: AQCVRFVn7XnoDxAAoHoQt7LGIfUrYGangFQ92A==
	caps: [mon] allow profile bootstrap-rbd-mirror
client.bootstrap-rgw
	key: AQCVRFVnaAzpDxAAe7t/4WqV3y6hHKo1Lj/e9w==
	caps: [mon] allow profile bootstrap-rgw
mgr.openeuler-riscv64
	key: AQCZWFVnhNi5BhAAO3ecohoIDb0wr71yI+q28Q==
	caps: [mds] allow *
	caps: [mon] allow profile mgr
	caps: [osd] allow *
```

将此key文件命名为keyring输出到mgr数据目录

```bash
ceph auth get mgr.openeuler-riscv64 -o /var/lib/ceph/mgr/ceph-openeuler-riscv64/keyring
```

查看keyring

```bash
[root@openeuler-riscv64 ceph]# cat /var/lib/ceph/mgr/ceph-openeuler-riscv64/keyring
[mgr.openeuler-riscv64]
	key = AQCZWFVnhNi5BhAAO3ecohoIDb0wr71yI+q28Q==
	caps mds = "allow *"
	caps mon = "allow profile mgr"
	caps osd = "allow *"
```

启动ceph-mgr，并查看状态

```bash
ceph-mgr -i openeuler-riscv64
或者
sudo systemctl stop ceph-mgr@openeuler-riscv64
sudo systemctl start ceph-mgr@openeuler-riscv64
sudo systemctl enable ceph-mgr@openeuler-riscv64
sudo systemctl -l --no-page status ceph-mgr@openeuler-riscv64
```

![Image](./c07.png)

![Image](./c08.png)

如上图所示问题，其他人也遇到了这个问题<https://gitee.com/src-openeuler/ceph/issues/IB7N9R?from=project-issue>，解决方案是使用oe2309 repo仓库对应的低版本python加密库相关依赖包（python3-asn1crypto python3-cffi python3-cryptography python3-ply python3-pyasn1 python3-pycparser python3-pyOpenSSL）。

下载这些低版本python加密库相关依赖包

```bash
mkdir tmp
cd tmp
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-asn1crypto-1.5.1-1.oe2309.noarch.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-cffi-1.15.1-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-cryptography-40.0.2-2.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-ply-3.11-3.oe2309.noarch.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-pyasn1-0.4.8-4.oe2309.noarch.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-pycparser-2.21-2.oe2309.noarch.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/repo/23.09/OS/riscv64/Packages/python3-pyOpenSSL-23.2.0-1.oe2309.noarch.rpm
```

查看下载的oe2309原生软件包

```bash
[root@openeuler-riscv64 tmp]# ls
python3-asn1crypto-1.5.1-1.oe2309.noarch.rpm
python3-cffi-1.15.1-3.oe2309.riscv64.rpm
python3-cryptography-40.0.2-2.oe2309.riscv64.rpm
python3-ply-3.11-3.oe2309.noarch.rpm
python3-pyasn1-0.4.8-4.oe2309.noarch.rpm
python3-pycparser-2.21-2.oe2309.noarch.rpm
python3-pyOpenSSL-23.2.0-1.oe2309.noarch.rpm
```

查看现有已安装的oe2403原生软件包

```bash
[root@openeuler-riscv64 tmp]# dnf list python3-asn1crypto python3-cffi python3-cryptography python3-ply python3-pyasn1 python3-pycparser python3-pyOpenSSL
Last metadata expiration check: 1:00:23 ago on Sun 08 Dec 2024 04:03:46 PM CST.
Installed Packages
python3-asn1crypto.noarch                    1.5.1-1.oe2403                  @OS
python3-cffi.riscv64                         1.16.0-2.oe2403                 @OS
python3-cryptography.riscv64                 42.0.2-3.oe2403                 @OS
python3-ply.noarch                           3.11-3.oe2403                   @OS
python3-pyOpenSSL.noarch                     24.0.0-1.oe2403                 @OS
python3-pyasn1.noarch                        0.5.1-1.oe2403                  @OS
python3-pycparser.noarch                     2.21-2.oe2403                   @OS
```

卸载已安装的oe2403原生软件包

```bash
rpm -evh python3-asn1crypto.noarch --nodeps
rpm -evh python3-cffi --nodeps
rpm -evh python3-cryptography --nodeps
rpm -evh python3-ply --nodeps
rpm -evh python3-pyasn1 --nodeps
rpm -evh python3-pycparser --nodeps
rpm -evh python3-pyOpenSSL --nodeps
```

安装已下载的oe2309原生软件包

```bash
rpm -ivh python3-asn1crypto-1.5.1-1.oe2309.noarch.rpm --force --nodeps
rpm -ivh python3-cffi-1.15.1-3.oe2309.riscv64.rpm --force --nodeps
rpm -ivh python3-cryptography-40.0.2-2.oe2309.riscv64.rpm --force --nodeps
rpm -ivh python3-ply-3.11-3.oe2309.noarch.rpm --force --nodeps
rpm -ivh python3-pyasn1-0.4.8-4.oe2309.noarch.rpm --force --nodeps
rpm -ivh python3-pycparser-2.21-2.oe2309.noarch.rpm --force --nodeps
rpm -ivh python3-pyOpenSSL-23.2.0-1.oe2309.noarch.rpm --force --nodeps
```

再次查看集群状态，观察到PyO3 modules的相关警告信息已经去除

```bash
ceph -s
```

![Image](./c09.png)

## 9 测试ceph集群的Dashboard

查看dashboard的安装情况

```bash
[root@openeuler-riscv64 ~]# dnf list ceph-mgr-dashboard
Last metadata expiration check: 0:32:12 ago on Sun 08 Dec 2024 04:03:46 PM CST.
Installed Packages
ceph-mgr-dashboard.noarch                 2:18.2.2-5.oe2403                  @OS
```

mgr节点主持dashboard的webserver，这里不启用ssl

```bash
sudo ceph config set mgr mgr/dashboard/ssl false
```

激活dashboard

```bash
sudo ceph mgr module disable dashboard
sudo ceph mgr module enable dashboard
```

设置dashboard监听地址和端口

```bash
ceph config set mgr mgr/dashboard/openeuler-riscv64/server_addr 10.0.2.15
ceph config set mgr mgr/dashboard/openeuler-riscv64/server_port 9090
```

获取dashboard访问网址

```bash
[root@openeuler-riscv64 tmp]# sudo ceph mgr services
{
    "dashboard": "http://10.0.2.15:9090/"
}
```

查看网络状态

```bash
[root@openeuler-riscv64 tmp]# netstat -tnlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 10.0.2.15:6805          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6804          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6807          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6806          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6801          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6800          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6803          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6802          0.0.0.0:*               LISTEN      5262/ceph-osd       
tcp        0      0 10.0.2.15:6809          0.0.0.0:*               LISTEN      47148/ceph-mgr      
tcp        0      0 10.0.2.15:6808          0.0.0.0:*               LISTEN      47148/ceph-mgr      
tcp        0      0 10.0.2.15:6789          0.0.0.0:*               LISTEN      1112/ceph-mon       
tcp        0      0 127.0.0.1:33139         0.0.0.0:*               LISTEN      1067/containerd     
tcp        0      0 10.0.2.15:2380          0.0.0.0:*               LISTEN      2242/etcd           
tcp        0      0 10.0.2.15:2379          0.0.0.0:*               LISTEN      2242/etcd           
tcp        0      0 10.0.2.15:3300          0.0.0.0:*               LISTEN      1112/ceph-mon       
tcp        0      0 10.0.2.15:9090          0.0.0.0:*               LISTEN      47148/ceph-mgr      
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      891/rpcbind         
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1054/sshd: /usr/sbi 
tcp        0      0 127.0.0.1:2379          0.0.0.0:*               LISTEN      2242/etcd           
tcp        0      0 127.0.0.1:2381          0.0.0.0:*               LISTEN      2242/etcd           
tcp        0      0 127.0.0.1:10248         0.0.0.0:*               LISTEN      1119/kubelet        
tcp        0      0 127.0.0.1:10249         0.0.0.0:*               LISTEN      2933/kube-proxy     
tcp        0      0 127.0.0.1:10257         0.0.0.0:*               LISTEN      2235/kube-controlle 
tcp        0      0 127.0.0.1:10259         0.0.0.0:*               LISTEN      2221/kube-scheduler 
tcp6       0      0 :::6443                 :::*                    LISTEN      2223/kube-apiserver 
tcp6       0      0 :::10256                :::*                    LISTEN      2933/kube-proxy     
tcp6       0      0 :::10250                :::*                    LISTEN      1119/kubelet        
tcp6       0      0 :::111                  :::*                    LISTEN      891/rpcbind         
tcp6       0      0 :::22                   :::*                    LISTEN      1054/sshd: /usr/sbi 
```

查看Dashboard的管理员账号

```bash
[root@openeuler-riscv64 tmp]# ceph dashboard ac-user-show admin
Error ENOENT: User 'admin' does not exist
```

创建管理员账号

```bash
touch admin-pw.txt
echo -n "Aa123456" > admin-pw.txt
ceph dashboard ac-user-create admin -i admin-pw.txt administrator
```

显示如下信息

```bash
{"username": "admin", "password": "$2b$12$iJL6Ye3HbLyMVNryjG90ReBI4uvKv1hWdUGCgqv2Yhr0JtdX2BT.i", "roles": ["administrator"], "name":
 null, "email": null, "lastUpdate": 1733707215, "enabled": true, "pwdExpirationDate": null, "pwdUpdateRequired": false}
```

安装浏览器

```bash
dnf install chromium
```

进入桌面，启动浏览器，登录Dashboard

![Image](./c10.png)

查看集群状态

![Image](./c11.png)

## 10 总结

openEuler2403原生的ceph相对openEuler2309原生的ceph要完善一些，但需要借助oe2309的部分相关软件包方能完美实现Dashboard。

最后，恭喜openEuler2403，ceph已经彻底enable了。
