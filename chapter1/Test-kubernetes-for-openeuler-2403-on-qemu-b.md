# 操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part B）

---

## 1 目标

测试kubernetes在openeuler2403上的可用性。

## 2 系统环境

同上篇教程[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part A）](../chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-a.md)

## 3 基础镜像

首先查看官方images，发现images已经列出来了，暂不验证，也无需另行创建。如果拉不下来，再做处理。

```bash
[root@openeuler-riscv64 ~]# kubeadm config images list
I1126 15:30:35.450430  118948 version.go:256] remote version is much newer: v1.31.3; falling back to: stable-1.29
registry.k8s.io/kube-apiserver:v1.29.11
registry.k8s.io/kube-controller-manager:v1.29.11
registry.k8s.io/kube-scheduler:v1.29.11
registry.k8s.io/kube-proxy:v1.29.11
registry.k8s.io/coredns/coredns:v1.11.1
registry.k8s.io/pause:3.9
registry.k8s.io/etcd:3.5.10-0
```

## 4 容器及网络插件

### 4.1 安装容器运行时

这里选择containerd，openEuler2403里面自带的1.6版本有问题，这里选择1.7版本。

```bash
wget https://github.com/containerd/containerd/releases/download/v1.7.3/containerd-1.7.3-linux-riscv64.tar.gz
tar Cxzvf /usr/local containerd-1.7.3-linux-riscv64.tar.gz
```

查看版本

```bash
[root@openeuler-riscv64 ~]# containerd -v
containerd github.com/containerd/containerd v1.7.3 7880925980b188f4c97b462f709d0db8e8962aff
```

### 4.2 安装网络插件

这里选择cni和flannel

```bash
wget https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-riscv64-v1.3.0.tgz
wget https://github.com/flannel-io/cni-plugin/releases/download/v1.4.0-flannel1/flannel-riscv64
```

安装插件

```bash
mkdir -p /opt/cni/bin
tar -xzf cni-plugins-linux-riscv64-v1.3.0.tgz -C /opt/cni/bin
mv ./flannel-riscv64 /opt/cni/bin/flannel
chmod +x /opt/cni/bin/flannel
```

### 4.3 配置启动containerd

创建目录

```bash
mkdir -p /etc/containerd/
```

生成默认配置

```bash
containerd config default | sudo tee /etc/containerd/config.toml
```

编辑配置文件

```bash
vim /etc/containerd/config.toml
```

修改其中的[plugins."io.containerd.grpc.v1.cri"]，根据前面的images信息，将sandbox_image = "registry.k8s.io/pause:3.8"修改为sandbox_image = "registry.k8s.io/pause:3.9"

将SystemdCgroup value从false改为true

```bash
sed -i 's/SystemdCgroup = false/SystemdCgroup = true/g' /etc/containerd/config.toml
```

在获取containerd.service之前，我们可能会需要改下域名解析地址，内容改为nameserver 119.29.29.29

```bash
vim /etc/resolv.conf
```

下载containerd.service

```bash
curl -L https://raw.githubusercontent.com/containerd/containerd/refs/heads/release/1.7/containerd.service -o /etc/systemd/system/containerd.service
```

启动containerd

```bash
systemctl daemon-reload
systemctl start containerd
systemctl enable containerd
systemctl status containerd
systemctl -l --no-page status containerd
journalctl -u  containerd  --no-page
```

成功启动containerd服务

![Image](./k8s1.png)

### 4.4 安装crictl

```bash
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.27.1/crictl-v1.27.1-linux-riscv64.tar.gz
tar zxvf crictl-v1.27.1-linux-riscv64.tar.gz -C /usr/local/bin
crictl info
```

查看版本

```bash
[root@openeuler-riscv64 ~]# crictl -v
crictl version v1.27.1
```

启用服务

```bash
systemctl daemon-reload
systemctl restart containerd
systemctl status containerd
```

配置

```bash
crictl config --set runtime-endpoint=unix:///run/containerd/containerd.sock --set image-endpoint=unix:///run/containerd/containerd.sock
```

查看配置

```bash
[root@openeuler-riscv64 ~]# cat /etc/crictl.yaml
runtime-endpoint: "unix:///run/containerd/containerd.sock"
image-endpoint: "unix:///run/containerd/containerd.sock"
timeout: 0
debug: false
pull-image-on-create: false
disable-pull-on-run: false
[root@openeuler-riscv64 ~]# crictl info
{
  "status": {
    "conditions": [
      {
        "type": "RuntimeReady",
        "status": true,
        "reason": "",
        "message": ""
      },

mkdir -p /etc/cni/net.d
cat << EOF | tee /etc/cni/net.d/10-containerd-net.conflist
{
 "cniVersion": "1.0.0",
 "name": "containerd-net",
 "plugins": [
   {
     "type": "bridge",
     "bridge": "cni0",
     "isGateway": true,
     "ipMasq": true,
     "promiscMode": true,
     "ipam": {
       "type": "host-local",
       "ranges": [
         [{
           "subnet": "10.244.0.0/16"
         }],
         [{
           "subnet": "2001:db8:4860::/64"
         }]
       ],
       "routes": [
         { "dst": "0.0.0.0/0" },
         { "dst": "::/0" }
       ]
     }
   },
   {
     "type": "portmap",
     "capabilities": {"portMappings": true},
     "externalSetMarkChain": "KUBE-MARK-MASQ"
   }
 ]
}
EOF
```

重启containerd

```bash
sudo systemctl daemon-reload
sudo systemctl restart containerd
```

## 5 环境准备

关闭selinux, firewalld, swap

```bash
setenforce 0
sed -i 's/^SELINUX=.*/SELINUX=disabled/' /etc/selinux/config

systemctl disable firewalld
systemctl stop firewalld

#临时关闭swap
swapoff -a
#永久关闭swap
sed -ri 's/.*swap.*/#&/' /etc/fstab
grep swap /etc/fstab 
```

自动同步时间

```bash
dnf install ntp chrony
systemctl start ntpd
systemctl enable ntpd
echo '* */6 * * * /usr/sbin/ntpdate time.windows.com > /dev/null 2>&1' >> /var/spool/cron/root
#注释默认配置
sudo sed -i 's/^pool/#&/' /etc/chrony.conf
#指定上游公共 ntp 服务器，并允许其他节点同步时间
sudo tee -a /etc/chrony.conf << EOF
server 0.asia.pool.ntp.org iburst
server 1.asia.pool.ntp.org iburst
server 2.asia.pool.ntp.org iburst
server 3.asia.pool.ntp.org iburst
allow all
EOF
#重启chronyd服务
sudo systemctl restart chronyd
#开启网络时间同步功能
sudo timedatectl set-ntp true
#执行以下命令，查看存在以^*开头的行，说明已经与服务器时间同步
chronyc sources
```

启用ip_vs模块

```bash
dnf install ipset ipvsadm
#添加需要加载的模块
mkdir -p /etc/sysconfig/ipvsadm
cat > /etc/sysconfig/ipvsadm/ipvs.modules <<EOF
#!/bin/bash
modprobe -- ip_vs
modprobe -- ip_vs_rr
modprobe -- ip_vs_wrr
modprobe -- ip_vs_sh
modprobe -- nf_conntrack
EOF
#授权运行
chmod 755 /etc/sysconfig/ipvsadm/ipvs.modules
#加载ipvs模块
bash /etc/sysconfig/ipvsadm/ipvs.modules
#检查
lsmod | grep -e ip_vs -e nf_conntrack
```

配置路由转发及桥接

```bash
#配置
cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward=1
EOF
#生效
sysctl --system
#加载br_netfilter模块
modprobe  br_netfilter
#检查
lsmod |grep  br_netfilter
```

## 6 单机部署测试

完成安装后，查看kubernetes 1.29各组件的版本，发现这次kubectl已经可用了。

```bash
主机名              IP地址
openeuler-riscv64  10.0.2.15
```

主机域名解析

```bash
cat <<EOF >> /etc/hosts
10.0.2.15 openeuler-riscv64
EOF
```

如需更改节点主机名，使用hostnamectl修改主机名，例如hostnamectl set-hostname k8s-master。本教程因为是单机部署测试，所以不修改主机名。

集群初始化（master节点），其中v=5指输出日志信息等级，方便调试。

```bash
kubeadm init \
--apiserver-advertise-address=10.0.2.15 \
--kubernetes-version=v1.29.11 \
--pod-network-cidr=10.244.0.0/16 \
--service-cidr=10.96.0.0/12 \
--v=5
```

期间会出现错误 [ERROR FileContent--proc-sys-net-bridge-bridge-nf-call-iptables]: /proc/sys/net/bridge/bridge-nf-call-iptables does not exist。这需要确保net.ipv4.ip_forward=1再生效1次并检查，再次启动成功。

```bash
[root@openeuler-riscv64 ~]# sysctl -w net.ipv4.ip_forward=1
net.ipv4.ip_forward = 1
[root@openeuler-riscv64 ~]# cat /proc/sys/net/ipv4/ip_forward
1
```

再次运行集群初始化命令，已无错误，但在拉取基础镜像时失败。查看官方的images注册，<https://github.com/kubernetes/registry.k8s.io>。经验证，kubernetes 1.29.1的基础镜像在官方并不存在。

![Image](./k8s2.png)

## 7 总结

1）k8s的组件和控制平台在openEuler2403中已经得到了原生支持，并正常运行。可喜可贺。

2）在单机部署测试时，并未发现可用的镜像hub，为openEuler2403的原生版本kubernetes1.29.1提供基础riscv64的镜像。如有镜像的话，那么这个实验可以继续下去。

3）发现奕斯伟的docker hub里面的基础镜像是1.29.0的版本（<https://hub.docker.com/r/eswincomputing/kube-apiserver/tags>）

![Image](./k8s3.png)

4）使用奕斯伟的基础镜像也许可行，有待验证。

## 8 参考资料

<https://blog.csdn.net/ljx1528/article/details/137714292>

<https://build.tarsier-infra.isrc.ac.cn/package/show/openEuler:24.03:Epol/kubernetes>

<https://www.cnblogs.com/hello-littlebaby/p/18046131>
