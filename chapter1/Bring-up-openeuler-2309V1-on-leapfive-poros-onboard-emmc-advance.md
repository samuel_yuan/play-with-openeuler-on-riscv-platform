# 操作手册：Bring up openeuler 2309V1 on leapfive poros by onboard emmc with advanced operation

---

## 1 目标

目前，国内有些高校已经拿到了poros开发板，尤其是广东澳门这边的高校，这些高校距离Leapfive较近，合作也更紧密一些。对于有些版本的poros开发板，为了避免用户的误操作，仅能从板载emmc上启动。如果手中拿到的是这样的poros开发板，如何刷emmc将会困扰初级用户许久，本教程将解决这个问题，方便同学们玩openeuler2309。

## 2 检查uboot环境

接上篇教程[《Bring up openeuler 2309V1 on leapfive poros by onboard emmc》](./Bring-up-openeuler-2309V1-on-leapfive-poros-onboard-emmc.md)

使用串口连接主机，并使用MobaXterm等工具连接开发板（这些基本操作可以参考前面的教程），接通poros开发板电源，在出现下述提示时，敲击任意键进入uboot环境。

```bash
U-Boot 2020.07-rc3-00044-gdb000c3e-dirty (Dec 26 2023 - 15:00:16 +0800), Build: jenkins-nb2_build-145

CPU:   rv64imafdc
Model: LeapFive Poros
DRAM:  4 GiB
MMC:   emmc@301007000: 0, sdio0@301005000: 1
Loading Environment from <NULL>... SF: Detected is25wp256 with page size 256 Bytes, erase size 4 KiB, total 32 MiB
*** Warning - bad CRC, using default environment

In:    serial@4F0009000
Out:   serial@4F0009000
Err:   serial@4F0009000
Net:
Warning: ethernet0@301000000 (eth0) using random MAC address - c2:22:76:8f:f4:92
eth0: ethernet0@301000000
Warning: ethernet1@301002000 (eth1) using random MAC address - 5e:48:8a:7d:c5:e7
, eth1: ethernet1@301002000
Hit any key to stop autoboot:  0
=>
```

发现其中并没有saveenv命令，这应该是避免用户的误操作，所以从uboot环境去掉了该命令。

```bash
=> ?
```

查看环境，发现这个版本写死了，即必须从板载emmc启动，应该也是为了避免用户的误操作。

```bash
=> printenv
```

以下主要列出与emmc启动有关的env信息：即启动顺序中mmcboot排在了第一，mmcblk0p1即为mmc 0的第1个分区，其中kernel Image 和设备树leapfive-nb2.dtb放在该分区的boot目录之中。

```bash
......
board_name=poros
bootargs_mmc=setenv bootargs 'console=ttyS0,115200n8 console=tty1 root=/dev/mmcblk0p1 rootwait rw rootfstype=ext4 rdinit=/sbin/init'
bootcmd=run $modeboot; run sdboot; run usbboot; run mmcboot
......
load_uenv=ext4load ${bootdev} ${bootdevnum}:${bootpart} 0x808000000 ${bootenvfile}
mmcboot=echo Booting from mmc ...; if run load_uenv; then run import_uenv; run boot_ext;fi;run bootargs_mmc ; ext4load mmc 0 ${kernel_addr_r} boot/Image; ext4load mmc 0 ${fdt_addr_r} boot/leapfive-nb2.dtb;booti ${kernel_addr_r} - ${fdt_addr_r}
......
modeboot=mmcboot
......
```

那么我们查看板载emmc，有32GB容量

```bash
=> mmcinfo
Device: emmc@301007000
Manufacturer ID: 45
OEM: 100
Name: DA603
Bus Speed: 52000000
Mode: MMC High Speed (52MHz)
Rd Block Len: 512
MMC version 5.1
High Capacity: Yes
Capacity: 29.1 GiB
Bus Width: 8-bit
Erase Group Size: 512 KiB
HC WP Group Size: 8 MiB
User Capacity: 29.1 GiB WRREL
Boot Capacity: 4 MiB ENH
RPMB Capacity: 4 MiB ENH
Boot area 0 is not write protected
Boot area 1 is not write protected
```

查看emmc的分区，说明emmc只有mmcblk0p1里面有内容，原生的Leapfive的操作系统的boot文件和rootfs文件都在此分区之中。

```bash
=> ext4ls mmc 0:0
** Unrecognized filesystem type **
=> ext4ls mmc 0:1
<DIR>       4096 .
<DIR>       4096 ..
<DIR>      16384 lost+found
<DIR>       4096 bin
<DIR>       4096 run
<DIR>       4096 home
<DIR>       4096 sbin
<DIR>       4096 tmp
<DIR>       4096 etc
<DIR>       4096 media
<DIR>       4096 mnt
<DIR>       4096 var
<DIR>       4096 boot
<DIR>       4096 dev
<DIR>       4096 usr
<DIR>       4096 sys
<DIR>       4096 proc
<DIR>       4096 lib
<DIR>       4096 opt
=> ext4ls mmc 0:2
** Invalid partition 2 **
```

下面，本教程将把openEuler2309版本，刷入emmc，并让poros开发板成功启动

## 3 操作步骤

总体实施路径：准备openEuler2309的sd卡 - 在uboot下手动实现SD卡启动openEuler2309 - 从板载emmc启动openEuler2309。

### 3.1 准备openEuler2309的sd卡

将sd卡插入usb读卡器中，接到电脑主机。在主机的ubuntu系统中为SD卡分区，本教程选择分1个区。

```bash
sudo gpart
```

![Image](./bring1.png)

完成分区后查看SD卡内容如下

```bash
riscv@ubuntu:~/riscv64-linux$ cd /media/riscv/b9237214-0bdc-422f-b119-4f66b3aabb5c/
riscv@ubuntu:/media/riscv/b9237214-0bdc-422f-b119-4f66b3aabb5c$ ls
lost+found
```

解压openEuler2309的rootfs到sd卡上

```bash
sudo tar -xvzf /mnt/hgfs/img/openEuler-23.09-V1-riscv64/rootfs/openeuler-rootfs-xfce.tar.gz -C /media/riscv/b9237214-0bdc-422f-b119-4f66b3aabb5c/
```

检查poros开发包的prebuilt目录，将nb2-poros-hdmi.dtb和Image复制到sd卡的boot目录中

```bash
sudo cp nb2-poros-hdmi.dtb /media/riscv/b9237214-0bdc-422f-b119-4f66b3aabb5c/boot/
sudo cp Image /media/riscv/b9237214-0bdc-422f-b119-4f66b3aabb5c/boot/
```

根据uboot env的信息，重命名nb2-poros-hdmi.dtb，即：

```bash
cd /media/riscv/b9237214-0bdc-422f-b119-4f66b3aabb5c/boot/
sudo mv nb2-poros-hdmi.dtb leapfive-nb2.dtb
```

至此openeuler2309的sd卡准备完毕。

### 3.2 在uboot下手动实现SD卡启动openEuler2309

取出usb读卡器，接到poros开发包的usb接口，接通电源启动开发板，并进入uboot环境。运行如下命令：

```bash
usb start
ext4load usb 0 0x80a000000 boot/Image
ext4load usb 0 0x80f000000 boot/leapfive-nb2.dtb
setenv bootargs 'console=ttyS0,115200n8 console=tty1 root=/dev/sda1 rootwait rw rdinit=/sbin/init'
booti 0x80a000000 - 0x80f000000
```

### 3.3 从板载emmc启动openEuler2309

启动openeuler后，查看板载emmc为/dev/mmcblk0p1

```bash
[root@openeuler-riscv64 ~]# ls /dev/mmc*
/dev/mmcblk0       /dev/mmcblk0boot1  /dev/mmcblk0rpmb
/dev/mmcblk0boot0  /dev/mmcblk0p1
[root@openeuler-riscv64 rootfs]# sudo fdisk -l
Disk /dev/sda: 59.48 GiB, 63864569856 bytes, 124735488 sectors
Disk model: Storage Device
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x3b1719ca
Device     Boot Start       End   Sectors  Size Id Type
/dev/sda1        2048 124735487 124733440 59.5G 83 Linux

Disk /dev/mmcblk0: 29.12 GiB, 31268536320 bytes, 61071360 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xafa00cf0

Device         Boot Start     End Sectors Size Id Type
/dev/mmcblk0p1       2048 4194303 4192256   2G 83 Linux
```

发现mmcblk0需要重新分区和格式化，否则容量只有2G，装不下openeuler。以下是分区和格式化的步骤：

```bash
[root@openeuler-riscv64 ~]# sudo fdisk /dev/mmcblk0
Welcome to fdisk (util-linux 2.39.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
Command (m for help): d
Selected partition 1
Partition 1 has been deleted.

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-61071359, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-61071359, default 61071359):

Created a new partition 1 of type 'Linux' and of size 29.1 GiB.
Partition #1 contains a ext4 signature.

Do you want to remove the signature? [Y]es/[N]o: y

The signature will be removed by a write command.

Command (m for help): a
Selected partition 1
The bootable flag on partition 1 is enabled now.

Command (m for help): p
Disk /dev/mmcblk0: 29.12 GiB, 31268536320 bytes, 61071360 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xafa00cf0

Device         Boot Start      End  Sectors  Size Id Type
/dev/mmcblk0p1 *     2048 61071359 61069312 29.1G 83 Linux

Filesystem/RAID signature on partition 1 will be wiped.

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
[ 3291.255463]  mmcblk0: p1
Syncing disks.

[ 3291.274404]  mmcblk0: p1
[root@openeuler-riscv64 ~]# sudo mkfs.ext4 -L rootfs /dev/mmcblk0p1
mke2fs 1.47.0 (5-Feb-2023)
Discarding device blocks: done
Creating filesystem with 7633664 4k blocks and 1908736 inodes
Filesystem UUID: fb2c26b6-7f61-4be4-84fc-a709cbfd7d34
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
        4096000

Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done
```

自此，emmc已完成分区和格式化，接下来就是复制文件到emmc中。

创建rootfs目录，挂载/dev/mmcblk0p1

```bash
[root@openeuler-riscv64 ~]# sudo mkdir rootfs
[root@openeuler-riscv64 ~]# sudo mount /dev/mmcblk0p1 rootfs/
[root@openeuler-riscv64 ~]# cd rootfs/
[root@openeuler-riscv64 rootfs]# ls
lost+found
```

解压openeuler2303的rootfs到此目录中。

```bash
[root@openeuler-riscv64 rootfs]# sudo tar -xvzf /openeuler-rootfs-xfce.tar.gz -C .
```

将leapfive-nb2.dtb和Image复制到boot目录中,最后卸载rootfs。

```bash
[root@openeuler-riscv64 rootfs]# sudo cp /boot/leapfive-nb2.dtb boot/
[root@openeuler-riscv64 rootfs]# sudo cp /boot/Image boot/
[root@openeuler-riscv64 rootfs]# sync
[root@openeuler-riscv64 rootfs]# cd ..
[root@openeuler-riscv64 ~]#sudo umount rootfs
```

所需文件复制完毕，重新启动poros开发板，系统将从板载emmc上加载，并成功启动openeuler2309。

## 4 小结

本教程包含了uboot环境下的一些操作，虽然繁复了点儿，但是也可以加深大家对系统启动的一些理解。注意到，还有一种简单的方法可以让我们不受强制emmc启动的限制，从sd卡或者usb启动均可，即：删除板载emmc里面的启动文件即可。
