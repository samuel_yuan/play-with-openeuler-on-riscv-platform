# 操作手册：Porting openGauss thirty party dependency Masstree on openEuler2309V1

---

## 1 目标

rvspoc的比赛正如火如荼的进行着，为了凑个热闹，谨以此文献给那些对opengauss感兴趣的新人，希望更多的人加入到openeuler的适配工作中。

## 2 系统环境的选择

openGauss 5.1.0对系统环境的要求是gcc 10.3.1和python 3.8m。以下列出openeuler几个版本的编译环境：

1）openeuler2203：gcc 10.3.1，python 3.9.9。

十分接近opengauss对系统环境的要求，且有合适的openjdk版本，这个版本是最有利于移植opengauss的版本，工作量最小，最易出结果。考虑到已经有人分享了相关方案，关注rvspoc比赛的朋友应该已知晓，本文不再赘述，不讨论在这个版本移植opengauss。仅以此文勾引玩家入坑。

尽管如此，我也向大家推荐我认为较好的一个openeuler2203版本（要知道openeuler2203也是有许多版本的^_!），该版本是我早些时候为visionfive1和2打包的，链接为https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/community/samuel_yuan/230321oe-samuel/， 大家可以在这个经典版本上一边玩opengauss，一边玩我教程中的各种游戏，十分解压的系统环境。我一年前竟然做了这个版本，事实上原始版本已绝版，被某人删除了。除了这个版本外，后续的V1和V2版本玩游戏都有障碍。

在此openeuler2203经典版本上，移植opengauss lite版本跟玩似的。如有要求，会考虑适配visionfive1&2。

2）openeuler2303：gcc 10.3.1，python 3.10.9。

gcc是对口的，但是python 3.10.9，一些老的声明可能被抛弃，导致编译警告和错误。

我打包的openeuler2203经典版本上传ISCAS仓库后，不久openeuler2303发版了，这个版本可以玩游戏，但可能不太容易玩到opengauss。

3）openeuler2309：gcc 12.3.1，python 3.11.4。

gcc和python的版本高了，一些老的声明可能被抛弃，导致编译警告和错误。

但是，openeuler2309的kernel是最新的，对新玩具比较友好。所以，本教程尝试在这个版本上移植一个opengauss三方库，带着新玩家们入坑。

## 3 环境准备

上篇教程中，我们已经准备好了基于SG2042 Pioneer 1.2的openeuler2309V1系统环境。那么，现在我们例行装一些基本包。

1）gcc环境

```bash
dnf install -y curl bc git gperf libtool
dnf install autoconf automake python3 libmpc-devel mpfr-devel gmp-devel gawk bison flex texinfo patchutils gcc gcc-c++ zlib-devel expat-devel
```

2）python环境

```bash
dnf install python3-dev*
```

做python软连接

```bash
[root@openeuler-riscv64 /]# whereis python3
python3: /usr/bin/python3
[root@openeuler-riscv64 bin]# ll | grep python
lrwxrwxrwx 1 root root          10 Jul 20  2023 python3 -> python3.11
-rwxr-xr-x 1 root root        6192 Jul 20  2023 python3.11
[root@openeuler-riscv64 bin]# sudo ln -s /usr/bin/python3 /usr/bin/python
[root@openeuler-riscv64 bin]# ll | grep python
lrwxrwxrwx 1 root root          16 Feb 22 12:24 python -> /usr/bin/python3
lrwxrwxrwx 1 root root          10 Jul 20  2023 python3 -> python3.11
-rwxr-xr-x 1 root root        6192 Jul 20  2023 python3.11
```

3）openjdk环境

```bash
dnf install *java-1.8.0-openjdk*
java -version
版本信息如下：
openjdk version "1.8.0_382"
OpenJDK Runtime Environment Bisheng (build 1.8.0_382-b05)
OpenJDK 64-Bit Zero VM Bisheng (build 25.382-b05, interpreted mode)
```

## 4 移植masstree

为什么移植它？因为70%以上的opengauss依赖，已经被openeuler2309V1支持了（我有预感，今年opengauss可能会有个大版本更新支持openeuler2403LTS版本），你将遇见的第一个卡点就是masstree这个库。下面移植该库，以抛砖引玉，让新玩家们能够继续下去。因此，此处不考虑masstree的版本问题，此处以rvspoc比赛仓库（https://github.com/plctlab/rvspoc-s2305-openGauss-server，https://github.com/plctlab/rvspoc-s2305-openGauss-third_party ）中的masstree包（本文也提供此[masstree包](./masstree.tar)下载用于学习）为例，说明移植问题的处理。

1）第一次编译的问题如下：

```bash
g++: error: unrecognized command-line option ‘-mcx16’
```

2）进入dependency三方库masstree的目录，执行sh build.sh确认编译问题。查看 build.sh ，该脚本主要下载源码压缩包，解压，打补丁，然后编译源码，并将二进制程序复制到指定位置。进入masstree的code目录修改Makefile。

![Image](./libmasstree1.png)

加入以下代码：

```bash
else ifeq ($(shell uname -p),riscv64)
        CXXFLAGS += -march=rv64g -Wno-deprecated
```

加入-march=rv64g，以支持riscv编译；加入-Wno-deprecated，禁用废弃警告，老代码通常会有些废弃的特性，这个选项可以避免相应的警告。

考虑到opengauss里的masstree是老版本的代码，其中的reinterpret_cast在新版本的gcc（gcc 6.2.0及以上版本）编译时会抛出警告，需要修改编译参数CXXFLAGS，去掉其中的-Werror，避免将警告作为错误导致编译无法完成。reinterpret_cast是谨慎转换，暂且相信代码作者充分理解代码作用，希望不会有对齐问题，如有对齐问题，则需要多加点代码。我们这里仅举例说明移植问题的处理，用的代码较老，不涉及更多。

运行命令完成二进制库文件的编译。

```bash
make -sj
```

编译成功。

![Image](./libmasstree2.png)

## 5 总结

本文展示了移植opengauss三方库masstree的过程，说明了对三方库的编译参数进行调整放宽的涵义。即在编译参数CXXPLAGS中加入-march=rv64g是调整以适配riscv指令集，加入-Wno-deprecated和去掉-Werror则是放宽编译条件以适应openeuler2309V1的编译环境。

最后，祝玩家们入坑后嗨起来。
