# 操作手册：RVV Program and openEuler2403 B

---

## 1 目标

A 学习rvv intrinsic程序的编译和运行，并分别在仿真环境qemu和spike中运行。

B 学习rvv intrinsic程序，对比普通阵列加法和rvv阵列加法，并分别在仿真环境和openEuler环境中运行验证。

本篇教程内容聚焦B

## 2 RVV intrinsic 程序 - array add

编写array add 程序

```bash
sudo vim array_add.c
```

添加代码

```c
#include <stdio.h>
#include <stdlib.h>
#include <riscv_vector.h>
#include <time.h>

// 正常的数组相加函数
void array_add_normal(size_t n, const float *a, const float *b, float *c) {
    for (size_t i = 0; i < n; i++) {
        c[i] = a[i] + b[i];
    }
}

// 使用 RVV 向量化的数组相加函数
void array_add_rvv(size_t n, const float *a, const float *b, float *c) {
    size_t l;
    vfloat32m8_t va, vb, vc;

    for (; n > 0; n -= l) {
        l = __riscv_vsetvl_e32m8(n);  // 设置向量长度
        va = __riscv_vle32_v_f32m8(a, l);  // 加载 a
        vb = __riscv_vle32_v_f32m8(b, l);  // 加载 b
        vc = __riscv_vfadd_vv_f32m8(va, vb, l);  // 向量加法
        __riscv_vse32_v_f32m8(c, vc, l);  // 存回 c
        a += l;
        b += l;
        c += l;
    }
}

int main() {
    size_t N = 1024;  // 数组大小
    float *a, *b, *c, *d;

    // 分配内存，确保数据对齐
    posix_memalign((void**)&a, 32, N * sizeof(float));  
    posix_memalign((void**)&b, 32, N * sizeof(float));  
    posix_memalign((void**)&c, 32, N * sizeof(float));  
    posix_memalign((void**)&d, 32, N * sizeof(float));  

    // 初始化数据
    for (size_t i = 0; i < N; i++) {
        a[i] = i * 1.0f;
        b[i] = i * 2.0f;
    }

    struct timespec start, end;
    
    // 使用正常的数组加法
    clock_gettime(CLOCK_REALTIME, &start);
    array_add_normal(N, a, b, c);
    clock_gettime(CLOCK_REALTIME, &end);
    long seconds = end.tv_sec - start.tv_sec;
    long nanoseconds = end.tv_nsec - start.tv_nsec;
    if (start.tv_nsec > end.tv_nsec) {
        --seconds;
        nanoseconds += 1000000000;
    }
    printf("Elapsed time for normal add: %ld.%09ld seconds\n", seconds, nanoseconds);
    
    // 使用 RVV 向量化的加法
    clock_gettime(CLOCK_REALTIME, &start);
    array_add_rvv(N, a, b, d);
    clock_gettime(CLOCK_REALTIME, &end);
    seconds = end.tv_sec - start.tv_sec;
    nanoseconds = end.tv_nsec - start.tv_nsec;
    if (start.tv_nsec > end.tv_nsec) {
        --seconds;
        nanoseconds += 1000000000;
    }
    printf("Elapsed time for RVV add: %ld.%09ld seconds\n", seconds, nanoseconds);

    // 可以在这里打印部分结果，验证两种方法是否一致
    printf("c[0]: %f, d[0]: %f\n", c[0], d[0]);

    // 释放内存
    free(a);
    free(b);
    free(c);
    free(d);

    return 0;
}
```

采用riscv64-unknown-linux-gnu-gcc编译代码

```bash
riscv64-unknown-linux-gnu-gcc -march=rv64gcv -O2 -g -static ./array_add.c -o ./array_add
```

测试结果1-spike 运行

```bash
riscv@qemu-vm:~/riscv64-linux/rvv_test$ spike --isa=rv64gcv /opt/riscv/riscv64-unknown-linux-gnu/bin/pk ./array_add
Elapsed time for normal add: 0.000003188 seconds
Elapsed time for RVV add: 0.000000728 seconds
c[0]: 0.000000, d[0]: 0.000000
```

测试结果2-qemu 运行

```bash
riscv@qemu-vm:~/riscv64-linux/rvv_test$ qemu-riscv64 array_add
Elapsed time for normal add: 0.000101666 seconds
Elapsed time for RVV add: 0.000086109 seconds
c[0]: 0.000000, d[0]: 0.000000
```

测试结果3-在openEuler2403LTS系统中运行

![Image](./rvv-openEuler2403LTS.png)

测试结果4-在muse box k1 bianbu RVA22系统中运行

![Image](./rvv-k1-bianbu.png)

## 4 小结

1）spike比qemu的仿真运行的效率要高，正常加法运算的时间比RVV加法运算的时间要长。

2）openEuler2403LTS无法运行rvv的程序。

3）openEuler2403LTS版本仅支持到RVA20，不支持rvv扩展，因此无法运行rvv的程序。社区里给的说法是rvv扩展将在openEuler2403LTS SP1版本中得到支持，该版本支持到RVA22，并对部分扩展删减了一点，预计在年前推出。支持RVA23版本的openEuler预计要到2603版本才能得到支持。

## 5 参考资料

https://blog.51cto.com/u_16213591/9767474

https://github.com/riscv-non-isa/rvv-intrinsic-doc/tree/v1.0.x/examples
