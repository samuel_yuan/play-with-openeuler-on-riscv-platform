# 操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part D）

---

## 1 目标

测试kubernetes在openeuler2403上的可用性。

## 2 前期工作

系统环境参考上篇教程[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part A）](../chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-a.md)

基础镜像、容器及网络插件、环境设置参考上篇教程[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part B）](../chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-b.md)

验证基础镜像、集群初始化参考上篇教程[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part C）](../chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-c.md)

## 3 hello示例

获取节点信息

```bash
[root@openeuler-riscv64 ~]# kubectl get pod -A
NAMESPACE      NAME                                        READY   STATUS    RESTARTS       AGE
kube-flannel   kube-flannel-ds-2mc2f                       1/1     Running   0              105m
kube-system    coredns-76f75df574-8m497                    0/1     Running   1 (7d2h ago)   6d21h
kube-system    coredns-76f75df574-d926v                    0/1     Running   1 (7d2h ago)   6d21h
kube-system    etcd-openeuler-riscv64                      1/1     Running   1 (7d2h ago)   6d21h
kube-system    kube-apiserver-openeuler-riscv64            1/1     Running   1 (7d2h ago)   6d21h
kube-system    kube-controller-manager-openeuler-riscv64   1/1     Running   1 (7d2h ago)   6d21h
kube-system    kube-proxy-xm9p6                            1/1     Running   1 (7d2h ago)   6d21h
kube-system    kube-scheduler-openeuler-riscv64            1/1     Running   1 (7d2h ago)   6d21h
[root@openeuler-riscv64 ~]# kubectl get nodes
NAME                STATUS   ROLES           AGE     VERSION
openeuler-riscv64   Ready    control-plane   6d21h   v1.29.1
```

编辑echo-service.yaml，这里直接使用<https://hub.docker.com/u/carlosedp>的hello镜像<https://hub.docker.com/r/carlosedp/echo_on_riscv/tags>。

```bash
cat <<EOF >> echo-service.yaml
apiVersion: v1
kind: Pod
metadata:
  name: hello
spec:
  containers:
  - name: hello
    image: carlosedp/echo_on_riscv
    ports:
    - containerPort: 8080
      hostPort: 8080
  tolerations:
  - key: "node-role.kubernetes.io/control-plane"
    operator: "Exists"
    effect: "NoSchedule"
EOF
```

部署

```bash
[root@openeuler-riscv64 ~]# kubectl apply -f echo-service.yaml
pod/hello created
```

检查

```bash
[root@openeuler-riscv64 ~]# kubectl get pod -o wide
NAME    READY   STATUS              RESTARTS   AGE   IP       NODE                NOMINATED NODE   READINESS GATES
hello   0/1     ContainerCreating   0          16s   <none>   openeuler-riscv64   <none>           <none>
[root@openeuler-riscv64 ~]# kubectl describe pods/hello
Name:             hello
Namespace:        default
Priority:         0
Service Account:  default
Node:             openeuler-riscv64/10.0.2.15
Start Time:       Mon, 02 Dec 2024 13:40:15 +0800
Labels:           <none>
Annotations:      <none>
Status:           Running
IP:               10.244.0.6
IPs:
  IP:  10.244.0.6
  IP:  2001:db8:4860::6
Containers:
  hello:
    Container ID:   containerd://fc274b5adb3ed05ac67bef422ecf5830bff8391baf5469e21ebc2f1daf40363a
    Image:          carlosedp/echo_on_riscv
    Image ID:       docker.io/carlosedp/echo_on_riscv@sha256:2f18f6056f625b2702426158792ce76b9bd2d41c9b8cfe52225d1408dd6d9432
    Port:           8080/TCP
    Host Port:      8080/TCP
    State:          Running
      Started:      Mon, 02 Dec 2024 13:40:38 +0800
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-nk8gp (ro)
Conditions:
  Type                        Status
  PodReadyToStartContainers   True
  Initialized                 True
  Ready                       True
  ContainersReady             True
  PodScheduled                True
Volumes:
  kube-api-access-nk8gp:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   BestEffort
Node-Selectors:              <none>
Tolerations:                 node-role.kubernetes.io/control-plane:NoSchedule op=Exists
                             node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  26s   default-scheduler  Successfully assigned default/hello to openeuler-riscv64
  Normal  Pulling    19s   kubelet            Pulling image "carlosedp/echo_on_riscv"
  Normal  Pulled     5s    kubelet            Successfully pulled image "carlosedp/echo_on_riscv" in 13.893s (13.893s including waiting)
  Normal  Created    5s    kubelet            Created container hello
  Normal  Started    3s    kubelet            Started container hello
```

测试hello示例

```bash
[root@openeuler-riscv64 ~]# curl 127.0.0.1:8080
Hello, World! I'm running on linux/riscv64 inside a container!
```

![Image](./k8s7.png)

## 4 总结

1）carlosedp大神的hello镜像成功运行，输出了我们期待的调试信息。

2）carlosedp大神的docker hub上还有更多的镜像用例，可供调测，也希望社区里面的爱好者们调测更多镜像。
