# 操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part C）

---

## 1 目标

测试kubernetes在openeuler2403上的可用性。

## 2 前期工作

系统环境参考上篇教程[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part A）](../chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-a.md)

基础镜像、容器及网络插件、环境设置参考上篇教程[操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part B）](../chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-b.md)

openEuler社区，近期开放了镜像hub，本篇教程将使用这些来自社区的基础镜像，测试单机部署的情况。

## 3 验证基础镜像

从社区分享的镜像hub拉取基础镜像

```bash
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/kube-apiserver:v1.29.1
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/kube-controller-manager:v1.29.1
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/kube-scheduler:v1.29.1
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/kube-proxy:v1.29.1
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/coredns:v1.11.1
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/pause:3.9
ctr -n k8s.io images pull hub.oepkgs.net/ruoqing/etcd:3.5.10-0
```

或者#kubeadm config images pull --image-repository=hub.oepkgs.net/ruoqing/kubernetesVersion: 1.29.1

查看已拉取的镜像

```bash
[root@openeuler-riscv64 ~]# ctr -n k8s.io images list | grep hub.oepkgs.net/ruoqing/
hub.oepkgs.net/ruoqing/coredns:v1.11.1                                                                  application/vnd.docker.distribution.manifest.v2+json      sha256:9595bff80434bc1ff58c778cf29a65f0dcf86509cb4ad2f9d5cbc18980449d42 282.0 MiB linux/riscv64                                                                io.cri-containerd.image=managed
hub.oepkgs.net/ruoqing/etcd:3.5.10-0                                                                    application/vnd.docker.distribution.manifest.v2+json      sha256:35750bbef2ff197e1d5c4bc698a78207a08f748a52647fe74f8e5ae4987f8657 101.6 MiB linux/riscv64                                                                io.cri-containerd.image=managed
hub.oepkgs.net/ruoqing/kube-apiserver:v1.29.1                                                           application/vnd.docker.distribution.manifest.v2+json      sha256:7351fee5b8f9bd90a1e8f3348e00e6717da2bcc2d8f350302badfd09d5a58320 123.0 MiB linux/riscv64                                                                io.cri-containerd.image=managed
hub.oepkgs.net/ruoqing/kube-controller-manager:v1.29.1                                                  application/vnd.docker.distribution.manifest.v2+json      sha256:1cca505dc9a08836aa3910c6cc09a75bfa37bb2b5344adbd8ac01f11ab4b278a 121.4 MiB linux/riscv64                                                                io.cri-containerd.image=managed
hub.oepkgs.net/ruoqing/kube-proxy:v1.29.1                                                               application/vnd.docker.distribution.manifest.v2+json      sha256:4d005cf935dd5687126d8bae5b960bfa15816e40178f2d53e1961e803000513f 323.1 MiB linux/riscv64                                                                io.cri-containerd.image=managed
hub.oepkgs.net/ruoqing/kube-scheduler:v1.29.1                                                           application/vnd.docker.distribution.manifest.v2+json      sha256:b208846242b846a5718ca94ffe2b5de827b93b88f7ae192da0fdaf8614381436 105.7 MiB linux/riscv64                                                                io.cri-containerd.image=managed
hub.oepkgs.net/ruoqing/pause:3.9                                                                        application/vnd.docker.distribution.manifest.v2+json      sha256:dea2d77edb6a41bce47d18a5b5cf4283a59305ad6101a552f91a180f41433149 268.6 KiB linux/riscv64                                                                io.cri-containerd.image=managed
```

打tag

```bash
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/pause:3.9 registry.k8s.io/pause:3.9
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/etcd:3.5.10-0 registry.k8s.io/etcd:3.5.10-0
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/coredns:v1.11.1  registry.k8s.io/coredns/coredns:v1.11.1
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/kube-apiserver:v1.29.1 registry.k8s.io/kube-apiserver:v1.29.11
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/kube-controller-manager:v1.29.1 registry.k8s.io/kube-controller-manager:v1.29.11
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/kube-scheduler:v1.29.1 registry.k8s.io/kube-scheduler:v1.29.11
ctr -n k8s.io image tag hub.oepkgs.net/ruoqing/kube-proxy:v1.29.1 registry.k8s.io/kube-proxy:v1.29.11
```

检查镜像

```bash
ctr -n k8s.io images list
```

信息如下：

```bash
registry.k8s.io/coredns/coredns:v1.11.1                                 application/vnd.docker.distribution.manifest.v2+json      sha256:9595bff80434bc1ff58c778cf29a65f0dcf86509cb4ad2f9d5cbc18980449d42 282.0 MiB linux/riscv64                                                                io.cri-containerd.image=managed
registry.k8s.io/etcd:3.5.10-0                                           application/vnd.docker.distribution.manifest.v2+json      sha256:35750bbef2ff197e1d5c4bc698a78207a08f748a52647fe74f8e5ae4987f8657 101.6 MiB linux/riscv64                                                                io.cri-containerd.image=managed
registry.k8s.io/kube-apiserver:v1.29.11                                 application/vnd.docker.distribution.manifest.v2+json      sha256:7351fee5b8f9bd90a1e8f3348e00e6717da2bcc2d8f350302badfd09d5a58320 123.0 MiB linux/riscv64                                                                io.cri-containerd.image=managed
registry.k8s.io/kube-controller-manager:v1.29.11                        application/vnd.docker.distribution.manifest.v2+json      sha256:1cca505dc9a08836aa3910c6cc09a75bfa37bb2b5344adbd8ac01f11ab4b278a 121.4 MiB linux/riscv64                                                                io.cri-containerd.image=managed
registry.k8s.io/kube-proxy:v1.29.11                                     application/vnd.docker.distribution.manifest.v2+json      sha256:4d005cf935dd5687126d8bae5b960bfa15816e40178f2d53e1961e803000513f 323.1 MiB linux/riscv64                                                                io.cri-containerd.image=managed
registry.k8s.io/kube-scheduler:v1.29.11                                 application/vnd.docker.distribution.manifest.v2+json      sha256:b208846242b846a5718ca94ffe2b5de827b93b88f7ae192da0fdaf8614381436 105.7 MiB linux/riscv64                                                                io.cri-containerd.image=managed
registry.k8s.io/pause:3.9                                               application/vnd.docker.distribution.manifest.v2+json      sha256:dea2d77edb6a41bce47d18a5b5cf4283a59305ad6101a552f91a180f41433149 268.6 KiB linux/riscv64                                                                io.cri-containerd.image=managed
```

看上去一切正常

## 4 集群初始化（master节点）

```bash
kubeadm init \
--apiserver-advertise-address=10.0.2.15 \
--kubernetes-version=v1.29.11 \
--pod-network-cidr=10.244.0.0/16 \
--service-cidr=10.96.0.0/12 \
--v=5
```

如下图，我们可以观察到在集群初始化时，判断所需的基础镜像已存在。

![Image](./k8s4.png)

初始化需要等待几分钟

![Image](./k8s5.png)

初始化成功，上图中的集群信息需要记录下来

```bash
Your Kubernetes control-plane has initialized successfully!
To start using your cluster, you need to run the following as a regular user:
  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
Alternatively, if you are the root user, you can run:
  export KUBECONFIG=/etc/kubernetes/admin.conf
You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/
Then you can join any number of worker nodes by running the following on each as root:
kubeadm join 10.0.2.15:6443 --token fajrgb.xcgdogvzt7c5cfcl \
        --discovery-token-ca-cert-hash sha256:b2017778ac7ba38c0e273ff950293123b9201227d46df04ada1d3808db1397bf
```

按以上信息，完成设置。

```bash
mkdir -p ~/.kube
cp -i /etc/kubernetes/admin.conf ~/.kube/config
chown $(id -u):$(id -g) ~/.kube/config
```

## 5 安装网络插件（master节点）

```bash
[root@openeuler-riscv64 ~]# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
namespace/kube-flannel created
clusterrole.rbac.authorization.k8s.io/flannel created
clusterrolebinding.rbac.authorization.k8s.io/flannel created
serviceaccount/flannel created
configmap/kube-flannel-cfg created
daemonset.apps/kube-flannel-ds created
```

## 6 测试

检查kubelet和containerd等服务的状态

```bash
#systemctl start kubelet
#systemctl enable --now kubelet
systemctl -l --no-page status kubelet
#systemctl start containerd
#systemctl enable containerd
systemctl -l --no-page status containerd
```

![Image](./k8s6.png)

```bash
[root@openeuler-riscv64 ~]# crictl  ps -a | grep kube | grep -v pause
55b6983b6080d       14e28ac5fffb1       About a minute ago   Running             kube-flannel              0                   edfd198794dd4       kube-flannel-ds-2mc2f
6ce6f280f4e5d       14e28ac5fffb1       About a minute ago   Exited              install-cni               0                   edfd198794dd4       kube-flannel-ds-2mc2f
20ff011dabdbf       ecce7df02ccb6       2 minutes ago        Exited              install-cni-plugin        0                   edfd198794dd4       kube-flannel-ds-2mc2f
d0dbedd558d33       f7bc1242b4d8f       15 minutes ago       Running             kube-proxy                1                   fe192d5e29dbf       kube-proxy-xm9p6
f4815e8ae614d       57c9a9a978831       16 minutes ago       Running             kube-controller-manager   1                   cae53d61edfd1       kube-controller-manager-openeuler-riscv64
9cf7afa2bcb38       35b6c407a10ef       16 minutes ago       Running             kube-scheduler            1                   560fb9f479c89       kube-scheduler-openeuler-riscv64
1abb4ecab934a       099b8bc070969       16 minutes ago       Running             kube-apiserver            1                   08c59543be31a       kube-apiserver-openeuler-riscv64
93f514194a8ab       f7bc1242b4d8f       6 days ago           Exited              kube-proxy                0                   69f6455126e22       kube-proxy-xm9p6
44919e5e66ab3       57c9a9a978831       6 days ago           Exited              kube-controller-manager   0                   109dff7dceeaf       kube-controller-manager-openeuler-riscv64
e150b990b4386       099b8bc070969       6 days ago           Exited              kube-apiserver            0                   50a82fcc0455d       kube-apiserver-openeuler-riscv64
59645e77ddf1a       35b6c407a10ef       6 days ago           Exited              kube-scheduler            0                   ed0d39d0caad6       kube-scheduler-openeuler-riscv64
[root@openeuler-riscv64 ~]# crictl --runtime-endpoint unix:///var/run/containerd/containerd.sock ps -a | grep kube | grep -v pause
55b6983b6080d       14e28ac5fffb1       About a minute ago   Running             kube-flannel              0                   edfd198794dd4       kube-flannel-ds-2mc2f
6ce6f280f4e5d       14e28ac5fffb1       2 minutes ago        Exited              install-cni               0                   edfd198794dd4       kube-flannel-ds-2mc2f
20ff011dabdbf       ecce7df02ccb6       2 minutes ago        Exited              install-cni-plugin        0                   edfd198794dd4       kube-flannel-ds-2mc2f
d0dbedd558d33       f7bc1242b4d8f       15 minutes ago       Running             kube-proxy                1                   fe192d5e29dbf       kube-proxy-xm9p6
f4815e8ae614d       57c9a9a978831       17 minutes ago       Running             kube-controller-manager   1                   cae53d61edfd1       kube-controller-manager-openeuler-riscv64
9cf7afa2bcb38       35b6c407a10ef       17 minutes ago       Running             kube-scheduler            1                   560fb9f479c89       kube-scheduler-openeuler-riscv64
1abb4ecab934a       099b8bc070969       17 minutes ago       Running             kube-apiserver            1                   08c59543be31a       kube-apiserver-openeuler-riscv64
93f514194a8ab       f7bc1242b4d8f       6 days ago           Exited              kube-proxy                0                   69f6455126e22       kube-proxy-xm9p6
44919e5e66ab3       57c9a9a978831       6 days ago           Exited              kube-controller-manager   0                   109dff7dceeaf       kube-controller-manager-openeuler-riscv64
e150b990b4386       099b8bc070969       6 days ago           Exited              kube-apiserver            0                   50a82fcc0455d       kube-apiserver-openeuler-riscv64
59645e77ddf1a       35b6c407a10ef       6 days ago           Exited              kube-scheduler            0                   ed0d39d0caad6       kube-scheduler-openeuler-riscv64

```

检查集群状态，正常。

```bash
[root@openeuler-riscv64 ~]# kubectl get pods -n kube-system
NAME                                        READY   STATUS    RESTARTS       AGE
coredns-76f75df574-8m497                    0/1     Running   1 (7d1h ago)   6d20h
coredns-76f75df574-d926v                    0/1     Running   1 (7d1h ago)   6d20h
etcd-openeuler-riscv64                      1/1     Running   1 (7d1h ago)   6d20h
kube-apiserver-openeuler-riscv64            1/1     Running   1 (7d1h ago)   6d20h
kube-controller-manager-openeuler-riscv64   1/1     Running   1 (7d1h ago)   6d20h
kube-proxy-xm9p6                            1/1     Running   1 (7d1h ago)   6d20h
kube-scheduler-openeuler-riscv64            1/1     Running   1 (7d1h ago)   6d20h
[root@openeuler-riscv64 ~]# kubectl get nodes
NAME                STATUS   ROLES           AGE     VERSION
openeuler-riscv64   Ready    control-plane   6d20h   v1.29.1
[root@openeuler-riscv64 ~]# kubectl get cs
Warning: v1 ComponentStatus is deprecated in v1.19+
NAME                 STATUS    MESSAGE   ERROR
controller-manager   Healthy   ok
scheduler            Healthy   ok
etcd-0               Healthy   ok
[root@openeuler-riscv64 ~]# kubectl cluster-info
Kubernetes control plane is running at https://10.0.2.15:6443
CoreDNS is running at https://10.0.2.15:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## 7 总结

1）经验证，社区分享的kubernetes 1.29.1的基础镜像可用，现在openEuler2403LTS可以说基本支持kubernetes了。

2）在单机部署测试时，仅master节点即可。有同学提问能否在单机部署node节点，尝试的结果如下：

```bash
kubeadm join 10.0.2.15:6443 --token fajrgb.xcgdogvzt7c5cfcl \
        --discovery-token-ca-cert-hash sha256:b2017778ac7ba38c0e273ff950293123b9201227d46df04ada1d3808db1397bf
```

发现端口10250被kubelet占用

```bash
[root@openeuler-riscv64 ~]# netstat -ntlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 127.0.0.1:2379          0.0.0.0:*               LISTEN      43662/etcd
tcp        0      0 127.0.0.1:2381          0.0.0.0:*               LISTEN      43662/etcd
tcp        0      0 127.0.0.1:10259         0.0.0.0:*               LISTEN      44082/kube-schedule
tcp        0      0 127.0.0.1:10257         0.0.0.0:*               LISTEN      44127/kube-controll
tcp        0      0 127.0.0.1:10248         0.0.0.0:*               LISTEN      1093/kubelet
tcp        0      0 127.0.0.1:10249         0.0.0.0:*               LISTEN      44575/kube-proxy
tcp        0      0 10.0.2.15:2380          0.0.0.0:*               LISTEN      43662/etcd
tcp        0      0 10.0.2.15:2379          0.0.0.0:*               LISTEN      43662/etcd
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      1033/sshd: /usr/sbi
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      880/rpcbind
tcp        0      0 127.0.0.1:37469         0.0.0.0:*               LISTEN      1052/containerd
tcp6       0      0 :::6443                 :::*                    LISTEN      43961/kube-apiserve
tcp6       0      0 :::10250                :::*                    LISTEN      1093/kubelet
tcp6       0      0 :::10256                :::*                    LISTEN      44575/kube-proxy
tcp6       0      0 :::22                   :::*                    LISTEN      1033/sshd: /usr/sbi
tcp6       0      0 :::111                  :::*                    LISTEN      880/rpcbind
```

也就是说，单机的10250端口已经被master节点占用，如要部署node节点，建议在其他设备上部署。

## 8 参考资料

<https://blog.csdn.net/ljx1528/article/details/137714292>

<https://build.tarsier-infra.isrc.ac.cn/package/show/openEuler:24.03:Epol/kubernetes>

<https://www.cnblogs.com/hello-littlebaby/p/18046131>
