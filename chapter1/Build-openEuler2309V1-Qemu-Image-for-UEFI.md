# 操作手册：Build openEuler2309V1 Qemu Image for UEFI EDK2

---

## 1 目标

制作基于UEFI EDK2的openEuler2309V1 Qemu镜像，实现UEFI的正常启动以及openEuler2309V1的正常运行。

## 2 制作过程

### 2.1 准备主机系统环境Ubuntu 22.04

查看系统环境

```bash
riscv@qemu-vm:~/riscv64-linux$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 22.04.3 LTS
Release:	22.04
Codename:	jammy
riscv@qemu-vm:~/riscv64-linux$ riscv64-linux-gnu-gcc --version
riscv64-linux-gnu-gcc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

符号链接python

```bash
riscv@qemu-vm:~/riscv64-linux$ which python3
/usr/bin/python3
riscv@qemu-vm:~/riscv64-linux$ sudo ln -sf /usr/bin/python3 /usr/bin/python
```

### 2.2 准备qemu8.0

进入工作目录，下载qemu8.0，并完成编译。

```bash
sudo git clone --recursive --depth 1 --branch v8.0.0 https://github.com/qemu/qemu.git
cd qemu
sudo mkdir build
cd build
sudo ../configure --target-list=riscv64-softmmu,riscv64-linux-user --prefix=/opt/qemu
sudo make -j $(nproc)
sudo make install
```

添加qemu路径

```bash
sudo nano /etc/profile
export PATH=$PATH:/opt/qemu/bin
source /etc/profile
```

查看qemu版本

```bash
riscv@qemu-vm:/opt/qemu/bin$ qemu-system-riscv64 --version
QEMU emulator version 8.0.0 (v8.0.0)
Copyright (c) 2003-2022 Fabrice Bellard and the QEMU Project developers
```

### 2.3 准备EDK2 stable202305

下载EDK2源码

```bash
sudo git clone --recurse-submodule --branch edk2-stable202305 https://github.com/tianocore/edk2.git
```

设置环境路径，本次工作的内容将全部放在/home/riscv/riscv64-linux/qemu-virt目录中。

```bash
export WORKSPACE=/home/riscv/riscv64-linux/qemu-virt
export GCC5_RISCV64_PREFIX=riscv64-linux-gnu-
export PACKAGES_PATH=$WORKSPACE/edk2
export EDK_TOOLS_PATH=$WORKSPACE/edk2/BaseTools
```

进入工作目录WORKSPACE，设置目录权限，配置脚本

```bash
sudo chmod -R 777 edk2
sudo source edk2/edksetup.sh
sudo mkdir Build
sudo chmod -R 777 Build
```

编译固件

```bash
sudo make -C edk2/BaseTools clean
sudo make -C edk2/BaseTools
sudo make -C edk2/BaseTools/Source/C
source edk2/edksetup.sh BaseTools
sudo build -a RISCV64 --buildtarget RELEASE -p OvmfPkg/RiscVVirt/RiscVVirtQemu.dsc -t GCC5
```

完成编译，并检查编译结果RISCV_VIRT.fd

```bash
GUID cross reference file can be found at /home/riscv/riscv64-linux/qemu-virt/Build/RiscVVirtQemu/RELEASE_GCC5/FV/Guid.xref
FV Space Information
DXEFV [99%Full] 4653056 (0x470000) total, 4642992 (0x46d8b0) used, 10064 (0x2750) free
FVMAIN_COMPACT [11%Full] 7602176 (0x740000) total, 851128 (0xcfcb8) used, 6751048 (0x670348) free
- Done -
Build end time: 14:28:34, Jan.07 2024
Build total time: 00:01:13

riscv@qemu-vm:~/riscv64-linux/qemu-virt$ ll /home/riscv/riscv64-linux/qemu-virt/Build/RiscVVirtQemu/RELEASE_GCC5/FV/
total 20248
drwxrwxr-x  3 riscv riscv    4096  1月  7 14:28 ./
drwxrwxrwx  4 root  root     4096  1月  7 14:28 ../
-rw-rw-r--  1 riscv riscv 4653056  1月  7 14:28 DXEFV.Fv
-rw-rw-r--  1 riscv riscv      86  1月  7 14:28 DXEFV.Fv.map
-rw-rw-r--  1 riscv riscv    3706  1月  7 14:28 DXEFV.Fv.txt
-rw-rw-r--  1 riscv riscv   14440  1月  7 14:28 DXEFV.inf
drwxrwxr-x 80 riscv riscv   12288  1月  7 14:28 Ffs/
-rw-rw-r--  1 riscv riscv      20  1月  7 14:28 FVMAIN_COMPACT.ext
-rw-rw-r--  1 riscv riscv 7602176  1月  7 14:28 FVMAIN_COMPACT.Fv
-rw-rw-r--  1 riscv riscv     291  1月  7 14:28 FVMAIN_COMPACT.Fv.map
-rw-rw-r--  1 riscv riscv     153  1月  7 14:28 FVMAIN_COMPACT.Fv.txt
-rw-rw-r--  1 riscv riscv    1042  1月  7 14:28 FVMAIN_COMPACT.inf
-rw-rw-r--  1 riscv riscv     464  1月  7 14:28 GuidedSectionTools.txt
-rw-rw-r--  1 riscv riscv   21821  1月  7 14:28 Guid.xref
-rw-rw-r--  1 riscv riscv 8388608  1月  7 14:28 RISCV_VIRT.fd
```

设置固件image的大小为32M。

```bash
truncate -s 32M Build/RiscVVirtQemu/RELEASE_GCC5/FV/RISCV_VIRT.fd
```

将编译得到的RISCV_VIRT.fd拷贝到运行目录中（我的运行目录是/mnt/hgfs/img/20231201/qemu-virt）。

```bash
cd /home/riscv/riscv64-linux/qemu-virt/
sudo cp Build/RiscVVirtQemu/RELEASE_GCC5/FV/RISCV_VIRT.fd /mnt/hgfs/img/20231201/qemu-virt/
```

### 2.4 准备openSBI 1.2

从penglai的github仓库下载opensbi 1.2，并完成编译。

```bash
sudo git clone https://github.com/penglai-enclave/penglai-enclave-sPMP
cd penglai-enclave-sPMP
sudo git submodule update --init –recursive
sudo make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv PLATFORM=generic -j $(nproc)
```

将编译得到的fw_dynamic.bin拷贝到运行目录中。

```bash
cd /home/riscv/riscv64-linux/qemu-virt/
sudo cp penglai-enclave-sPMP/opensbi-1.2/build/platform/generic/firmware/fw_dynamic.bin /mnt/hgfs/img/20231201/qemu-virt/
```

### 2.5 准备openEuler的镜像

#### 2.5.1 备份openEuler2309V1 qemu preview 官方镜像的boot文件和rootfs文件

在WORKSPACE工作目录创建tmp等目录。其中boot和rootfs目录用于挂载镜像，boot1和rootfs1用于备份对应目录中的文件。

```bash
sudo mkdir tmp
sudo chmod -R 777 tmp
sudo mkdir -p tmp/boot
sudo mkdir -p tmp/boot1
sudo mkdir -p tmp/rootfs
sudo mkdir -p tmp/rootfs1
```

在https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/QEMU/上，下载镜像，解压缩，并挂载到/dev/nbd1。

```bash
sudo modprobe nbd max_part=8
sudo /opt/qemu/bin/qemu-nbd -c /dev/nbd1 /mnt/hgfs/img/20231201/qemu-virt/openEuler-23.09-V1-xfce-qemu-preview.qcow2
```

将/dev/nbd1中的2个分区分别挂载到相应目录，并备份文件到boot1和rootfs1,然后卸载目录。

```bash
#挂载目录
sudo mount /dev/nbd1p1 /home/riscv/riscv64-linux/qemu-virt/tmp/boot
sudo mount /dev/nbd1p2 /home/riscv/riscv64-linux/qemu-virt/tmp/rootfs
cd /home/riscv/riscv64-linux/qemu-virt/tmp/
sudo cp -rf boot/* boot1/
sudo cp -rf rootfs/* rootfs1/
sync
#修改fstab配置文件，需取消/boot分区挂载设置
sudo sed -i "s/.*boot.*/#&/" rootfs1/etc/fstab 
#卸载目录
sudo umount boot
sudo umount rootfs
#取消镜像挂载
sudo /opt/qemu/bin/qemu-nbd -d /dev/nbd1
```

至此，完成备份openEuler2309V1 qemu preview 官方镜像的boot文件和rootfs文件。

#### 2.5.2 准备kernel 6.4.0-10.1.0并更新备份的boot文件和rootfs文件

安装前置依赖项

```bash
sudo apt install libelf-dev
sudo apt install dwarves
```

进入工作目录/home/riscv/riscv64-linux/qemu-virt，下载kernel源码

```bash
sudo git clone -b '6.4.0-10.1.0' https://gitee.com/openeuler/kernel/
```

将备份的内核配置文件复制为.config，然后清空boot1目录，以备备份自己编译的内核。

```bash
sudo cp tmp/boot1/config-6.4.0-10.1.0.20.oe2309.riscv64 kernel/.config
sudo rm -rf tmp/boot1/*
```

进入kernel目录，编译kernel和modules。

```bash
#清除之前生成的文件
sudo make ARCH=riscv mrproper
#进入kernel配置菜单
sudo make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv menuconfig
#在.config - Linux/riscv 6.4.0 Kernel Configuration的 > Device Drivers > Firmware Drivers > EFI (Extensible Firmware Interface) Support中：
#设置Enable the generic EFI decompressor
#设置EFI Confidential Computing Secret Area Support
#编译kernel
sudo make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv -j $(nproc)
#编译modules
sudo make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv modules
```

![Image](./u1.png)

在kernel目录中新建rootfs目录和boot目录，用于安装modules和kernel

```bash
sudo mkdir rootfs boot
#安装modules到kernel目录中的rootfs目录
sudo make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv modules_install INSTALL_MOD_PATH=./rootfs/
#安装kernel到kernel目录中的boot目录
sudo make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv install INSTALL_PATH=./boot/
#拷贝efi等文件到boot目录
sudo cp arch/riscv/boot/vmlinuz.efi ./boot/
sudo cp arch/riscv/boot/vmlinuz ./boot/
```

进入usr目录，可以看到initramfs_data.cpio

```bash
sudo gzip initramfs_data.cpio
sudo cp initramfs_data.cpio.gz initramfs-6.4.0+.img
#退到kernel目录，复制initramfs到boot目录
sudo cp usr/initramfs-6.4.0+.img ./boot/
```

在kernel目录中的boot目录中，编写startup.nsh，加入如下内容：

```bash
fs0:\vmlinuz.efi root=/dev/vda2 rw console=ttyS0 rootwait selinux=0 earlycon
```

至此，我们可以看到该boot目录中的文件包括：startup.nsh，config-6.4.0+，initramfs-6.4.0+.img，System.map-6.4.0+，vmlinuz，vmlinuz-6.4.0+，vmlinuz.efi。rootfs目录中包括了lib/modules/6.4.0+。

更新备份的boot文件和rootfs文件。

```bash
#进入WORKSPACE工作目录
cd /home/riscv/riscv64-linux/qemu-virt/
#更新备份目录boot1（已清空）
sudo cp kernel/boot/* tmp/boot1/
#更新备份目录rootfs1
sudo cp -rf kernel/rootfs/lib/modules/6.4.0+ tmp/rootfs1/lib/modules/
```

#### 2.5.3 制作openEuler-23.09-V1-xfce-qemu-uefi.qcow2

在运行目录（/mnt/hgfs/img/20231201/qemu-virt）中创建20G的openEuler-23.09-V1-xfce-qemu-uefi.qcow2

```bash
qemu-img create -f qcow2 openEuler-23.09-V1-xfce-qemu-uefi.qcow2 20G
```

挂载该qcow2镜像，并分区

```bash
sudo modprobe nbd max_part=8
sudo /opt/qemu/bin/qemu-nbd -c /dev/nbd1 /mnt/hgfs/img/20231201/qemu-virt/openEuler-23.09-V1-xfce-qemu-uefi.qcow2 
sudo gdisk /dev/nbd1
```

分区操作如下：

```bash
riscv@qemu-vm:/mnt/hgfs/img/20231201/qemu-virt$ sudo gdisk /dev/nbd1
GPT fdisk (gdisk) version 1.0.8

Partition table scan:
  MBR: not present
  BSD: not present
  APM: not present
  GPT: not present

Creating new GPT entries in memory.

Command (? for help): p
Disk /dev/nbd1: 41943040 sectors, 20.0 GiB
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 13516879-036D-4C64-89B4-F2D65A2AEDD7
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 41943006
Partitions will be aligned on 2048-sector boundaries
Total free space is 41942973 sectors (20.0 GiB)

Number  Start (sector)    End (sector)  Size       Code  Name

Command (? for help): ?
b	back up GPT data to a file
c	change a partition's name
d	delete a partition
i	show detailed information on a partition
l	list known partition types
n	add a new partition
o	create a new empty GUID partition table (GPT)
p	print the partition table
q	quit without saving changes
r	recovery and transformation options (experts only)
s	sort partitions
t	change a partition's type code
v	verify disk
w	write table to disk and exit
x	extra functionality (experts only)
?	print this menu

Command (? for help): o
This option deletes all partitions and creates a new protective MBR.
Proceed? (Y/N): y 

Command (? for help): n
Partition number (1-128, default 1): 
First sector (34-41943006, default = 2048) or {+-}size{KMGTP}: 
Last sector (2048-41943006, default = 41943006) or {+-}size{KMGTP}: 1048542
Current type is 8300 (Linux filesystem)
Hex code or GUID (L to show codes, Enter = 8300): EF00
Changed type of partition to 'EFI system partition'

Command (? for help): p
Disk /dev/nbd1: 41943040 sectors, 20.0 GiB
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 1CB6D839-B441-478B-90F7-C5791F669E73
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 41943006
Partitions will be aligned on 2048-sector boundaries
Total free space is 40896478 sectors (19.5 GiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048         1048542   511.0 MiB   EF00  EFI system partition

Command (? for help): n
Partition number (2-128, default 2): 
First sector (34-41943006, default = 1048576) or {+-}size{KMGTP}: 
Last sector (1048576-41943006, default = 41943006) or {+-}size{KMGTP}: 
Current type is 8300 (Linux filesystem)
Hex code or GUID (L to show codes, Enter = 8300): 
Changed type of partition to 'Linux filesystem'

Command (? for help): p
Disk /dev/nbd1: 41943040 sectors, 20.0 GiB
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 1CB6D839-B441-478B-90F7-C5791F669E73
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 41943006
Partitions will be aligned on 2048-sector boundaries
Total free space is 2047 sectors (1023.5 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048         1048542   511.0 MiB   EF00  EFI system partition
   2         1048576        41943006   19.5 GiB    8300  Linux filesystem

Command (? for help): w

Final checks complete. About to write GPT data. THIS WILL OVERWRITE EXISTING
PARTITIONS!!

Do you want to proceed? (Y/N): y
OK; writing new GUID partition table (GPT) to /dev/nbd1.
The operation has completed successfully.
```

对分区格式化

```bash
sudo mkfs.fat -F32 /dev/nbd1p1
sudo mkfs.ext4 -L rootfs /dev/nbd1p2
```

至此镜像文件已经创建完毕，开始挂载并复制镜像内容。

```bash
#挂载镜像
sudo mount /dev/nbd1p1 /home/riscv/riscv64-linux/qemu-virt/tmp/boot
sudo mount /dev/nbd1p2 /home/riscv/riscv64-linux/qemu-virt/tmp/rootfs
#复制备份的boot1和rootfs1的内容到对应的目录
cd /home/riscv/riscv64-linux/qemu-virt/tmp/
sudo cp boot1/* boot/
sudo cp -rf rootfs1/* /rootfs/
sync
#取消挂载
sudo umount boot
sudo umount rootfs
sudo /opt/qemu/bin/qemu-nbd -d /dev/nbd1
```

至此，openEuler-23.09-V1-xfce-qemu-uefi.qcow2镜像文件创建完毕。

## 3 运行测试

在运行目录中，执行以下命令，也可编辑成一个start.sh文件：

```bash
qemu-system-riscv64 \
-bios ./fw_dynamic.bin \
-nographic -machine virt -machine acpi=off \
-m 4G -smp 4 \
-display sdl \
-object rng-random,filename=/dev/urandom,id=rng0 \
-device virtio-vga -device virtio-rng-device,rng=rng0 \
-drive file=./RISCV_VIRT.fd,if=pflash,format=raw,unit=1 \
-device virtio-blk-device,drive=hd0 \
-drive file=./openEuler-23.09-V1-xfce-qemu-uefi.qcow2,format=qcow2,id=hd0 \
-device virtio-net-device,netdev=usernet -netdev user,id=usernet,hostfwd=tcp::12055-:22 -device qemu-xhci -usb -device usb-kbd -device usb-tablet
```

![Image](./u6.png)

成功运行。

![Image](./u2.png)

![Image](./u3.png)

## 4 总结

1、这次实验成功用uefi edk2启动openeuler2309V1，但没有用到grub.cfg，而是在uefi shell下直接执行fs0:\vmlinuz.efi root=/dev/vda2 rw console=ttyS0 rootwait selinux=0 earlycon。这是因为vmlinuz.efi执行完后，不会自动加载grub.cfg。这和之前的情况不同，具体原因还有待研究。

2、考虑到为penglai TEE的集成，继续沿用kernel 6.1版本的efi文件已经不太合适了。因此这次实验采用了自行编译的efi文件，在openEuler2309V1的基础上打开了内核对Confidential Computing Secret Area的支持。也因此带来了新的问题，在此基础上如果没有关闭Selinux，那么会出现如下问题，需要后续设置Selinux和Penglai TEE，进一步实验。

![Image](./u4.png)

3、这个版本的镜像能够开展Penglai TEE的相关实验，但应该是没怎么设置的原因，还是会出现如下警告：

![Image](./u5.png)

## 5 参考资料

https://github.com/vlsunil/riscv-uefi-edk2-docs/wiki/RISC-V-Qemu-Virt-support

https://github.com/vlsunil/riscv-uefi-edk2-docs

https://github.com/tianocore/tianocore.github.io/wiki/Getting-Started-with-EDK-II
