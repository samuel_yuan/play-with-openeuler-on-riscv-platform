# 操作手册：Bring up openeuler 2309V1 on leapfive poros by onboard emmc

---

## 1 目标

通过板载emmc，使用支持LeapFive Poros开发板的openeuler2309V1镜像启动开发板。

## 2 准备工作

接上篇教程[《Bring up openeuler 2309V1 on leapfive poros by sdcard》](./Bring-up-openeuler-2309V1-on-leapfive-poros-sdcard.md)

准备一个USB读卡器，将上篇教程中的sd卡放入，接到ubuntu主机上

![Image](./emmc1.png)

如上图所示，使用gparted工具调整sd卡分区大小，将未分配的空间全部分配到文件系统所在分区

```bash
sudo gparted
```

然后将openeuler2309的rootfs压缩包拷贝至sd卡的文件系统分区中。

```bash
sudo cp openeuler-rootfs-xfce.tar.gz /media/riscv/5f9aa1a2-5e9d-47b7-ae29-7795777e4eb3/
sync
```

## 3 复制文件系统到板载emmc上

使用sd卡启动openeuler系统，查看mmc设备

```bash
[root@openeuler-riscv64 ~]# ls /dev/mmc*
/dev/mmcblk0       /dev/mmcblk0p1    /dev/mmcblk1
/dev/mmcblk0boot0  /dev/mmcblk0p2    /dev/mmcblk1p1
/dev/mmcblk0boot1  /dev/mmcblk0rpmb  /dev/mmcblk1p2
```

其中mmcblk0即为板载emmc，其中/dev/mmcblk0p2放的文件系统，挂载目录。

```bash
sudo mkdir rootfs
sudo mount /dev/mmcblk0p2 rootfs/
```

进入rootfs目录，可以看到板载emmc上的文件系统，让我们将其全部删除。

```bash
[root@openeuler-riscv64 rootfs]# ls
bin   dev  home  lost+found  mnt  proc  sbin  tmp  var
boot  etc  lib   media       opt  run   sys   usr
[root@openeuler-riscv64 rootfs]# sudo rm -rf *
```

将openeuler-rootfs-xfce.tar.gz解压缩至rootfs中。

```bash
[root@openeuler-riscv64 ~]# sudo tar -xvzf /openeuler-rootfs-xfce.tar.gz -C rootfs/
```

卸载rootfs

```bash
sudo umount rootfs
```

至此板载emmc上的文件系统复制完毕。

## 4 启动

按照上篇教程中uboot环境配置的启动顺序为sdboot、usbboot、mmcboot。在没有插入带系统的sd卡或u盘时，系统将从板载emmc上启动。

![Image](./poros-openeuler2309V1.png)
