# 操作手册：RVV Program and openEuler2403 A

---

## 1 目标

A 学习rvv intrinsic程序的编译和运行，并分别在仿真环境qemu和spike中运行。

B 学习rvv intrinsic程序，对比普通阵列加法和rvv阵列加法，并分别在仿真环境和openEuler环境中运行验证。

本篇教程内容聚焦A

## 2 准备工作

实验主机的系统为Ubuntu22.04

### 2.1 riscv-gnu-toolchain工具链的安装

riscv的gcc编译器，分为2大类：裸机： unknown-elf，none-embed；linux：linux。除了none-embed编译器，对于每一类，如果禁用multilib，那么又分为32位版本和64位版本。如果使能multilib，那么就只有一个版本，但是这个版本工具，可以同时支持32位和64位。编译配置中的--with-abi-xxx的xxx可以选择：lp64、lp64f、lp64d

下载riscv-gnu-toolchain工具链的2024.12.16分支，这应该比较新的版本了。

```bash
git clone -b 2024.12.16 https://github.com/riscv-collab/riscv-gnu-toolchain
cd riscv-gnu-toolchain
git submodule update --init --recursive
```

创建构建目录和安装目录

```bash
mkdir build && cd build
mkdir -p /opt/riscv64
```

编译riscv64-unknown-elf-gcc，该工具针对于riscv64架构的编译器，使用的C运行库为newlib

```bash
../configure --prefix=/opt/riscv64 --enable-multilib --enable-languages=c,c++ --with-arch=rv64gcv --with-abi=lp64 --enable-rvv
sudo make -j $(nproc)
```

编译riscv64-unknown-linux-gnu-gcc，该工具针对于riscv64架构的编译器，使用的C运行库为linux中的标准glibc

```bash
../configure --prefix=/opt/riscv64 --with-arch=rv64gcv --with-abi=lp64 --enable-multilib --enable-linux
sudo make linux -j $(nproc)
```

配置环境变量

```bash
sudo vim /etc/profile
```

添加路径

```bash
export PATH=$PATH:/opt/riscv64/bin
```

生效

```bash
source /etc/profile
```

查看riscv64-unknown-elf-gcc版本

```bash
riscv@qemu-vm:~/riscv64-linux/riscv-gnu-toolchain/build$ riscv64-unknown-elf-gcc --version
riscv64-unknown-elf-gcc (g04696df0963) 14.2.0
Copyright (C) 2024 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

查看riscv64-unknown-linux-gnu-gcc版本

```bash
riscv@qemu-vm:~/riscv64-linux/riscv-gnu-toolchain/build$ riscv64-unknown-linux-gnu-gcc --version
riscv64-unknown-linux-gnu-gcc (g04696df0963) 14.2.0
Copyright (C) 2024 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

### 2.2 qemu模拟器的安装

安装依赖

```bash
sudo apt install python3-venv
sudo apt install python3-pip
sudo pip install tomli
```

下载qemu模拟器，9.1版本支持RVA23

```bash
wget https://download.qemu.org/qemu-9.1.0.tar.xz
tar xvJf qemu-9.1.0.tar.xz
cd qemu-9.1.0
```

配置编译及安装

```bash
./configure --prefix=/opt/qemu --target-list=riscv64-softmmu,riscv64-linux-user
make -j $(nproc)
sudo make install
```

配置环境变量

```bash
sudo vim /etc/profile
```

添加路径

```bash
export PATH=$PATH:/opt/qemu/bin
```

生效

```bash
source /etc/profile
```

查看版本

```bash
riscv@qemu-vm:~/riscv64-linux/qemu-9.1.0$ qemu-system-riscv64 --version
QEMU emulator version 9.1.0
Copyright (c) 2003-2024 Fabrice Bellard and the QEMU Project developers
```

### 2.3 spike模拟器的安装

安装依赖

```bash
sudo apt update
sudo apt install -y autoconf automake libtool g++ pkg-config make python3
sudo apt install device-tree-compiler libboost-regex-dev libboost-system-dev
```

下载spike

```bash
git clone https://github.com/riscv/riscv-isa-sim.git
```

编译安装spike

```bash
cd riscv-isa-sim
mkdir build
cd build
../configure --prefix=/opt/riscv --enable-rvv
make
sudo make install
```

配置环境变量

```bash
vim /etc/profile
```

添加路径

```bash
export PATH=$PATH:/opt/riscv/bin
```

生效

```bash
source /etc/profile
```

### 2.4 安装PK

为了让Spike正确执行成程序，需要安装RISCV-V Proxy Kernel(pk)。下载pk：

```bash
cd ~/riscv64-linux/
git clone https://github.com/riscv/riscv-pk.git
cd riscv-pk
mkdir build
cd build
```

针对于riscv64架构的编译器，使用的C运行库为linux中的标准glibc

```bash
../configure --prefix=/opt/riscv --host=riscv64-unknown-linux-gnu
```

针对于riscv64架构的编译器，使用的C运行库为linux中的标准newlib（可选，但不建议，该C运行库对spike的支持不好，所以本教材不采用这种方式）

```bash
../configure --prefix=/opt/riscv --host=riscv64-unknown-elf
```

编辑makefile，启用 zifencei 扩展，在CFLAGS中添加 -march=rv64imafdc_zifencei

```bash
vim Makefile
```

在CFLAGS中添加

```bash
CFLAGS        :=-march=rv64imafdc_zifencei -Wall -Werror -D__NO_INLINE__ -mcmodel=medany -O2 -std=gnu99 -Wno-unused -Wno-attributes -fno-delete-null-pointer-checks -fno-PIE $(CFLAGS) $(march) $(mabi) -DBBL_LOGO_FILE=\"bbl_logo_file\" -DMEM_START=0x80000000 -fno-stack-protector -U_FORTIFY_SOURCE
```

编译和安装

```bash
make
sudo make install
```

注意：spike在对应工具链的pk上运行的应用程序，必须是静态编译，编译参数需要-static参数。

## 3 RVV intrinsic 程序 - Vector add

进入工作目录

```bash
cd ~/riscv64-linux/
sudo mkdir -p rvv_test
#更改目录权限
sudo chmod 777 -R ~/riscv64-linux/rvv_test
cd rvv_test/
```

编写Vector add 程序

```bash
sudo vim vadd.c
```

添加代码

```c
// Reference: https://pages.dogdog.run/toolchain/riscv_vector_extension.html
// #define __riscv_vector // make VSC happy when reading riscv_vector.h
#include <riscv_vector.h>
#include <stdio.h>

int x[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
int y[10] = {0, 9, 8, 7, 6, 5, 4, 3, 2, 1};
int z[10], z_real[10];

void vec_add_rvv(int* dst, int* lhs, int* rhs, size_t avl) {
    vint32m2_t vlhs, vrhs, vres;
    for (size_t vl; (vl = __riscv_vsetvl_e32m2(avl));
         avl -= vl, lhs += vl, rhs += vl, dst += vl) {
        vlhs = __riscv_vle32_v_i32m2(lhs, vl);
        vrhs = __riscv_vle32_v_i32m2(rhs, vl);
        vres = __riscv_vadd_vv_i32m2(vlhs, vrhs, vl);
        __riscv_vse32_v_i32m2(dst, vres, vl);
    }
}

void vec_add_real(int* dest, int* lhs, int* rhs, size_t vlen) {
    for (size_t i = 0; i < vlen; i++) {
        dest[i] = lhs[i] + rhs[i];
    }
}

void print_vec(int* v, size_t vlen, char* msg) {
    printf("%s={ ", msg);
    for (size_t i = 0; i < vlen; i++) {
        printf("%d, ", v[i]);
    }

    printf("}\n");
}

int main(int argc, char const* argv[]) {

// check RVV support
#ifndef __riscv_v_intrinsic
    printf("RVV NOT supported in this compiler\n");
    return 0;
#endif

    vec_add_rvv(z, x, y, 10);
    vec_add_real(z_real, x, y, 10);

    print_vec(x, 10, "x[10]");
    print_vec(y, 10, "y[10]");

    for (size_t i = 0; i < 10; i++) {
        if (z[i] != z_real[i]) {
            printf("==========\nTest FAILED: pos %d mismatch\n", i);
            print_vec(z, 10, "z[10]");
            print_vec(z_real, 10, "z_real[10]");

            return -1;
        }
    }

    print_vec(z, 10, "z[10]");
    printf("==========\nTest PASSED\n");

    return 0;
}
```

采用riscv64-unknown-linux-gnu-gcc编译代码

```bash
riscv64-unknown-linux-gnu-gcc -march=rv64gcv -O2 -g -static ./vadd.c -o ./vadd
```

采用riscv64-unknown-elf-gcc编译代码（可选，但不建议，编译的代码仅qemu可运行，spike则不可运行）

```bash
riscv64-unknown-elf-gcc -march=rv64gcv -O2 -g -static ./vadd.c -o ./vadd
```

采用qemu模拟器运行

```bash
riscv@qemu-vm:~/riscv64-linux/rvv_test$ qemu-riscv64 vadd
x[10]={ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, }
y[10]={ 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, }
z[10]={ 1, 11, 11, 11, 11, 11, 11, 11, 11, 1, }
==========
Test PASSED
```

![Image](./rvv-qemu.png)

采用spike模拟器运行

```bash
riscv@qemu-vm:~/riscv64-linux/rvv_test$ spike --isa=rv64gcv /opt/riscv/riscv64-unknown-linux-gnu/bin/pk ./vadd
x[10]={ 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, }
y[10]={ 0, 9, 8, 7, 6, 5, 4, 3, 2, 1, }
z[10]={ 1, 11, 11, 11, 11, 11, 11, 11, 11, 1, }
==========
Test PASSED
```

![Image](./rvv-spike.png)

## 4 小结

1）建议使用riscv64-unknown-linux-gnu-gcc来编译，支持glibc，对模拟器友好一些。

## 5 参考资料

https://blog.51cto.com/u_16213591/9767474

https://github.com/riscv-non-isa/rvv-intrinsic-doc/tree/v1.0.x/examples
