# 操作手册：Network configuration for openEuler 2309V1 on Pioneer

---

## 1 目标

我们配置网络时，通常会编辑/etc/sysconfig/network-scripts/中的ifcfg配置文件，然而openEuler 2309V1中默认是没有ifcfg配置文件的。为了配置固定IP地址，有人通过文本工具创建网络配置文件，有人通过网管工具创建新的网络连接来创建网络配置文件，然而这些方法其实是有风险的，配置过程中也可能存在错误或者警告，运行过程中也可能带来掉线的风险。与此同时，这些方法大量存在于网络文章之中，混淆视听，甚至有些大佬有时也会掉到频繁掉线的问题中。本教程在此给出标准的做法，即在MilkV Pioneer上的openEuler2309V1正确配置网络接口的固定IP地址。

## 2 配置

1）显示DHCP分配的IP地址

```bash
[root@openeuler-riscv64 ~]# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enP3p197s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:68:00:17 brd ff:ff:ff:ff:ff:ff
3: enP3p201s0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:68:00:18 brd ff:ff:ff:ff:ff:ff
4: enP2p129s0f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether a0:36:9f:54:71:94 brd ff:ff:ff:ff:ff:ff
    inet 192.168.20.70/24 brd 192.168.20.255 scope global dynamic noprefixroute enP2p129s0f0
       valid_lft 84971sec preferred_lft 84971sec
    inet6 fe80::6a5d:934d:dffa:5537/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
5: enP2p129s0f1: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc mq state DOWN group default qlen 1000
    link/ether a0:36:9f:54:71:96 brd ff:ff:ff:ff:ff:ff
......
```

2）显示网络接口设备状态，下述信息说明openEuler的Pioneer镜像会默认创建几个网络连接，其中网络连接Wired connection 1对应指定的网络接口名称为enP2p129s0f0。

```bash
[root@openeuler-riscv64 ~]# nmcli -p device
=====================
  Status of devices
=====================
DEVICE        TYPE      STATE                   CONNECTION
-----------------------------------------------------------------------------------------
enP2p129s0f0  ethernet  connected               Wired connection 1
lo            loopback  connected (externally)  lo
docker0       bridge    connected (externally)  docker0
virbr0        bridge    connected (externally)  virbr0
enP2p129s0f1  ethernet  unavailable             --
enP3p197s0    ethernet  unavailable             --
enP3p201s0    ethernet  unavailable             --
virbr0-nic    tun       unmanaged               --
```

3）查看Wired connection 1网络链接的具体情况。

```bash
[root@openeuler-riscv64 ~]# nmcli con show 'Wired connection 1'
connection.id:                          Wired connection 1
connection.uuid:                        949f96f7-7e06-3023-84b8-88df03a9b51f
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              enP2p129s0f0
connection.autoconnect:                 yes
......
ipv4.method:                            auto
ipv4.dns:                               --
ipv4.addresses:                         --
ipv4.gateway:                           --
......
IP4.ADDRESS[1]:                         192.168.20.70/24
IP4.GATEWAY:                            192.168.20.1
IP4.ROUTE[1]:                           dst = 192.168.20.0/24, nh = 0.0.0.0, mt = 100
IP4.ROUTE[2]:                           dst = 0.0.0.0/0, nh = 192.168.20.1, mt = 100
IP4.DNS[1]:                             8.8.8.8
......
```

4）进入网络配置脚本目录，可以看到默认情况下没有网络配置文件。

```bash
[root@openeuler ~]# cd /etc/sysconfig/network-scripts/
[root@openeuler network-scripts]# ls
[root@openeuler network-scripts]#
```

5）使用nmcli来生成网络Wired connection 1的配置文件ifcfg-Wired_connection_1

```bash
[root@openeuler-riscv64 network-scripts]# nmcli con modify 'Wired connection 1' ipv4.method manual ipv4.addresses 192.168.20.101/24 ipv4.gateway 192.168.20.1 ipv4.dns 8.8.8.8
[root@openeuler-riscv64 network-scripts]# nmcli con show 'Wired connection 1'
connection.id:                          Wired connection 1
connection.uuid:                        949f96f7-7e06-3023-84b8-88df03a9b51f
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              enP2p129s0f0
connection.autoconnect:                 yes
......
ipv4.method:                            manual
ipv4.dns:                               8.8.8.8
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
ipv4.addresses:                         192.168.20.101/24
ipv4.gateway:                           192.168.20.1
......
```

可以看到网络配置生效了，然后再来看配置文件已经成功生成。（实际上，将ipv4.method设置为manual就会生成这个ifcfg文件）

```bash
[root@openeuler-riscv64 network-scripts]# ls
ifcfg-Wired_connection_1
```

查看配置文件

```bash
[root@openeuler-riscv64 network-scripts]# cat ifcfg-Wired_connection_1
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
IPADDR=192.168.20.101
PREFIX=24
GATEWAY=192.168.20.1
DNS1=8.8.8.8
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=default
NAME="Wired connection 1"
UUID=949f96f7-7e06-3023-84b8-88df03a9b51f
DEVICE=enP2p129s0f0
ONBOOT=yes
AUTOCONNECT_PRIORITY=-999
```

重启，修改成功。

## 3 小结

看上去简单的事情，有时候会不简单。

## 4 参考

https://blog.csdn.net/phoenixFlyzzz/article/details/136093901

https://blog.csdn.net/cuichongxin/article/details/134316966

https://blog.csdn.net/m0_74367891/article/details/137141654

https://blog.51cto.com/u_16175464/9260371