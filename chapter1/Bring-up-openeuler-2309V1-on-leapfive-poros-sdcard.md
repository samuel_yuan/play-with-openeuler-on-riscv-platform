# 操作手册：Bring up openeuler 2309V1 on leapfive poros by sdcard

---

## 1 目标

通过sd卡，使用支持LeapFive Poros开发板的openeuler2309V1镜像启动开发板。

## 2 准备工作

### 2.1 外设连接

![Image](./poros2.png)

### 2.2 系统镜像

镜像openEuler-23.09-V1-base-poros-preview.img和openEuler-23.09-V1-xfce-poros-preview.img在以下地址下载

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/preview/openEuler-23.09-V1-riscv64/

## 3 uboot环境设置

MobaXterm通过COM口连上poros开发板

![Image](./poros1.png)

如上图所示在自启动倒计时这里，按键进入uboot命令行。打印当前环境配置，我们看到在默认配置时是按照板载emmc、sd卡、usb盘的启动顺序来启动。

```bash
=> print
arch=riscv
baudrate=115200
board=poros
board_name=poros
bootargs_mmc=setenv bootargs 'console=ttyS0,115200n8 console=tty1 root=/dev/mmcblk0p2 rootwait rw rootfstype=ext4 rdinit=/sbin/init'
bootargs_ram=setenv bootargs 'console=ttyS0,115200n8 console=tty1 root=/dev/ram0 rw rdinit=/sbin/init'
bootargs_sd=setenv bootargs 'console=ttyS0,115200n8 root=/dev/mmcblk1p1 rootwait rw rootfstype=ext4 rdinit=/sbin/init'
bootargs_tftp=setenv bootargs 'console=ttyS0,115200n8 console=tty1 root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait rdinit=/sbin/init'
bootargs_usb=setenv bootargs 'console=ttyS0,115200n8 root=/dev/sda1 rootwait rw rootfstype=ext4 rdinit=/sbin/init'
bootcmd=run $modeboot; run sdboot; run usbboot; run mmcboot
bootdelay=5
cpu=generic
fdt_addr_r=0x80f000000
fdt_high=0xffffffffffffffff
fdtcontroladdr=8ff5e2980
initrd_high=0xffffffffffffffff
kernel_addr_r=0x80a000000
mmcboot=run bootargs_mmc ; fatload mmc 0:1 ${kernel_addr_r} Image; fatload mmc 0:1 ${fdt_addr_r} nb2-poros.dtb;booti ${kernel_addr_r} - ${fdt_addr_r}
modeboot=mmcboot
preboot=setenv fdt_addr ${fdtcontroladdr};fdt addr ${fdtcontroladdr};
pxefile_addr_r=0x88200000
ramboot=run bootargs_ram ; booti ${kernel_addr_r} - ${fdt_addr_r}
ramdisk_addr_r=0x88300000
scriptaddr=0x88100000
sdboot=run bootargs_sd ; ext4load mmc 1 ${kernel_addr_r} boot/Image;ext4load mmc 1 ${fdt_addr_r} boot/nb2-poros.dtb;booti ${kernel_addr_r} - ${fdt_addr_r}
spiboot=run bootargs_mmc ; sf probe;sf read ${kernel_addr_r} 0x200000 0x1d00000;sf read ${fdt_addr_r} 0x180000 0x10000; booti ${kernel_addr_r} - ${fdt_addr_r}
stderr=serial@4F0009000
stdin=serial@4F0009000
stdout=serial@4F0009000
tftp_boot=run bootargs_tftp ; booti ${kernel_addr_r} - ${fdt_addr_r}
usb_boot=usb start;setenv load_addr_r 0x808000000;ext4load usb 0 ${load_addr_r} boot/boot.scr;source ${load_addr_r}
usbboot=run bootargs_usb ; usb reset;ext4load usb 0 ${kernel_addr_r} boot/Image;ext4load usb 0 ${fdt_addr_r} boot/nb2-poros.dtb;booti ${kernel_addr_r} - ${fdt_addr_r}
vendor=leapfive

Environment size: 2008/131068 bytes
```

考虑到我们玩开发板，换系统频繁，这里采用sd卡的启动方式，因此需要修改环境配置。考虑到我的镜像中分了2个区，分别为boot分区和rootfs分区，对应mmcblk1p1和mmcblk1p2。所以这里要改文件系统所在为mmcblk1p2.

```bash
setenv bootargs_sd 'setenv bootargs "console=ttyS0,115200n8 console=tty0 root=/dev/mmcblk1p2 rw rootfstype=ext4 rootwait rdinit=/sbin/init"'
```

考虑到mmc 0被板载emmc占用，因此这里sd卡使用mmc1，即对应mmcblk1。这里可以看到设备树文件和kernel image的路径和名称，可自行设置。我这按最简单的来。

```bash
setenv sdboot 'run bootargs_sd ; fatload mmc 1:1 ${kernel_addr_r} Image;fatload mmc 1:1 ${fdt_addr_r} nb2-poros.dtb;booti ${kernel_addr_r} - ${fdt_addr_r}'
```

考虑到从sd卡启动，修改启动顺序。

```bash
setenv bootcmd 'run sdboot; run usbboot; run mmcboot'
```

环境配置保存。

```bash
saveenv
```

## 4 安装镜像

下载烧录工具balenaEtcher。

```bash
wget https://github.com/balena-io/etcher/releases/download/v1.7.9/balena-etcher-electron-1.7.9-linux-x64.zip
```

安装解压工具，并解压

```bash
sudo apt install zip unzip
sudo unzip balena-etcher-electron-1.7.9-linux-x64.zip
```

运行balenaEtcher

```bash
sudo ./balenaEtcher-1.7.9-x64.AppImage
```

![Image](./poros3.png)

如图所示，选择openEuler-23.09-V1-xfce-poros-preview.img镜像文件，选择sd卡的分区，点击Flash开始烧录系统到sd卡上。

![Image](./poros4.png)

如图所示，系统已完成烧录

## 5 启动

将sd卡插入poros开发板上的对应槽位，启动系统。输入root账号，和密码openEuler12#$进入系统

![Image](./poros-openeuler2309V1.png)
