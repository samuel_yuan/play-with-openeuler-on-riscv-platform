# 操作手册：Test Penglai TEE on openEuler2309V1 Qemu

---

## 1 目标

基于UEFI EDK2的openEuler2309V1 Qemu镜像，测试Penglai TEE。Penglai主要部件为：opensbi, driver，sdk，secGear，其中opensbi使用交叉编译，其他使用本地编译。考虑到之前的教程镜像中已经有opensbi 1.2，因此这里不赘述，只讲driver，sdk，secGear。考虑到这里的secGear有两种处理方法，在本文中暂不说明，后续留专门的章节来说明；本文只说明penglai-enclave-driver，penglai-sdk的测试情况。

## 2 准备工作

安装环境依赖

系统环境，请下载基于UEFI EDK2的openEuler2309V1 Qemu镜像（参考之前教程制作的镜像, https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/community/samuel_yuan/openeuler2309/qemu-uefi/ ）

启动系统，安装必要的依赖项目

```bash
dnf install -y kernel-devel kernel-source openssl-devel cmake g++
```

检查kernel src安装的目录

```bash
[root@openeuler-riscv64 ~]# cd /usr/src/kernels/
[root@openeuler-riscv64 kernels]# ls
6.4.0-10.1.0.20.oe2309.riscv64
```

修复kernel的module，考虑到之前制作的qemu镜像中为了支持uefi对kernel进行了重新编译，所以这里的build是一个失效的连接，应改为正确的连接。

```bash
[root@openeuler-riscv64 kernels]# cd /lib/modules/6.4.0+/
[root@openeuler-riscv64 6.4.0+]# file build
build: broken symbolic link to /home/riscv/riscv64-linux/qemu-virt/kernel
[root@openeuler-riscv64 6.4.0+]# ls -l build
lrwxrwxrwx 1 root root 42 Jan  9 19:09 build -> /home/riscv/riscv64-linux/qemu-virt/kernel
[root@openeuler-riscv64 6.4.0+]# ln -sf /usr/src/kernels/6.4.0-10.1.0.20.oe2309.riscv64 build
[root@openeuler-riscv64 6.4.0+]# ls -l build
lrwxrwxrwx 1 root root 47 May 30 20:04 build -> /usr/src/kernels/6.4.0-10.1.0.20.oe2309.riscv64
```

## 3 测试penglai-enclave-driver

下载Penglai-Enclave-sPMP

```bash
git clone -recursive https://github.com/penglai-enclave/penglai-enclave-sPMP
```

修改Makefile，将其中的make -C ../openeuler-kernel/ ARCH=riscv M=$(PWD) modules改为make -C /usr/lib/modules/$(shell uname -r)/build ARCH=riscv M=$(PWD) modules，具体如下：

```bash
##
## Author: Dong Du
## Description:
##      The file now assumes the kernel located in ../openeuler-kernel,
##      if you would like to use your own one, please change the path

obj-m += penglai.o
penglai-objs := penglai-enclave-driver.o \
        penglai-enclave-elfloader.o \
        penglai-enclave-page.o \
        penglai-enclave.o \
        penglai-enclave-ioctl.o

all:
        make -C /usr/lib/modules/$(shell uname -r)/build ARCH=riscv M=$(PWD) modules

clean:
        make -C ../openeuler-kernel/ ARCH=riscv M=$(PWD) clean
```

编译

```bash
make -j$(nproc)
```

![Image](./tee1.png)

![Image](./tee2.png)

加载penglai内核模块penglai.ko

```bash
[root@openeuler-riscv64 penglai-enclave-driver]# sudo insmod penglai.ko
[10336.294618][ T2297] enclave_ioctl_init...
[10336.296510][ T2297] [Penglai KModule] register_chrdev succeeded!
```

卸载penglai内核模块（熟悉操作）

```bash
[root@openeuler-riscv64 penglai-enclave-driver]# sudo rmmod penglai
[10365.301497][ T2303] enclave_ioctl_exit...
[10365.302073][ T2303] KERNEL MODULE:  free secmem:paddr:103800000, vaddr:ff60000083800000, order:22
M mode:Finish free and there is no mem to reclaim
```

再次加载

```bash
[root@openeuler-riscv64 penglai-enclave-driver]# sudo insmod penglai.ko
[10336.294618][ T2297] enclave_ioctl_init...
[10336.296510][ T2297] [Penglai KModule] register_chrdev succeeded!
```

查看penglai内核模块，命令返回结果，说明该模块已经加载成功。

```bash
[root@openeuler-riscv64 penglai-enclave-driver]# lsmod | grep penglai
penglai               249856  0
```

![Image](./tee3.png)

## 4 测试penglai-sdk（测试2403这个分支）

### 4.1 编译penglai-sdk

下载sdk

```bash
cd ~
git clone -b oe-24.03 https://github.com/Penglai-Enclave/penglai-sdk.git
```

编译sdk

```bash
[root@openeuler-riscv64 ~]# cd penglai-sdk
[root@openeuler-riscv64 penglai-sdk]# make -j$(nproc)
```

将sdk安装到/opt/penglai的目录中

```bash
[root@openeuler-riscv64 penglai-sdk]# make DESTDIR=/opt/penglai install
mkdir -p /opt/penglai/lib
mkdir -p /opt/penglai/runtime
mkdir -p /opt/penglai/bin
find lib -name "*.a" -exec cp --parent {} /opt/penglai/ \;
find lib -name "*.h" -exec cp --parent {} /opt/penglai/ \;
cp -r penglai_sdk_ssl /opt/penglai/
cp -r runtime/lib /opt/penglai/runtime
cp -r penglai-host/penglai-host /opt/penglai/bin
chmod +x /opt/penglai/bin/penglai-host
cp app.* /opt/penglai
```

成功完成编译

![Image](./tee4.png)

![Image](./tee5.png)

### 4.2 测试penglai-sdk的penglai demo

取得Penglai-demo，下述命令可以取得子模块Penglai-demo和secGear，但这里的secGear源并非为riscv所准备的，这里暂且不讨论secGear。

```bash
[root@openeuler-riscv64 penglai-sdk]# git submodule update --init --recursive
Submodule 'Penglai-demo' (https://github.com/Penglai-Enclave/Penglai-demo.git) registered for path 'Penglai-demo'
Submodule 'secGear' (https://github.com/Penglai-Enclave/Penglai-secGear.git) registered for path 'secGear'
Cloning into '/root/penglai-sdk/Penglai-demo'...
Cloning into '/root/penglai-sdk/secGear'...
Submodule path 'Penglai-demo': checked out '99039a18b5dfd58015287b2aace6de5fa240c57b'
Submodule path 'secGear': checked out '8f7f880f907a4236d8f1c7ff969ead78eef0756d'
```

编译penglai demo

```bash
[root@openeuler-riscv64 penglai-sdk]# cd Penglai-demo/
[root@openeuler-riscv64 Penglai-demo]# PENGLAI_LIB=/opt/penglai make
```

![Image](./tee6.png)

测试用例count，运行成功。

```bash
host:1: enclave attest4 Penglai-demo]# /opt/penglai/bin/penglai-host count/count 
0xA8, 0x74, 0x41, 0x57,  0x37, 0x35, 0x26, 0x00,  0xFD, 0xDC, 0xED, 0x28,  0x25, 0x4D, 0xC4, 0x14,  
0x39, 0x88, 0xF0, 0x42,  0x7A, 0xD0, 0x44, 0x76,  0xF5, 0xA8, 0x0A, 0xA1,  0x13, 0x4E, 0x15, 0xD6,  
0xF7, 0xEB, 0x66, 0x6E,  0x0B, 0x62, 0xBC, 0x50,  0xFA, 0x71, 0xB3, 0x3F,  0xC2, 0x2F, 0xF6, 0xDF,  
0x5A, 0xFF, 0x2A, 0x49,  0x5C, 0x22, 0xC7, 0xBF,  0x4D, 0xE2, 0x29, 0xCA,  0x8E, 0x8A, 0x5C, 0x5B,  
host:1: enclave run
[22852.213646][ T1774] [Penglai Driver@penglai_enclave_run] run returned successfully
[root@openeuler-riscv64 Penglai-demo]# 
```

![Image](./tee7.png)

再试试用例test_random，运行成功

```bash
[root@openeuler-riscv64 Penglai-demo]# /opt/penglai/bin/penglai-host gm_test_enclaves/test_random
host:1: enclave attest
0x45, 0xEB, 0x43, 0x26,  0x3C, 0x3A, 0xD9, 0x0C,  0x41, 0x2B, 0xE8, 0xA3,  0x0A, 0x30, 0xEE, 0xFE,  
0x59, 0x49, 0x75, 0x0B,  0x4B, 0xDE, 0xA7, 0x80,  0x69, 0xBA, 0x00, 0x5D,  0x55, 0x13, 0xF4, 0xC4,  
0xB9, 0xC7, 0xE0, 0x1F,  0xF6, 0xB0, 0x58, 0x65,  0x0C, 0xDD, 0x45, 0xDF,  0xC3, 0x76, 0x53, 0x2B,  
0x14, 0x49, 0xB4, 0xD0,  0x13, 0x79, 0x23, 0xE7,  0xCC, 0x50, 0x69, 0x82,  0xDA, 0xC9, 0x7E, 0x0B,  
host:1: enclave run
[ 2885.750711][ T1396] [TRACE] init_time 0: random prikey:
[ 2885.751155][ T1396] 163, 49, 114, 6, 166, 79, 99, 78, 188, 110, 128, 153, 229, 238, 183, 254
[ 2885.751480][ T1396] 68, 129, 231, 202, 29, 29, 71, 63, 31, 9, 199, 89, 153, 134, 104, 202
[ 2885.780449][ T1396] [TRACE] init_time 0, sign_time 0: SUCCESS
[ 2885.807393][ T1396] [TRACE] init_time 0, sign_time 1: SUCCESS
[ 2885.834837][ T1396] [TRACE] init_time 0, sign_time 2: SUCCESS
[ 2885.861782][ T1396] [TRACE] init_time 0, sign_time 3: SUCCESS
[ 2885.889968][ T1396] [TRACE] init_time 0, sign_time 4: SUCCESS
[ 2885.917145][ T1396] [TRACE] init_time 0, sign_time 5: SUCCESS
[ 2885.943909][ T1396] [TRACE] init_time 0, sign_time 6: SUCCESS
[ 2885.970568][ T1396] [TRACE] init_time 0, sign_time 7: SUCCESS
[ 2885.998431][ T1396] [TRACE] init_time 0, sign_time 8: SUCCESS
[ 2886.025309][ T1396] [TRACE] init_time 0, sign_time 9: SUCCESS
[ 2886.037460][ T1396] [TRACE] init_time 1: random prikey:
[ 2886.038458][ T1396] 217, 227, 204, 0, 126, 50, 92, 251, 116, 40, 156, 71, 13, 188, 134, 166
[ 2886.039046][ T1396] 51, 116, 20, 229, 94, 139, 209, 103, 50, 223, 102, 230, 148, 155, 137, 47
[ 2886.066642][ T1396] [TRACE] init_time 1, sign_time 0: SUCCESS
[ 2886.092585][ T1396] [TRACE] init_time 1, sign_time 1: SUCCESS
[ 2886.119394][ T1396] [TRACE] init_time 1, sign_time 2: SUCCESS
[ 2886.146903][ T1396] [TRACE] init_time 1, sign_time 3: SUCCESS
[ 2886.174468][ T1396] [TRACE] init_time 1, sign_time 4: SUCCESS
[ 2886.201730][ T1396] [TRACE] init_time 1, sign_time 5: SUCCESS
[ 2886.228687][ T1396] [TRACE] init_time 1, sign_time 6: SUCCESS
[ 2886.257334][ T1396] [TRACE] init_time 1, sign_time 7: SUCCESS
[ 2886.284075][ T1396] [TRACE] init_time 1, sign_time 8: SUCCESS
[ 2886.312465][ T1396] [TRACE] init_time 1, sign_time 9: SUCCESS
[ 2886.327013][ T1396] [TRACE] init_time 2: random prikey:
[ 2886.327713][ T1396] 198, 78, 212, 163, 160, 115, 210, 151, 4, 57, 73, 69, 177, 56, 33, 142
[ 2886.328300][ T1396] 22, 76, 46, 216, 87, 83, 228, 163, 77, 125, 238, 178, 233, 67, 37, 75
[ 2886.355777][ T1396] [TRACE] init_time 2, sign_time 0: SUCCESS
[ 2886.382347][ T1396] [TRACE] init_time 2, sign_time 1: SUCCESS
[ 2886.409528][ T1396] [TRACE] init_time 2, sign_time 2: SUCCESS
[ 2886.437060][ T1396] [TRACE] init_time 2, sign_time 3: SUCCESS
[ 2886.464812][ T1396] [TRACE] init_time 2, sign_time 4: SUCCESS
[ 2886.492586][ T1396] [TRACE] init_time 2, sign_time 5: SUCCESS
[ 2886.519667][ T1396] [TRACE] init_time 2, sign_time 6: SUCCESS
[ 2886.546635][ T1396] [TRACE] init_time 2, sign_time 7: SUCCESS
[ 2886.574159][ T1396] [TRACE] init_time 2, sign_time 8: SUCCESS
[ 2886.601388][ T1396] [TRACE] init_time 2, sign_time 9: SUCCESS
[ 2886.613340][ T1396] [TRACE] init_time 3: random prikey:
[ 2886.614366][ T1396] 2, 235, 35, 21, 250, 10, 134, 18, 63, 177, 96, 200, 65, 144, 87, 174
[ 2886.615015][ T1396] 134, 54, 138, 36, 183, 56, 243, 150, 150, 132, 241, 253, 58, 135, 15, 32
[ 2886.642868][ T1396] [TRACE] init_time 3, sign_time 0: SUCCESS
[ 2886.669846][ T1396] [TRACE] init_time 3, sign_time 1: SUCCESS
[ 2886.695983][ T1396] [TRACE] init_time 3, sign_time 2: SUCCESS
[ 2886.724045][ T1396] [TRACE] init_time 3, sign_time 3: SUCCESS
[ 2886.750973][ T1396] [TRACE] init_time 3, sign_time 4: SUCCESS
[ 2886.778317][ T1396] [TRACE] init_time 3, sign_time 5: SUCCESS
[ 2886.805307][ T1396] [TRACE] init_time 3, sign_time 6: SUCCESS
[ 2886.832250][ T1396] [TRACE] init_time 3, sign_time 7: SUCCESS
[ 2886.859415][ T1396] [TRACE] init_time 3, sign_time 8: SUCCESS
[ 2886.885712][ T1396] [TRACE] init_time 3, sign_time 9: SUCCESS
[ 2886.898471][ T1396] [TRACE] init_time 4: random prikey:
[ 2886.899334][ T1396] 233, 147, 211, 146, 216, 235, 54, 7, 219, 249, 218, 45, 154, 23, 99, 234
[ 2886.899973][ T1396] 198, 211, 167, 55, 97, 143, 204, 177, 70, 116, 116, 145, 5, 130, 45, 39
[ 2886.927641][ T1396] [TRACE] init_time 4, sign_time 0: SUCCESS
[ 2886.953894][ T1396] [TRACE] init_time 4, sign_time 1: SUCCESS
[ 2886.980763][ T1396] [TRACE] init_time 4, sign_time 2: SUCCESS
[ 2887.008992][ T1396] [TRACE] init_time 4, sign_time 3: SUCCESS
[ 2887.036249][ T1396] [TRACE] init_time 4, sign_time 4: SUCCESS
[ 2887.063542][ T1396] [TRACE] init_time 4, sign_time 5: SUCCESS
[ 2887.091452][ T1396] [TRACE] init_time 4, sign_time 6: SUCCESS
[ 2887.119170][ T1396] [TRACE] init_time 4, sign_time 7: SUCCESS
[ 2887.146834][ T1396] [TRACE] init_time 4, sign_time 8: SUCCESS
[ 2887.173565][ T1396] [TRACE] init_time 4, sign_time 9: SUCCESS
[ 2887.184633][ T1396] [TRACE] init_time 5: random prikey:
[ 2887.185655][ T1396] 201, 234, 31, 199, 36, 179, 117, 99, 131, 198, 200, 69, 251, 43, 235, 46
[ 2887.186126][ T1396] 24, 198, 79, 46, 117, 67, 150, 123, 110, 223, 241, 236, 255, 136, 92, 160
[ 2887.213371][ T1396] [TRACE] init_time 5, sign_time 0: SUCCESS
[ 2887.239656][ T1396] [TRACE] init_time 5, sign_time 1: SUCCESS
[ 2887.266494][ T1396] [TRACE] init_time 5, sign_time 2: SUCCESS
[ 2887.293772][ T1396] [TRACE] init_time 5, sign_time 3: SUCCESS
[ 2887.321486][ T1396] [TRACE] init_time 5, sign_time 4: SUCCESS
[ 2887.349533][ T1396] [TRACE] init_time 5, sign_time 5: SUCCESS
[ 2887.377014][ T1396] [TRACE] init_time 5, sign_time 6: SUCCESS
[ 2887.404393][ T1396] [TRACE] init_time 5, sign_time 7: SUCCESS
[ 2887.431621][ T1396] [TRACE] init_time 5, sign_time 8: SUCCESS
[ 2887.458272][ T1396] [TRACE] init_time 5, sign_time 9: SUCCESS
[ 2887.469661][ T1396] [TRACE] init_time 6: random prikey:
[ 2887.470418][ T1396] 49, 9, 89, 240, 61, 115, 6, 44, 189, 144, 113, 6, 35, 239, 70, 125
[ 2887.470761][ T1396] 152, 79, 56, 25, 70, 21, 138, 226, 5, 177, 13, 227, 19, 59, 174, 182
[ 2887.497690][ T1396] [TRACE] init_time 6, sign_time 0: SUCCESS
[ 2887.523722][ T1396] [TRACE] init_time 6, sign_time 1: SUCCESS
[ 2887.550312][ T1396] [TRACE] init_time 6, sign_time 2: SUCCESS
[ 2887.578027][ T1396] [TRACE] init_time 6, sign_time 3: SUCCESS
[ 2887.606622][ T1396] [TRACE] init_time 6, sign_time 4: SUCCESS
[ 2887.634813][ T1396] [TRACE] init_time 6, sign_time 5: SUCCESS
[ 2887.661453][ T1396] [TRACE] init_time 6, sign_time 6: SUCCESS
[ 2887.689718][ T1396] [TRACE] init_time 6, sign_time 7: SUCCESS
[ 2887.717210][ T1396] [TRACE] init_time 6, sign_time 8: SUCCESS
[ 2887.744374][ T1396] [TRACE] init_time 6, sign_time 9: SUCCESS
[ 2887.755413][ T1396] [TRACE] init_time 7: random prikey:
[ 2887.756057][ T1396] 105, 196, 109, 63, 65, 248, 74, 63, 108, 245, 14, 148, 50, 120, 196, 119
[ 2887.756462][ T1396] 155, 20, 37, 144, 12, 96, 212, 206, 44, 187, 105, 2, 211, 171, 154, 206
[ 2887.784516][ T1396] [TRACE] init_time 7, sign_time 0: SUCCESS
[ 2887.810973][ T1396] [TRACE] init_time 7, sign_time 1: SUCCESS
[ 2887.837240][ T1396] [TRACE] init_time 7, sign_time 2: SUCCESS
[ 2887.866741][ T1396] [TRACE] init_time 7, sign_time 3: SUCCESS
[ 2887.894537][ T1396] [TRACE] init_time 7, sign_time 4: SUCCESS
[ 2887.921673][ T1396] [TRACE] init_time 7, sign_time 5: SUCCESS
[ 2887.948710][ T1396] [TRACE] init_time 7, sign_time 6: SUCCESS
[ 2887.976725][ T1396] [TRACE] init_time 7, sign_time 7: SUCCESS
[ 2888.005064][ T1396] [TRACE] init_time 7, sign_time 8: SUCCESS
[ 2888.033165][ T1396] [TRACE] init_time 7, sign_time 9: SUCCESS
[ 2888.043826][ T1396] [TRACE] init_time 8: random prikey:
[ 2888.044456][ T1396] 12, 120, 183, 135, 135, 64, 179, 77, 249, 199, 152, 2, 130, 55, 188, 103
[ 2888.045549][ T1396] 2, 104, 22, 56, 47, 138, 167, 134, 138, 41, 98, 165, 252, 22, 72, 142
[ 2888.072335][ T1396] [TRACE] init_time 8, sign_time 0: SUCCESS
[ 2888.099553][ T1396] [TRACE] init_time 8, sign_time 1: SUCCESS
[ 2888.126123][ T1396] [TRACE] init_time 8, sign_time 2: SUCCESS
[ 2888.152743][ T1396] [TRACE] init_time 8, sign_time 3: SUCCESS
[ 2888.180906][ T1396] [TRACE] init_time 8, sign_time 4: SUCCESS
[ 2888.208767][ T1396] [TRACE] init_time 8, sign_time 5: SUCCESS
[ 2888.235970][ T1396] [TRACE] init_time 8, sign_time 6: SUCCESS
[ 2888.264410][ T1396] [TRACE] init_time 8, sign_time 7: SUCCESS
[ 2888.291998][ T1396] [TRACE] init_time 8, sign_time 8: SUCCESS
[ 2888.319068][ T1396] [TRACE] init_time 8, sign_time 9: SUCCESS
[ 2888.329373][ T1396] [TRACE] init_time 9: random prikey:
[ 2888.330627][ T1396] 21, 81, 30, 128, 182, 244, 28, 14, 225, 49, 167, 10, 251, 5, 79, 114
[ 2888.330862][ T1396] 20, 211, 145, 43, 115, 20, 101, 236, 229, 223, 121, 2, 14, 99, 108, 55
[ 2888.357932][ T1396] [TRACE] init_time 9, sign_time 0: SUCCESS
[ 2888.384513][ T1396] [TRACE] init_time 9, sign_time 1: SUCCESS
[ 2888.411334][ T1396] [TRACE] init_time 9, sign_time 2: SUCCESS
[ 2888.438911][ T1396] [TRACE] init_time 9, sign_time 3: SUCCESS
[ 2888.466424][ T1396] [TRACE] init_time 9, sign_time 4: SUCCESS
[ 2888.494170][ T1396] [TRACE] init_time 9, sign_time 5: SUCCESS
[ 2888.520690][ T1396] [TRACE] init_time 9, sign_time 6: SUCCESS
[ 2888.548593][ T1396] [TRACE] init_time 9, sign_time 7: SUCCESS
[ 2888.575369][ T1396] [TRACE] init_time 9, sign_time 8: SUCCESS
[ 2888.602869][ T1396] [TRACE] init_time 9, sign_time 9: SUCCESS
[ 2888.630788][ T1396] [Penglai Driver@penglai_enclave_run] run returned successfully
```

## 5 结论

1）这次实验在uefi edk2的openeuler2309V1镜像的基础上，成功测试Penglai TEE，并成功创建enclave，运行了测试用例。

2）虽然成功进行了实验，但是需要特别注意opensbi的版本，这里的opensbi的版本为1.2。如果是其他版本，可能会出问题。

3）注意到在2403分支的用例执行过程中，优化了输出，锁的信息都不显示了，被优化掉了。

## 6 参考资料

https://isrc.iscas.ac.cn/riscv-raios/docs/software/penglai/penglai_demo.html

https://www.cnblogs.com/world-explorer/p/18064229

https://github.com/Penglai-Enclave/Penglai-Enclave-sPMP

https://github.com/Penglai-Enclave/penglai-sdk/tree/oe-24.03

https://github.com/Penglai-Enclave/Penglai-demo/tree/99039a18b5dfd58015287b2aace6de5fa240c57b