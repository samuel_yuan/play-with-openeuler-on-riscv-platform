# 操作手册：Test Kubernetes for openEuler 2309V1 on QEMU

---

## 1 目标

测试kubernetes在openeuler2309V1上的可用性。

## 2 系统环境

下载openEuler 2309V1 QEMU镜像:

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/preview/openEuler-23.09-V1-riscv64/QEMU/

## 3 安装依赖

进入ubuntu主机，下载镜像和shell文件，解压出qcow2文件，运行./start_vm_xfce.sh启动openEuler2309V1系统，安装必要的依赖项包括docker。

```bash
dnf install -y conntrack-tools socat bison flex docker
```

查看docker版本

```bash
[root@openeuler-riscv64 ~]# systemctl enable docker
[root@openeuler-riscv64 ~]# systemctl restart docker
[root@openeuler-riscv64 ~]# docker --version
Docker version 18.09.0, build 458f41a
```

## 4 安装kubernetes

下载Kubernetes rpm包

```bash
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-1.25.3-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-client-1.25.3-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-help-1.25.3-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-kubeadm-1.25.3-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-kubelet-1.25.3-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-master-1.25.3-3.oe2309.riscv64.rpm
wget https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/repo/extra/update/20240418_v0.1_epol/kubernetes-node-1.25.3-3.oe2309.riscv64.rpm
```

安装kubernetes rpm包

```bash
[root@openeuler-riscv64 ~]# rpm -ivh kubernetes-1.25.3-3.oe2309.riscv64.rpm 
Verifying...                          ################################# [100%]
Preparing...                          ################################# [100%]
Updating / installing...
   1:kubernetes-1.25.3-3.oe2309       ################################# [100%]
[root@openeuler-riscv64 rpms]# rpm -ivh kubernetes-kubeadm-1.25.3-3.oe2309.riscv64.rpm 
Verifying...                          ################################# [100%]
Preparing...                          ################################# [100%]
Updating / installing...
   1:kubernetes-kubeadm-1.25.3-3.oe230################################# [100%]
[root@openeuler-riscv64 rpms]# rpm -ivh kubernetes-kubelet-1.25.3-3.oe2309.riscv64.rpm 
Verifying...                          ################################# [100%]
Preparing...                          ################################# [100%]
Updating / installing...
   1:kubernetes-kubelet-1.25.3-3.oe230################################# [100%]
```

![Image](./k1.png)

## 5 测试

查看kubernetes版本

```bash
[root@openeuler-riscv64 ~]# sudo systemctl enable --now kubelet
[root@openeuler-riscv64 ~]# kubelet --version
Kubernetes v1.25.3
[root@openeuler-riscv64 ~]# kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"25", GitVersion:"v1.25.3", GitCommit:"434bfd82814af038ad94d62ebe59b133fcb50506", GitTreeState:"archive", BuildDate:"2023-09-25T00:00:00Z", GoVersion:"go1.21.4", Compiler:"gc", Platform:"linux/riscv64"}
[root@openeuler-riscv64 ~]# kubectl version
-bash: kubectl: command not found
```

发现缺kubectl，不能运行。在这里，我们注意到：

```bash
kubelet ：运行在cluster，负责启动pod管理容器
kubeadm ：k8s快速构建工具，用于初始化cluster
kubectl ：k8s命令工具，部署和管理应用，维护组件
```

查看下载的文件，发现的确没有kubectl，这里的kubernetes看上去是一个kubernetes的客户端，没有服务端。

![Image](./k2.png)

由此看来，目前的kubernetes尚不能支持master节点，但是应该可以配置node节点。让我们来看看是否满足相关条件。

启动kubelet服务，查看运行状态

```bash
sudo systemctl start kubelet
systemctl -l --no-page status docker.service
systemctl -l --no-page status kubelet
```

Docker服务运行正常

![Image](./k3.png)

kubelet服务运行正常

![Image](./k4.png)

看起来kubelet可用，kubeadm的命令也是可用的，可以在node节点使用kubeadm join命令来加入集群。

## 6 总结

1）在openeuler2309中，仅支持kubelet和kubeadm，kubectl没有得到支持。

2）即便没有kubectl，目前的kubernetes版本也能够配置RISCV的node节点来加入集群，与X86的master节点，构成一云多芯的混合云。

3）这里暂不深究kubectl的支持问题，但建议openeuler的新版本会解决这个问题。

## 7 参考资料

https://blog.51cto.com/u_2475296/10711546

https://blog.csdn.net/zhangybo/article/details/137550920

