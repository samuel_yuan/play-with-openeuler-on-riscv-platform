# 操作手册：Enable git-lfs support and image extension for openEuler2403 qemu image

---

## 1 目标

ISCAS提供的openEuler2403 qemu image为大家学习riscv的主要工具之一。但在实际操作中我们会遇到不支持git-lfs，镜像的最大空间被限制在20G的问题，为学员们带来了不便。本教材将为小白们解决这些问题。

## 2 准备工作

系统环境，请下载基于UEFI EDK2的openEuler2403 Qemu镜像

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/QEMU/openEuler-24.03-V1-xfce-qemu-testing-uefi.qcow2.zst

Qemu的版本为9.02

执行start_vm_xfce_uefi.sh启动openEuler2403系统

## 3 安装git-lfs

进入openEuler2403的qemu虚拟机中，安装git-lfs的依赖golang

```bash
dnf install golang
```

下载git-lfs构建脚本和源码

```bash
git clone https://gitee.com/src-openeuler/git-lfs
```

安装rpm-build工具，并在~目录下生成rpmbuild工作目录

```bash
cd ~
dnf install rpmdevtools*
rpmdev-setuptree
```

进入git-lfs目录，准备构建安装git-lfs

```bash
cd ~/git-lfs
cp ./git-lfs.spec ~/rpmbuild/SPECS/
cp ./git-lfs-v3.2.0.tar.gz ~/rpmbuild/SOURCES
cp ./0001-use-vendor-dir-for-build.patch ~/rpmbuild/SOURCES
```

构建并安装git-lfs

```bash
cd ~/rpmbuild/SPECS
rpmbuild -bb git-lfs.spec
cd ~/rpmbuild/RPMS
cd riscv64
dnf install ./git-lfs-3.2.0-1.riscv64.rpm
```

查看是否安装成功

```bash
git lfs version
git lfs install
```

![Image](./lfs.png)

一切顺利，git-lfs已经使能。

## 4 扩展openEuler-24.03-V1-xfce-qemu-testing-uefi.qcow2镜像的空间

在ubuntu主机上，调整镜像的大小为64G

```bash
qemu-img resize openEuler-24.03-V1-xfce-qemu-testing-uefi.qcow2 64G
```

启动qemu虚拟机，进入内部命令行环境使用fdisk来扩展分区。从以下磁盘分区情况可以看到我的镜像实际上已经达到该镜像的极限20G。我删除了rootfs分区（注意删除原有的分区，这不会删除数据，只是移除分区信息），然后新建了rootfs分区占用空间63.5G。

```bash
[root@openeuler-riscv64 ~]# fdisk /dev/vda
Welcome to fdisk (util-linux 2.39.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
GPT PMBR size mismatch (41943039 != 134217727) will be corrected by write.
The backup GPT table is not on the end of the device. This problem will be corrected by write.
This disk is currently in use - repartitioning is probably a bad idea.
It's recommended to umount all file systems, and swapoff all swap
partitions on this disk.

Command (m for help): p

Disk /dev/vda: 64 GiB, 68719476736 bytes, 134217728 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: E825E285-FD83-4589-8217-8F731EAE6B58
Device       Start      End  Sectors  Size Type
/dev/vda1     2048  1050623  1048576  512M BIOS boot
/dev/vda2  1050624 41943006 40892383 19.5G Linux filesystem

Command (m for help): d
Partition number (1,2, default 2): 2

Partition 2 has been deleted.

Command (m for help): n
Partition number (2-128, default 2): 2
First sector (1050624-134217694, default 1050624): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (1050624-134217694, default 134215679): 

Created a new partition 2 of type 'Linux filesystem' and of size 63.5 GiB.
Partition #2 contains a ext4 signature.

Do you want to remove the signature? [Y]es/[N]o: n

Command (m for help): w

The partition table has been altered.
Syncing disks.
```

完成镜像空间的调整

```bash
[root@openeuler-riscv64 ~]# resize2fs /dev/vda2
resize2fs 1.47.0 (5-Feb-2023)
Filesystem at /dev/vda2 is mounted on /; on-line resizing required
old_desc_blocks = 3, new_desc_blocks = 8
[  176.637580][ T2895] EXT4-fs (vda2): resizing filesystem from 5111547 to 16645632 blocks
[  177.595943][ T2895] EXT4-fs (vda2): resized filesystem to 16645632
The filesystem on /dev/vda2 is now 16645632 (4k) blocks long.
```

查看文件系统分区情况

```bash
[root@openeuler-riscv64 ~]# df -h
```

重启系统，成功进入qemu环境，成功完成了镜像空间的扩展。

## 5 小结

1）本教材成功实现了openEuler2403 qemu中git-lfs的使能，也成功的实现了镜像空间的扩展。

2）虽然成功的解决了问题，但是openEuler2403的系统环境中在进行大文件复制时，会出现死机的情况。例如我在拷贝llama模型文件时，就会100%出现死机问题，最后希望openEuler在大文件复制方面能够改善。

![Image](./bigfile1.png)

![Image](./bigfile2.png)

## 6 参考资料

https://blog.csdn.net/weixin_71079921/article/details/137264178

https://blog.csdn.net/m0_57236802/article/details/141218811
