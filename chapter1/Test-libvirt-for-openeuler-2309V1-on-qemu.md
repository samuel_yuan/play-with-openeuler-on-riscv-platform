# 操作手册：Test libvirt for openEuler 2309V1 on QEMU

---

## 1 目标

接上篇教程[操作手册：Test libvirt for openEuler 2303 and 2309 on QEMU](./Test-libvirt-for-openeuler-2303-and-2309-on-qemu.md), 观测遗留问题，测试libvirt是否能在openeuler上跑起来，并成功运行虚拟机镜像。如能正常跑起来，那么64核的sg2042就能尝试使用libvirt来管理多个虚拟机。

## 2 系统环境

下载openEuler 2309V1 QEMU镜像:

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/preview/openEuler-23.09-V1-riscv64/QEMU/

## 3 准备工作

进入ubuntu主机，下载镜像和shell文件，解压出qcow2文件，运行./start_vm_xfce.sh启动openEuler2309V1系统，用MobaXterm，正确填写ubuntu主机的地址，和openEuler系统对应的端口号12055，然后登陆系统。

![Image](./v1.png)

登陆后，将openEuler-23.09-V1-base-qemu-preview.qcow2.zst传到目录中，并解压，供后续嵌入的虚拟机载入。

![Image](./v2.png)

## 4 测试

启动openEuler2309V1，查看qemu支持的情况（openEuler2309初版是没有qemu-system-riscv），可以看到在V1版本中有了，给中科院的老师们点赞。

```bash
[root@openeuler-riscv64 ~]# dnf list qemu*
Last metadata expiration check: 0:46:53 ago on Sun 03 Dec 2023 11:12:49 AM CST.
Installed Packages
qemu.riscv64                                    10:6.2.0-80.oe2309                     @OS
qemu-block-curl.riscv64                         10:6.2.0-80.oe2309                     @OS
qemu-block-iscsi.riscv64                        10:6.2.0-80.oe2309                     @OS
qemu-hw-usb-host.riscv64                        10:6.2.0-80.oe2309                     @OS
qemu-img.riscv64                                10:6.2.0-80.oe2309                     @OS
Available Packages
qemu-block-rbd.riscv64                          10:6.2.0-80.oe2309                     OS
qemu-block-ssh.riscv64                          10:6.2.0-80.oe2309                     OS
qemu-debuginfo.riscv64                          10:6.2.0-80.oe2309                     OS
qemu-debugsource.riscv64                        10:6.2.0-80.oe2309                     OS
qemu-guest-agent.riscv64                        10:6.2.0-80.oe2309                     OS
qemu-help.noarch                                10:6.2.0-80.oe2309                     OS
qemu-system-aarch64.riscv64                     10:6.2.0-80.oe2309                     OS
qemu-system-arm.riscv64                         10:6.2.0-80.oe2309                     OS
qemu-system-riscv.riscv64                       10:6.2.0-80.oe2309                     OS
qemu-system-x86_64.riscv64                      10:6.2.0-80.oe2309                     OS
```

既然有qemu-system-riscv，那么我们修改下Domain XML。

```bash
sudo vim vm-oe2309.xml
```

内容如下：

```bash
<domain type='qemu'>
  <name>openEuler2309V1</name>
  <memory unit='KiB'>1048576</memory>
  <currentMemory unit='KiB'>1048576</currentMemory>
  <vcpu placement='static'>2</vcpu>
  <os>
    <type arch='riscv64' machine='virt'>hvm</type>
    <loader readonly='yes' type='rom'>/root/openEuler/fw_payload_oe_uboot_2304.bin</loader>
     <boot dev='hd'/>
   </os>
   <clock offset='localtime'/>
   <on_poweroff>destroy</on_poweroff>
   <on_reboot>restart</on_reboot>
   <on_crash>destroy</on_crash>
   <devices>
     <emulator>/usr/bin/qemu-system-riscv64</emulator>
     <disk type='file' device='disk'>
       <driver name='qemu' type='qcow2'/>
       <source file='/root/openEuler/openEuler-23.09-V1-base-qemu-preview.qcow2'/>
       <target dev='vda' bus='virtio'/>
       <address type='pci' domain='0x0000' bus='0x00' slot='0x08' function='0x0'/>
     </disk>
     <controller type='pci' index='0' model='pcie-root'/>
     <serial type='pty'>
         <target port='0'/>
     </serial>
     <console type='pty'>
         <target type='serial' port='0'/>
     </console>
     <interface type='user'>
       <mac address='24:42:53:21:52:45'/>
       <model type='virtio'/>
     </interface>
   </devices>
</domain>
```

按照Domain XML上的描述，将之前解压的镜像文件放到对应的openEuler目录中，然后修改目录权限。

```bash
[root@openeuler-riscv64 ~]# chmod 777 openEuler/ -R
[root@openeuler-riscv64 ~]# ll openEuler/
total 2036816
-rwxrwxrwx 1 root root    2764568 Dec  3 12:13 fw_payload_oe_uboot_2304.bin
-rwxrwxrwx 1 root root 2082930688 Dec  3 11:52 openEuler-23.09-V1-base-qemu-preview.qcow2
```

调整libvirt的用户权限，加入root用户，然后重启服务，并确认服务处于running状态。

```bash
sudo vim /etc/libvirt/qemu.conf
sudo systemctl restart libvirtd
sudo systemctl -l --no-page status libvirtd
```

![Image](./v3.png)

定义虚拟机，并查看虚拟机列表

```bash
[root@openeuler-riscv64 ~]# virsh define vm-oe2309.xml
Domain openEuler2309V1 defined from vm-oe2309.xml

[root@openeuler-riscv64 ~]# virsh list --all
 Id   Name              State
----------------------------------
 -    openEuler2309V1   shut off
```

我们看到虚拟机定义成功，目前处于关闭状态。那么我们将其启动。

```bash
[root@openeuler-riscv64 ~]# virsh start openEuler2309V1
Domain openEuler2309V1 started

[root@openeuler-riscv64 ~]# virsh list --all
 Id   Name              State
---------------------------------
 1    openEuler2309V1   running
```

虚拟机启动成功，目前处于运行状态。那么我们登陆console。

```bash
virsh console openEuler2309V1
```

![Image](./v4.png)

我们看到系统可以启动，但是最后无法进入console。

![Image](./v5.png)

以下是其他virsh命令，这里列出参考。（例如虚拟机系统关不掉，那就强制关闭。）

```bash
[root@openeuler-riscv64 ~]# virsh shutdown openEuler2309V1
Domain openEuler2309V1 is being shutdown

[root@openeuler-riscv64 ~]# virsh destroy openEuler2309V1
Domain openEuler2309V1 destroyed

[root@openeuler-riscv64 ~]# virsh list --all
 Id   Name              State
----------------------------------
 -    openEuler2309V1   shut off

[root@openeuler-riscv64 ~]# virsh undefine openEuler2309V1
Domain openEuler2309V1 has been undefined
```

## 5 总结

openeuler2309V1是一个不错的版本，其中libvirt+qemu软件虚拟的方法成功启动虚拟机镜像并运行，但却无法进入console，这个问题我看到fedora和archlinux也同样存在，疑似bug。可以参考如下两个链接：

https://unix.stackexchange.com/questions/536284/fedora-30-cant-boot-faild-to-start-up-manager

https://bbs.archlinux.org/viewtopic.php?id=284565

这里暂不深究。后续可以尝试其他文件系统试试，例如busybox，windows7，windows10等等。

## 6 参考资料

https://libvirt.org/drvqemu.html

https://zhuanlan.zhihu.com/p/628943858

https://blog.csdn.net/woailuohui/article/details/84603944

https://blog.csdn.net/weixin_30550271/article/details/95202303

https://www.baidu.com/link?url=h1pA7PhL-XYFGkqLTX0Fz7Guosiuc1ISQ2qWJdnGiQZK0zwuMKpGWjfs6DvR03_AXDSnKYp73T1qFR7_vn0Cwa&wd=&eqid=b6093b7b0078ee2d00000006656c061f

https://blog.51cto.com/u_14247623/2773538

https://www.imooc.com/article/270462

https://blog.csdn.net/wangyijieonline/article/details/103181899