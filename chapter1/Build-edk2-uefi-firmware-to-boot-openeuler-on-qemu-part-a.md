# 操作手册：Build EDK2 UEFI firmware to boot openEuler on QEMU

---

## 1 目标

制作EDI2 UEFI固件，不依赖于任何厂商的bin文件，实现openEuler在QEMU运行。考虑到这是首次做，只考虑启动，其他后续再慢慢完善。

## 2 操作过程

主要过程如下：

UEFI shell -> grubriscv64.efi -> grub.cfg -> openEuler rootfs

### 2.1 制作EDI2 UEFI固件

操作主机的系统和工具链版本如下：

```bash
riscv@ubuntu:~/riscv64-linux$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 20.04.6 LTS
Release:	20.04
Codename:	focal
riscv@ubuntu:~/riscv64-linux$ riscv64-linux-gnu-gcc --version
riscv64-linux-gnu-gcc (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

安装前置软件包

```bash
sudo apt install build-essential uuid-dev iasl git nasm python3-distutils
```

下载EDK2源码

```bash
sudo git clone --recurse-submodule https://github.com/tianocore/edk2
```

设置环境路径，注意工具链最后一个"-"不要省略！不要省略！不要省略！

```bash
export WORKSPACE=/home/riscv/riscv64-linux
export GCC5_RISCV64_PREFIX=riscv64-linux-gnu-
export PACKAGES_PATH=$WORKSPACE/edk2
export EDK_TOOLS_PATH=$WORKSPACE/edk2/BaseTools
```

准备编译，有时候会出现权限问题，干脆给足，然后运行配置脚本。

```bash
sudo chmod -R 777 edk2
source edk2/edksetup.sh
```

结果如下：

```bash
riscv@ubuntu:~/riscv64-linux$ source edk2/edksetup.sh
Using EDK2 in-source Basetools
WORKSPACE: /home/riscv/riscv64-linux
EDK_TOOLS_PATH: /home/riscv/riscv64-linux/edk2/BaseTools
CONF_PATH: /home/riscv/riscv64-linux/edk2/Conf
Copying $EDK_TOOLS_PATH/Conf/build_rule.template
     to /home/riscv/riscv64-linux/edk2/Conf/build_rule.txt
Copying $EDK_TOOLS_PATH/Conf/tools_def.template
     to /home/riscv/riscv64-linux/edk2/Conf/tools_def.txt
Copying $EDK_TOOLS_PATH/Conf/target.template
     to /home/riscv/riscv64-linux/edk2/Conf/target.txt
```

编译固件，得到RISCV_VIRT.fd

```bash
make -C edk2/BaseTools clean
make -C edk2/BaseTools
make -C edk2/BaseTools/Source/C
source edk2/edksetup.sh BaseTools
build -a RISCV64 --buildtarget RELEASE -p OvmfPkg/RiscVVirt/RiscVVirtQemu.dsc -t GCC5
```

如下图

![Image](./fd01.png)

![Image](./fd02.png)

设置固件image的大小为32M，否则qemu会提示“qemu-system-riscv64: device requires 33554432 bytes, block backend provides 8388608 bytes”。

```bash
truncate -s 32M Build/RiscVVirtQemu/RELEASE_GCC5/FV/RISCV_VIRT.fd
```

至此，固件已准备好了。

### 2.2 制作openEuler的img

#### 2.2.1 准备工作

在工作目录创建kernel目录“boot”和rootfs目录“rootfs”

```bash
sudo mkdir boot
sudo mkdir rootfs
```

挂载镜像

```bash
sudo modprobe nbd max_part=8
sudo /opt/qemu/bin/qemu-nbd -c /dev/nbd1 /mnt/hgfs/qemu/openeuler/img/20230601/openEuler-23.03-V1-xfce-qemu-preview.qcow2
```

将该镜像的2个分区分别挂载到相应目录，并备份文件到boot1和rootfs1,然后卸载目录。

```bash
#挂载目录
sudo mount /dev/nbd1p1 /home/riscv/riscv64-linux/boot
sudo mount /dev/nbd1p2 /home/riscv/riscv64-linux/rootfs
sudo cp -rf /home/riscv/riscv64-linux/boot /home/riscv/riscv64-linux/boot1
sudo cp -rf /home/riscv/riscv64-linux/rootfs /home/riscv/riscv64-linux/rootfs1

#修改fstab配置文件，需取消/boot分区挂载设置
sudo sed -i "s/.*boot.*/#&/" /home/riscv/riscv64-linux/rootfs1/etc/fstab 

#卸载目录
sudo umount boot
sudo umount rootfs
#取消镜像挂载
sudo /opt/qemu/bin/qemu-nbd -d /dev/nbd1
```

#### 2.2.2 制作rootfs.img

安装必要前置

```bash
sudo apt install kpartx
```

创建20G的rootfs.img，注意这里要设置gpt分区。

```bash
qemu-img create rootfs.img 20g
sudo parted rootfs.img --script -- mklabel gpt
sudo parted rootfs.img --script -- mkpart primary ext4 2048s -1
```

img文件格式化，并查看img分区及格式化信息。

```bash
sudo losetup /dev/loop18 rootfs.img
sudo kpartx -av /dev/loop18
sudo mkfs.ext4 /dev/mapper/loop18p1
sudo parted rootfs.img
```

img文件准备好后，当然是把备份的系统文件复制过来。在工作目录执行下述指令：

```bash
#挂载到rootfs
sudo mount /dev/mapper/loop18p1 rootfs/
#复制系统文件
sudo cp -rf rootfs1/* rootfs/
sync
#更新fstab配置文件里根分区的UUID
new_rootfs_id=`sudo blkid /dev/mapper/loop18p1 -s UUID | awk -F": " '{ print $2 }'`
sudo sed -i "s/UUID=.* \/ /${new_rootfs_id} \/ /" rootfs/etc/fstab
#卸载rootfs目录
sudo umount rootfs
#卸载img，删除loop18
sudo kpartx -d rootfs.img 
```

至此，rootfs.img制作完成。

#### 2.2.3 制作efi.img

创建256M的efi.img，一切默认。

```bash
qemu-img create efi.img 256m
```

img文件格式化

```bash
sudo mkfs.msdos efi.img
```

查看img的创建情况

```bash
riscv@ubuntu:~/riscv64-linux$ sudo parted efi.img 
GNU Parted 3.3
Using /home/riscv/riscv64-linux/efi.img
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) print                                                            
Model:  (file)
Disk /home/riscv/riscv64-linux/efi.img: 268MB
Sector size (logical/physical): 512B/512B
Partition Table: loop
Disk Flags: 
Number  Start  End    Size   File system  Flags
 1      0.00B  268MB  268MB  fat16
```

在工作目录中建立EFI目录，并加载efi.img

```bash
sudo mkdir EFI
sudo mount -o loop efi.img EFI
```

编辑startup.nsh，内容如下：

```bash
fs0:\grubriscv64.efi
```

将编辑好的nsh拷贝到EFI目录中。在UEFI启动后，如果该文件存在，那么会自动运行其中的shell程序，无需手动。

```bash
sudo cp startup.nsh EFI/
```

准备[grubriscv64.efi](./grubriscv64.efi)，这里这个文件是直接从软件所周老师那获取的，版本和openEuler2303V1的kernel一致，均为6.1.19-2.oe2303.riscv64。（如果自制，本教程末尾会给出方法。）

复制grubriscv64.efi到EFI目录。nsh脚本运行后，会运行这个grubriscv64.efi。

```bash
sudo cp grubriscv64.efi EFI/
```

编辑grub.cfg，内容如下：

```bash
menuentry "openEuler2303 V1" {
        linux /vmlinuz-openEuler root=/dev/vda1 rw console=ttyS0 rootwait earlycon=uart8250,mmio,0x10000000
        initrd /initramfs-6.1.19-2.oe2303.riscv64.img
}
```

将编辑好的cfg拷贝到EFI目录中。grubriscv64.efi运行后，会执行cfg中的配置，启动操作系统菜单。

```bash
sudo cp grub.cfg EFI/
```

最后将备份的boot1目中的kernel文件全部拷贝到EFI目录。

```bash
sudo cp -rf boot1/* EFI/
sync
```

显示该目录的内容如下：

![Image](./uefi1.png)

确认EFI目录内容后，卸载文件镜像。

```bash
sudo umount EFI
```

至此，efi.img制作完成。

## 3 运行

将准备好的固件，img，全部拷贝至指定运行目录，例如，我的工作目录是~/riscv64-linux，运行目录是/mnt/hgfs/qemu/openeuler/img/20230601/。

```bash
sudo cp ~/riscv64-linux/efi.img /mnt/hgfs/qemu/openeuler/img/20230601/
sudo cp ~/riscv64-linux/rootfs.img /mnt/hgfs/qemu/openeuler/img/20230601/
sudo cp ~/riscv64-linux/Build/RiscVVirtQemu/RELEASE_GCC5/FV/RISCV_VIRT.fd /mnt/hgfs/qemu/openeuler/img/20230601/
```

运行qemu

```bash
qemu-system-riscv64 -nographic -machine virt -machine acpi=off -m 8G -smp 8 -display sdl -object rng-random,filename=/dev/urandom,id=rng0 -device virtio-vga -device virtio-rng-device,rng=rng0 -drive file=./RISCV_VIRT.fd,if=pflash,format=raw,unit=1 -drive file=./rootfs.img,format=raw,id=hd0 -device virtio-blk-device,drive=hd0 -drive file=efi.img,format=raw,id=hd1 -device virtio-blk-device,drive=hd1 -device virtio-net-device,netdev=usernet -netdev user,id=usernet,hostfwd=tcp::12055-:22 -device qemu-xhci -usb -device usb-kbd -device usb-tablet
```

让我们看看运行过程：

随着RISCV_VIRT.fd的正确加载，这里等待10s或者键入enter会直接进入下一步（按ESC会进入EDK2 bios的菜单）
![Image](./uefi2.png)

进入UEFI shell，我们可以看到，这里等待5s或者键入enter会直接进入下一步执行nsh脚本（这个等待时间可以通过Shell> set StartupDelay 1来设置）

![Image](./uefi3.png)

nsh脚本执行grubriscv64.efi，并进入grub菜单

![Image](./uefi4.png)

选择openEuler系统启动，这里面可以看到kernel的版本信息6.1.19

![Image](./uefi5.png)

一切正常

![Image](./uefi6.png)

成功进入了系统。

![Image](./uefi7.png)

## 4 总结

1、关于grubriscv64.efi，如果是自制的话，可以参考如下操作。

我只找到openEuler-23.09-Dev分支的源码，版本是6.4，下载源码。

```bash
git clone -b openEuler-23.09-Dev https://gitee.com/openeuler/kernel/
```

进入kernel源码目录，在kernel配置菜单中找到如下选项，并勾选。

```bash
Enable the generic EFI decompressor
```

然后编译源码，具体的编译脚本在之前的教程里面有，这里就不再累述。

```bash
  OBJCOPY arch/riscv/boot/Image
  AS      arch/riscv/boot/zboot-header.o
  PAD     arch/riscv/boot/vmlinux.bin
  GZIP    arch/riscv/boot/vmlinuz
  OBJCOPY arch/riscv/boot/vmlinuz.o
  LD      arch/riscv/boot/vmlinuz.efi.elf
  OBJCOPY arch/riscv/boot/vmlinuz.efi
  Kernel: arch/riscv/boot/vmlinuz.efi is ready
```

从以上信息，我们可以看到这里多出了vmlinuz.efi的文件，这个文件的版本就是6.4,配合其他同步产生的kernel文件，同样也能达到效果。

2、虽然成功进入了系统，但是还有许多提升空间。这将在后续教程中逐步完善。例如启动日志反应的情况，还有在内存映射时需要大约1分钟才能启动后续程序，将在后续教程处理。

```bash
[root@openeuler-riscv64 /]# journalctl -xb
Jun 30 21:02:34 openeuler-riscv64 kernel: Linux version 6.1.19-2.oe2303.riscv64>
Jun 30 21:02:34 openeuler-riscv64 kernel: random: crng init done
Jun 30 21:02:34 openeuler-riscv64 kernel: OF: fdt: Ignoring memory range 0x8000>
Jun 30 21:02:34 openeuler-riscv64 kernel: Machine model: riscv-virtio,qemu
Jun 30 21:02:35 openeuler-riscv64 kernel: earlycon: uart8250 at MMIO 0x00000000>
Jun 30 21:02:35 openeuler-riscv64 kernel: printk: bootconsole [uart8250] enabled
Jun 30 21:02:35 openeuler-riscv64 kernel: efi: EFI v2.70 by EDK II
Jun 30 21:02:35 openeuler-riscv64 kernel: efi: MEMATTR=0x17efc2018 INITRD=0x17d>
Jun 30 21:02:35 openeuler-riscv64 kernel: OF: fdt: Ignoring memory block 0x8000>
Jun 30 21:02:35 openeuler-riscv64 kernel: OF: fdt: Ignoring memory range 0x8004>
Jun 30 21:02:35 openeuler-riscv64 kernel: Zone ranges:
Jun 30 21:02:35 openeuler-riscv64 kernel:   DMA32    [mem 0x0000000080200000-0x>
Jun 30 21:02:35 openeuler-riscv64 kernel:   Normal   [mem 0x0000000100000000-0x>
Jun 30 21:02:35 openeuler-riscv64 kernel: Movable zone start for each node
Jun 30 21:02:35 openeuler-riscv64 kernel: Early memory node ranges
Jun 30 21:02:35 openeuler-riscv64 kernel:   node   0: [mem 0x0000000080200000-0>
Jun 30 21:02:35 openeuler-riscv64 kernel:   node   0: [mem 0x000000017fe3d000-0>
Jun 30 21:02:35 openeuler-riscv64 kernel:   node   0: [mem 0x000000017ffff000-0>
Jun 30 21:02:35 openeuler-riscv64 kernel: Initmem setup node 0 [mem 0x000000008>
Jun 30 21:02:35 openeuler-riscv64 kernel: SBI specification v1.0 detected
Jun 30 21:02:35 openeuler-riscv64 kernel: SBI implementation ID=0x1 Version=0x1>
Jun 30 21:02:35 openeuler-riscv64 kernel: SBI TIME extension detected
Jun 30 21:02:35 openeuler-riscv64 kernel: SBI IPI extension detected
lines 1-23
```

## 5 处理方法

感谢ouuleilei对本文的贡献，相关内容已合并进本文。

即修改/etc/fstab中的内容，注释掉/boot分区挂载。重启后可成功进入系统的命令行界面和图形界面。

![Image](./uefi8.png)

## 6 参考资料

https://github.com/vlsunil/riscv-uefi-edk2-docs/wiki/RISC-V-Qemu-Virt-support

https://github.com/vlsunil/riscv-uefi-edk2-docs

https://github.com/tianocore/tianocore.github.io/wiki/Getting-Started-with-EDK-II
