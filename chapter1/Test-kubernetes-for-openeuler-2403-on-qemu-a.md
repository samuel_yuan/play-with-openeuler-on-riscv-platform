# 操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part A）

---

## 1 目标

测试kubernetes在openeuler2403上的可用性。

## 2 系统环境

### 2.1 下载openEuler 2403LTS QEMU镜像 (4月9日版本)

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/QEMU/

注意：若qemu用的是9.0以上的版本，启动qemu的shell脚本（start_vm_xfce_uefi_penglai.sh）需要修改，才能启动，具体如下：

```bash
md="qemu-system-riscv64 \
  -bios "$fw3" \
  -nographic -machine virt,pflash0=pflash0,pflash1=pflash1,acpi=off\
  -smp "$vcpu" -m "$memory"G \
  -display sdl \
  -audiodev pa,id=snd0 \
  -blockdev node-name=pflash0,driver=file,read-only=on,filename="$fw1" \
  -blockdev node-name=pflash1,driver=file,filename="$fw2" \
  -drive file="$drive",format=qcow2,id=hd0,if=none \
  -object rng-random,filename=/dev/urandom,id=rng0 \
  -device virtio-vga \
  -device virtio-rng-device,rng=rng0 \
  -device virtio-blk-device,drive=hd0 \
  -device virtio-net-device,netdev=usernet \
  -netdev user,id=usernet,hostfwd=tcp::"$ssh_port"-:22 \
  -device qemu-xhci -usb -device usb-kbd -device usb-tablet -device usb-audio,audiodev=snd0"
```

原脚本如下，仅供参考：

```bash
cmd="qemu-system-riscv64 \
  -bios "$fw3" \
  -nographic -machine virt,pflash0=pflash0,pflash1=pflash1,acpi=off\
  -smp "$vcpu" -m "$memory"G \
  -display sdl \
  -audiodev pa,id=snd0 \
  -blockdev node-name=pflash0,driver=file,read-only=on,filename="$fw1" \
  -blockdev node-name=pflash1,driver=file,filename="$fw2" \
  -drive file="$drive",format=qcow2,id=hd0 \
  -object rng-random,filename=/dev/urandom,id=rng0 \
  -device virtio-vga \
  -device virtio-rng-device,rng=rng0 \
  -device virtio-blk-device,drive=hd0 \
  -device virtio-net-device,netdev=usernet \
  -netdev user,id=usernet,hostfwd=tcp::"$ssh_port"-:22 \
  -device qemu-xhci -usb -device usb-kbd -device usb-tablet -device usb-audio,audiodev=snd0"
```

### 2.2 更新系统

修改repo

```bash
vim /etc/yum.repos.d/openEuler.repo
```

内容修改如下：

```bash
[OS]
name=OS
baseurl=https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing//
20240708/v0.1/repo/24.03/OS/riscv64/
enabled=1
gpgcheck=0
priority=90

[everything]
name=everything
baseurl=https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing//
20240708/v0.1/repo/24.03/OS/riscv64/
enabled=1
gpgcheck=0
priority=99

[debuginfo]
name=debuginfo
baseurl=https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing//
20240708/v0.1/repo/24.03/OS/riscv64/
enabled=1
gpgcheck=0
priority=99

[EPOL]
name=EPOL
baseurl=https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing//
20240708/v0.1/repo/24.03/EPOL/main/riscv64/
enabled=1
gpgcheck=0
priority=90
[extra]
name=extra
baseurl=https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing//
20240708/v0.1/repo/extra/
enabled=1
gpgcheck=0
priority=90
[update]
name=update
baseurl=https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing//
20240708/v0.1/repo/extra/update/
enabled=1
gpgcheck=0
priority=90
```

更新系统至7月8日的版本，然后重启。这个版本的repo中有1.29版本的kubernetes包，而之前4月9日版本的repo中只有1.25版本的kubernetes包，该包之前测试过，是openEuler2309里原生的，其中kubectl没有得到支持。

```bash
dnf update
```

## 3 安装kubernetes

安装kubernetes依赖

```bash
dnf install -y conntrack-tools socat bison flex libseccomp ntpdate runc conntrack iptables-nft
```

查看kubernetes，发现已经更新到1.29版本

```bash
[root@openeuler-riscv64 ~]# dnf list kuber*
Last metadata expiration check: 0:39:56 ago on Tue 29 Oct 2024 05:44:32 PM CST.
Available Packages
kubernetes.riscv64                         1.29.1-6.oe2403                  EPOL
kubernetes-client.riscv64                  1.29.1-6.oe2403                  EPOL
kubernetes-help.riscv64                    1.29.1-6.oe2403                  EPOL
kubernetes-kubeadm.riscv64                 1.29.1-6.oe2403                  EPOL
kubernetes-kubelet.riscv64                 1.29.1-6.oe2403                  EPOL
kubernetes-master.riscv64                  1.29.1-6.oe2403                  EPOL
kubernetes-node.riscv64                    1.29.1-6.oe2403                  EPOL
```

直接安装

```bash
dnf install kuber*
```

## 4 测试

完成安装后，查看kubernetes 1.29各组件的版本，发现这次kubectl已经可用了。

```bash
[root@openeuler-riscv64 ~]# kubelet --version
Kubernetes v1.29.1
[root@openeuler-riscv64 ~]# kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"29", GitVersion:"v1.29.1", GitCommit:"bc401b91f2782410b
3fb3f9acf43a995c4de90d2", GitTreeState:"archive", BuildDate:"2024-07-03T00:00:00Z", GoVersion:"go1.21.6"
, Compiler:"gc", Platform:"linux/riscv64"}
[root@openeuler-riscv64 ~]# kubectl version
Client Version: v1.29.1
Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
```

## 5 总结

1）我们可以看到这个版本的openEuler2403已经支持kubectl，相对于openEuler2309取得了进步。

2）openEuler2403有了好的基础，下篇教程将进一步验证openEuler2403原生的kubernetes 1.29.1

## 6 参考资料
