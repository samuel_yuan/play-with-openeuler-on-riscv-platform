# 操作手册：Test secGear on openEuler2309V1 Qemu

---

## 1 目标

基于UEFI EDK2的openEuler2309V1 Qemu镜像，测试secGear。上次教程我们成功编译了Penglai-enclave-driver和测试了demo。本次教程将讲解secGear的正确打开方式。
## 2 准备工作

安装环境依赖

系统环境，请下载基于UEFI EDK2的openEuler2309V1 Qemu镜像（参考之前教程制作的镜像, https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/community/samuel_yuan/openeuler2309/qemu-uefi/ ）

启动系统，安装必要的编译依赖项目

```bash
dnf install cmake dune openssl openssl-devel -y
dnf install libmpc-devel mpfr-devel gmp-devel
```

进入~/目录，下载penglai sdk的secGear的依赖包，注意一定要下载oe-release的分支，其他分支都有问题，会在小结时提及。该依赖包中包含sdk、secGear、opam、penglai.ko等预编译内容。

```bash
wget https://ipads.se.sjtu.edu.cn:1313/f/717d7a03848c46d380d1/?dl=1 -O penglai_secGear_env-v1.0.zip
unzip penglai_secGear_env-v1.0.zip
cp ~/penglai_secGear_env-v1.0/opam.tar.gz .
tar -zxvf opam.tar.gz
```

使用ls -a查看解压后的目录，可以发现在当前目录下有一个.opam的隐藏目录，prebuilt的二进制都在这个目录之中。在~/.bashrc中添加如下环境变量并使其生效：

```bash
vim ~/.bashrc
```

```bash
export PATH=/root/.opam/4.12.0/bin:$PATH
```

使其生效

```bash
source ~/.bashrc
```

## 3 编译secGear example

创建开发目录

```bash
mkdir -p ~/dev
```

复制代码

```bash
[root@openeuler-riscv64 ~]# cp -rf penglai_secGear_env-v1.0/secGear dev/
[root@openeuler-riscv64 ~]# cp -rf penglai_secGear_env-v1.0/sdk dev/
```

编译以及安装example

```bash
[root@openeuler-riscv64 ~]# cd dev
[root@openeuler-riscv64 dev]# ls
sdk  secGear
[root@openeuler-riscv64 dev]# cd ~/dev/secGear
[root@openeuler-riscv64 secGear]# source environment && mkdir -p debug && cd debug
[root@openeuler-riscv64 debug]# cmake -DENCLAVE=PL .. && make && make install
```

## 4 运行secGear example

加载上个教程中penglai内核模块penglai.ko

```bash
[root@openeuler-riscv64 debug]# sudo insmod ~/penglai-enclave-sPMP/penglai-enclave-driver/penglai.ko 
[91847.815920][ T5230] penglai: loading out-of-tree module taints kernel.
[91847.818292][ T5230] penglai: module verification failed: signature and/or required key missing - tainting kernel
[91847.838361][ T5230] enclave_ioctl_init...
[91847.845180][ T5230] [Penglai KModule] register_chrdev succeeded!
```

成功运行secgear_helloworld

```bash
[root@openeuler-riscv64 debug]# ./bin/secgear_helloworld
Create secgear enclave
DEBUG:[_penglai_create] enter function: _penglai_create
elf_preload: size=0x6320
[91985.294380][ T5236] BUG: sleeping function called from invalid context at lib/usercopy.c:14
[91985.294738][ T5236] in_atomic(): 1, irqs_disabled(): 0, non_block: 0, pid: 5236, name: secgear_hellow
o
[91985.294958][ T5236] preempt_count: 1, expected: 0
[91985.295393][ T5236] CPU: 3 PID: 5236 Comm: secgear_hellowo Tainted: G           OE      6.4.0+ #1
[91985.295625][ T5236] Hardware name: riscv-virtio,qemu (DT)
[91985.296049][ T5236] Call Trace:
[91985.296284][ T5236] [<ffffffff800061bc>] dump_backtrace+0x28/0x30
[91985.296911][ T5236] [<ffffffff80b8adb0>] show_stack+0x38/0x44
DEBUG:[_penglai_create] penglai enclave create successfully! 
[91985.298674][ T5236] [<ffffffff80b9ed1a>] dump_stack_lvl+0x44/0x5c
[91985.299606][ T5236] [<ffffffff80b9ed4a>] dump_stack+0x18/0x20
[91985.299950][ T5236] [<ffffffff8004bd3c>] __might_resched+0x112/0x120
[91985.300346][ T5236] [<ffffffff8004bd94>] __might_sleep+0x4a/0x72
[91985.300647][ T5236] [<ffffffff8021f68e>] __might_fault+0x24/0x2c
[91985.300767][ T5236] [<ffffffff80599e64>] _copy_from_user+0x28/0xa4
[91985.300906][ T5236] [<ffffffff03bbe232>] penglai_enclave_run+0xae/0x38a [penglai]
[91985.301770][ T5236] [<ffffffff03bbeeac>] penglai_enclave_ioctl+0x18c/0x20a [penglai]
[91985.302277][ T5236] [<ffffffff802cffd2>] sys_ioctl+0x90/0xaa
[91985.302613][ T5236] [<ffffffff80b9f848>] do_trap_ecall_u+0xf0/0x104
[91985.303048][ T5236] [<ffffffff80003ea4>] ret_from_exception+0x0/0x64
[91985.305014][ T5236] Penglai Enclave is running
[91985.309434][ T5236] ecall function id: 0
[91985.310557][ T5236] ecall output_bytes_written: 48
[91985.318001][ T5236] [Penglai Driver@penglai_enclave_run] run returned successfully
secgear hello world!
DEBUG:[_penglai_destroy] enter function: _penglai_destroy
```

![Image](./tee10.png)

成功运行secgear_calculation

```bash
[root@openeuler-riscv64 debug]# ./bin/secgear_calculation 
Create secgear enclave
DEBUG:[_penglai_create] enter function: _penglai_create
elf_preload: size=0x6788
[92062.988934][ T5238] BUG: sleeping function called from invalid context at lib/usercopy.c:14
DEBUG:[_penglai_create] penglai [92062.989213][ T5238] in_atomic(): 1, irqs_disabled(): 0, non_block: 0,
 pid: 5238, name: secgear_calcula
enclave create s[92062.989373][ T5238] preempt_count: 1, expected: 0
uccessfully! 
[92062.989465][ T5238] CPU: 2 PID: 5238 Comm: secgear_calcula Tainted: G        W  OE      6.4.0+ #1
[92062.989766][ T5238] Hardware name: riscv-virtio,qemu (DT)
[92062.989962][ T5238] Call Trace:
[92062.990067][ T5238] [<ffffffff800061bc>] dump_backtrace+0x28/0x30
[92062.990174][ T5238] [<ffffffff80b8adb0>] show_stack+0x38/0x44
[92062.990293][ T5238] [<ffffffff80b9ed1a>] dump_stack_lvl+0x44/0x5c
[92062.990529][ T5238] [<ffffffff80b9ed4a>] dump_stack+0x18/0x20
[92062.990641][ T5238] [<ffffffff8004bd3c>] __might_resched+0x112/0x120
[92062.992136][ T5238] [<ffffffff8004bd94>] __might_sleep+0x4a/0x72
[92062.992308][ T5238] [<ffffffff8021f68e>] __might_fault+0x24/0x2c
[92062.992469][ T5238] [<ffffffff80599e64>] _copy_from_user+0x28/0xa4
[92062.992634][ T5238] [<ffffffff03bbe232>] penglai_enclave_run+0xae/0x38a [penglai]
[92062.992946][ T5238] [<ffffffff03bbeeac>] penglai_enclave_ioctl+0x18c/0x20a [penglai]
[92062.993322][ T5238] [<ffffffff802cffd2>] sys_ioctl+0x90/0xaa
[92062.993466][ T5238] [<ffffffff80b9f848>] do_trap_ecall_u+0xf0/0x104
[92062.993636][ T5238] [<ffffffff80003ea4>] ret_from_exception+0x0/0x64
[92062.995056][ T5238] Penglai Enclave is running
[92062.995631][ T5238] ecall function id: 0
[92062.996627][ T5238] enclave start ocall get_string
[92062.998201][ T5238] [Penglai Driver@penglai_enclave_run] return user for ocall 
[92063.003199][ T5238] secgear hello world!
[92063.003450][ T5238] enclave end ocall get_string
[92063.003702][ T5238] - - - enclave test lib function - - -
1 add 4 is 5.
6 minor 2 is 4.
2 multiply 3 is 6.
9 devide 3 is 3.
[92063.006187][ T5238] - - - test lib finish - - -
[92063.006510][ T5238] ecall output_bytes_written: 0
[92063.015000][ T5238] [Penglai Driver@penglai_enclave_resume] run returned successfully
DEBUG:[_penglai_destroy] enter function: _penglai_destroy
```

成功运行secgear_seal_data

```bash
[root@openeuler-riscv64 debug]# ./bin/secgear_seal_data
Create secgear enclave
DEBUG:[_penglai_create] enter function: _penglai_create
elf_preload: size=0x10dd0
DEBUG:[_penglai_create] penglai enclave create successfully! 
[92228.158967][ T5241] BUG: sleeping function called from invalid context at lib/usercopy.c:14
[92228.159417][ T5241] in_atomic(): 1, irqs_disabled(): 0, non_block: 0, pid: 5241, name: secgear_seal_da
[92228.159830][ T5241] preempt_count: 1, expected: 0
[92228.160083][ T5241] CPU: 0 PID: 5241 Comm: secgear_seal_da Tainted: G        W  OE      6.4.0+ #1
[92228.160501][ T5241] Hardware name: riscv-virtio,qemu (DT)
[92228.160599][ T5241] Call Trace:
[92228.160665][ T5241] [<ffffffff800061bc>] dump_backtrace+0x28/0x30
[92228.160808][ T5241] [<ffffffff80b8adb0>] show_stack+0x38/0x44
[92228.161680][ T5241] [<ffffffff80b9ed1a>] dump_stack_lvl+0x44/0x5c
[92228.162128][ T5241] [<ffffffff80b9ed4a>] dump_stack+0x18/0x20
[92228.162500][ T5241] [<ffffffff8004bd3c>] __might_resched+0x112/0x120
[92228.162903][ T5241] [<ffffffff8004bd94>] __might_sleep+0x4a/0x72
[92228.163171][ T5241] [<ffffffff8021f68e>] __might_fault+0x24/0x2c
[92228.163499][ T5241] [<ffffffff80599e64>] _copy_from_user+0x28/0xa4
[92228.163887][ T5241] [<ffffffff03bbe232>] penglai_enclave_run+0xae/0x38a [penglai]
[92228.164533][ T5241] [<ffffffff03bbeeac>] penglai_enclave_ioctl+0x18c/0x20a [penglai]
[92228.165111][ T5241] [<ffffffff802cffd2>] sys_ioctl+0x90/0xaa
[92228.165398][ T5241] [<ffffffff80b9f848>] do_trap_ecall_u+0xf0/0x104
[92228.165805][ T5241] [<ffffffff80003ea4>] ret_from_exception+0x0/0x64
[92228.166756][ T5241] Penglai Enclave is running
[92228.167294][ T5241] ecall function id: 0
[92228.173244][ T5241] [seal_key] in:abcd123
[92228.174531][ T5241] ecall output_bytes_written: 48
[92228.193660][ T5241] [Penglai Driver@penglai_enclave_run] run returned successfully
Data to encrypt
DEBUG:[_penglai_destroy] enter function: _penglai_destroy
```

![Image](./tee11.png)

## 5 结论

1）完成测试，但也发现版本和仓库没有统一好，还是建议尽快改进。

例如2403版本是无法完成example编译的，不会有执行文件，见下图：

![Image](./tee14.png)

再如最新版本，中的penglai sdk编译，会出错，见下图：

![Image](./tee13.png)

再如penglai官方readme中a52e409分支的penglai_secGear_env.tar.gz中的penglai sdk编译，会出错，见下图：

![Image](./tee12.png)

好在第四次尝试penglai sdk版本，总算是正确打开了secGear及其examples。

2）加载penglai内核模块penglai.ko时出现的警告提示，这是由于penglai sdk默认支持的ko文件是openeuler2303版本，而本教程中使用的ko文件是openeuler2309版本所致，及内核升级导致，社区暂时无人处理，目前未发现该问题会影响example的正常运行。不用过于纠结这个警告。

```bash
[91847.815920][ T5230] penglai: loading out-of-tree module taints kernel.
[91847.818292][ T5230] penglai: module verification failed: signature and/or required key missing - tainting kernel
```

3）example运行时会出现如下bug提示，该提示是linux kernel 6.X之后增加的检测，如果在拿锁过程中申请分配物理内存，提示可能有死锁风险，但是不影响程序正确运行。不用过于纠结这个警告。

```bash
BuG: sleeping function called from invalid context at lib/usercopy.c:14
```

4）总之，penglai加油！
## 6 参考资料

https://github.com/Penglai-Enclave/Penglai-secGear/blob/a52e409930f20f1148b050c45e28500dd02325d6/docs/riscv_tee.md

https://github.com/Penglai-Enclave/Penglai-secGear/blob/8f7f880f907a4236d8f1c7ff969ead78eef0756d/docs/riscv_tee.md

https://github.com/Penglai-Enclave/Penglai-secGear/blob/oe-release/docs/riscv_tee.md