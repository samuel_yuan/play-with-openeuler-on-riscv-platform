# 操作手册：Test Ceph for openEuler on QEMU part b

---

## 1 目标

Ceph是一种为优秀的性能、可靠性和可扩展性而设计的统一的、分布式文件系统。Ceph 不仅仅是一个文件系统，还是一个有企业级功能的对象存储生态环境。

本教程验证openeuler riscv的系统环境是否达到运行Ceph的要求。Ceph的配置灵活，功能强大，在riscv环境中，即便执行最简单的单机部署也有一些难度，同时未来也会配合SPDK一同工作，因此测试工作将按最简单机部署，分阶段进行，循序渐进。

上篇教程，我们测试了ceph集群的mon，这篇教程我们测试ceph集群的osd、mgr、dashboard。

## 2 系统环境

系统采用openEuler riscv 2303 qemu版本

[openEuler riscv 2303](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.03-V1-riscv64/QEMU/)，kernel 6.1.19，gcc 10.3.1，Python 3.10.9

## 3 测试ceph集群的osd

上篇教程中，我们准备了sd.img，本教程中会使用到。启动openEuler riscv 2303 qemu版本，查看设备情况。

```bash
[root@openeuler-riscv64 ~]# ls /dev/vdb
/dev/vdb
```

检查附加的磁盘，为没有数据的裸磁盘。

```bash
[root@openeuler-riscv64 ~]# parted /dev/vdb print
Error: /dev/vdb: unrecognised disk label
Model: Virtio Block Device (virtblk)                                      
Disk /dev/vdb: 537MB
Sector size (logical/physical): 512B/512B
Partition Table: unknown
Disk Flags: 
```

检查lvm设备

```bash
[root@openeuler-riscv64 ~]# ceph-volume lvm list
No valid Ceph lvm devices found
```

采用SHORT FORM的方式，即BLUESTORE的方式，添加osd

```bash
sudo ceph-volume lvm create --data /dev/vdb
```

![Image](./osd1.png)

再次查看lvm设备

```bash
[root@openeuler-riscv64 ~]# sudo ceph-volume lvm list
====== osd.0 =======
  [block]       /dev/ceph-10fe0714-d8b9-422a-82b1-f5e1febfe8ad/osd-block-8d0fdbd3-72f7-4cf1-bb1a-a3acb8386afe
      block device              /dev/ceph-10fe0714-d8b9-422a-82b1-f5e1febfe8ad/osd-block-8d0fdbd3-72f7-4cf1-bb1a-a3acb8386afe
      block uuid                nh1wBJ-CSn3-vFbF-LlWF-MeO3-607J-asrIhU
      cephx lockbox secret      
      cluster fsid              da7f8f09-4320-468f-9af9-657568f2e626
      cluster name              ceph
      crush device class        None
      encrypted                 0
      osd fsid                  8d0fdbd3-72f7-4cf1-bb1a-a3acb8386afe
      osd id                    0
      osdspec affinity          
      type                      block
      vdo                       0
      devices                   /dev/vdb
```

启动osd

```bash
ceph-osd -i 0
```

查看ceph集群状态，osd启动成功。

```bash
[root@openeuler-riscv64 ~]# ceph -s
  cluster:
    id:     da7f8f09-4320-468f-9af9-657568f2e626
    health: HEALTH_WARN
            mon is allowing insecure global_id reclaim
            no active mgr
            1 monitors have not enabled msgr2
  services:
    mon: 1 daemons, quorum openeuler-riscv64 (age 23m)
    mgr: no daemons active
    osd: 1 osds: 1 up, 1 in (since 82s)
  data:
    pools:   0 pools, 0 pgs
    objects: 0 objects, 0 B
    usage:   0 B used, 0 B / 0 B avail
    pgs:     
```

## 4 测试ceph集群的mgr

修复1 monitors have not enabled msgr2的health警告，启用msgr2可以实现通信的加密，提升安全性。

```bash
ceph mon enable-msgr2
```

再次查看ceph状态，发现1 monitors have not enabled msgr2的health警告已看不到了。

安装mgr，设置mgr节点。

查看ceph目录

```bash
[root@openeuler-riscv64 ~]# ls /var/lib/ceph/
bootstrap-mds  bootstrap-osd  bootstrap-rbd-mirror  crash  mgr	osd	 tmp
bootstrap-mgr  bootstrap-rbd  bootstrap-rgw	    mds    mon	radosgw
```

查看mgr目录，为空

```bash
ls /var/lib/ceph/bootstrap-mgr/
ls /var/lib/ceph/mgr/
```

查看key的信息，所有角色的启动key都已准备好

```bash
ceph auth list
```

![Image](./mgr1.png)

将key导入到mgr启动目录

```bash
ceph auth get client.bootstrap-mgr -o /var/lib/ceph/bootstrap-mgr/ceph.keyring
```

查看keyring

```bash
[root@openeuler-riscv64 ~]# cat /var/lib/ceph/bootstrap-mgr/ceph.keyring 
[client.bootstrap-mgr]
	key = AQAYOyVlCJ+iGxAAVnv5hWyPmqc+jrS/BOBBhw==
	caps mon = "allow profile bootstrap-mgr"
```

建立mgr数据目录

```bash
mkdir /var/lib/ceph/mgr/ceph-openeuler-riscv64
```

建立对应的授权key文件

```bash
ceph auth get-or-create mgr.openeuler-riscv64 mon 'allow profile mgr' osd 'allow *' mds 'allow *'
```

查看key信息，可以看到mgr.openeuler-riscv64的key已添加上。

```bash
ceph auth list
```

如果输入错误的名称，可以通过下述语句将错误的key删除

```bash
ceph auth rm mgr.openeuler-riscv64
```

将此key文件命名为keyring输出到mgr数据目录

```bash
ceph auth get mgr.openeuler-riscv64 -o /var/lib/ceph/mgr/ceph-openeuler-riscv64/keyring
```

查看keyring

```bash
[root@openeuler-riscv64 ceph]# cat /var/lib/ceph/mgr/ceph-openeuler-riscv64/keyring
[mgr.openeuler-riscv64]
	key = AQDDRyZl5Hg1FBAAsFQEnRHL9PvGQDxJKu+gig==
	caps mds = "allow *"
	caps mon = "allow profile mgr"
	caps osd = "allow *"
```

启动ceph-mgr，并查看状态

```bash
ceph-mgr -i openeuler-riscv64
或者
sudo systemctl stop ceph-mgr@openeuler-riscv64
sudo systemctl start ceph-mgr@openeuler-riscv64
sudo systemctl enable ceph-mgr@openeuler-riscv64
sudo systemctl -l --no-page status ceph-mgr@openeuler-riscv64
```

![Image](./mgr2.png)

![Image](./mgr3.png)

查看ceph集群mgr节点状态，看到已经激活。观察到还有警告，执行下面的语句禁用不安全模式。

```bash
ceph config set mon auth_allow_insecure_global_id_reclaim false
```

再次查看ceph集群状态，全部正常。

![Image](./mgr4.png)

## 5 测试ceph集群的dashboard

查看dashboard的安装情况

```bash
[root@openeuler-riscv64 mgr]# dnf list ceph-mgr-dashboard
Last metadata expiration check: 3:44:43 ago on Wed 11 Oct 2023 01:32:03 PM CST.
Installed Packages
ceph-mgr-dashboard.noarch             2:16.2.7-15.oe2303              @ceph-user
```

mgr节点主持dashboard的webserver，这里不启用ssl

```bash
sudo ceph config set mgr mgr/dashboard/ssl false
```

激活dashboard

```bash
sudo ceph mgr module disable dashboard
sudo ceph mgr module enable dashboard
```

设置dashboard监听地址和端口

```bash
ceph config set mgr mgr/dashboard/openeuler-riscv64/server_addr 10.0.2.15
ceph config set mgr mgr/dashboard/openeuler-riscv64/server_port 9090
```

获取dashboard访问网址

```bash
[root@openeuler-riscv64 ceph]# sudo ceph mgr services
{
    "dashboard": "http://10.0.2.15:9090/"
}
```

查看网络状态

```bash
[root@openeuler-riscv64 ~]# netstat -tnlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
tcp        0      0 10.0.2.15:9090          0.0.0.0:*               LISTEN      470/ceph-mgr        
tcp        0      0 10.0.2.15:6803          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6802          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6801          0.0.0.0:*               LISTEN      470/ceph-mgr        
tcp        0      0 10.0.2.15:6800          0.0.0.0:*               LISTEN      470/ceph-mgr        
tcp        0      0 10.0.2.15:6807          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6806          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6805          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6804          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6809          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6808          0.0.0.0:*               LISTEN      1026/ceph-osd       
tcp        0      0 10.0.2.15:6789          0.0.0.0:*               LISTEN      472/ceph-mon        
tcp        0      0 10.0.2.15:3300          0.0.0.0:*               LISTEN      472/ceph-mon        
tcp        0      0 0.0.0.0:111             0.0.0.0:*               LISTEN      328/rpcbind         
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      424/sshd: /usr/sbin 
tcp6       0      0 :::111                  :::*                    LISTEN      328/rpcbind         
tcp6       0      0 :::22                   :::*                    LISTEN      424/sshd: /usr/sbin 
```

看上去dashboard的网址已经准备好。设置ceph用户admin及密码

```bash
ceph dashboard ac-user-create admin 123456 administrator
```

出现以下错误，据称这是python 3.10的bug

```bash
PY_SSIZE_T_CLEAN macro must be defined for '#' formats
```

查看ceph集群状态，出现错误Module 'devicehealth' has failed: PY_SSIZE_T_CLEAN macro must be defined for '#' formats，目前看来这种问题是python 3.10的bug带来的。

![Image](./dashboard1.png)

## 6 总结

1、openeuler的系统环境基本满足ceph的运行，osd、mgr、dashboard等均能正常运行。

2、其中的dashboard无法被访问到，原因是python 3.10的bug，可以通过ceph的patch解决，下面是gitee上的参考issue，还希望riscv版本的openeuler进行改进。

https://gitee.com/src-openeuler/ceph/issues/I61IYV

![Image](./ceph-bug.png)

## 7 参考资料

https://docs.ceph.com/en/reef/mgr/dashboard/

https://huaweicloud.csdn.net/64f69d8c87b26b6585a1dc2c.html

https://www.baidu.com/link?url=bOGfZbYifwfD-paaPb8b2O9EaTyiZod-u4rMNaK-8Rue7rGQB5ZX8paFxz5b-KY4i65JC9MuWA-e1mUbEe43tkggR3lp45KWw81qeF3Dzma&wd=&eqid=9aa8caa70003532900000006652656a4

https://www.php1.cn/detail/openEuler_TongGu_1527c48e.html

https://www.cnblogs.com/cyh00001/p/16783700.html
