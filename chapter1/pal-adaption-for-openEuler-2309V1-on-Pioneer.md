# 操作手册：SDLPAL adaption for openEuler2309V1 on Pioneer

---

## 1 目标

在MilkV Pioneer上的openEuler2309V1适配游戏仙剑奇侠传1代，并能让游戏真正玩起来，可用于高校开放游园时使用，诱引玩家入坑openEuler2309 RISC-V。

## 2 下载源码及编译

1）下载仙剑奇侠传1代的源码

```bash
git clone https://github.com/sdlpal/sdlpal.git
cd sdlpal
git submodule update --init --recursive
```

2）编译源码

```bash
cd unix
make
```

完成编译

![Image](./pal1.png)

## 3 准备游戏数据文件

1）下载仙剑1dos版：https://www.3h3.com/danji/150.html

2）sdlpal使用的都是小写文件名，需要下载重命名软件，将仙剑1dos版pal目录中所有的文件名和扩展名全部重命名为小写：https://www.advancedrenamer.com/download

3）将pal目录拷贝至sdlpal/unix/中，查看pal目录，所有文件名均为小写。

![Image](./pal2.png)

4）然后将unix中编译好的sdlpal执行文件拷贝到sdlpal/unix/pal/中，进入pal目录，执行sdlpal。

```bash
cp sdlpal pal/
./sdlpal
```

游戏成功运行。

![Image](./pal3.png)

![Image](./pal4.png)

![Image](./pal5.png)

## 4 小结

1）SDL游戏实际上还有许多，目前大多支持到SDL2，但是在openeuler2309V1中支持的并不太好，缺乏SDL2_mixer和SDL2_image。

2）我曾尝试从fedora仓库下载SDL2_mixer和SDL2_image的包进行安装，而且也安装成功了，但在编译游戏源码时会出错。具体步骤如下：

```bash
#安装依赖
dnf install libwebp-devel
#安装SDL2_mixer
wget http://fedora.riscv.rocks/kojifiles/packages/SDL2_mixer/2.6.3/4.fc40/riscv64/SDL2_mixer-2.6.3-4.fc40.riscv64.rpm
wget http://fedora.riscv.rocks/kojifiles/packages/SDL2_mixer/2.6.3/4.fc40/riscv64/SDL2_mixer-devel-2.6.3-4.fc40.riscv64.rpm
rpm -ivh SDL2_mixer-2.6.3-4.fc40.riscv64.rpm
rpm -ivh SDL2_mixer-devel-2.6.3-4.fc40.riscv64.rpm
#安装SDL2_image
wget http://fedora.riscv.rocks/kojifiles/packages/SDL2_image/2.8.2/3.fc40/riscv64/SDL2_image-devel-2.8.2-3.fc40.riscv64.rpm
 wget http://fedora.riscv.rocks/kojifiles/packages/SDL2_image/2.8.2/3.fc40/riscv64/SDL2_image-2.8.2-3.fc40.riscv64.rpm
rpm -ivh SDL2_image-2.8.2-3.fc40.riscv64.rpm
rpm -ivh SDL2_image-devel-2.8.2-3.fc40.riscv64.rpm
```

但在编译游戏代码时，会出现以下错误（类似错误在多款游戏源码的编译出现，例如Super-Mario-Remake和c-dogs-sdl）：

```bash
/usr/bin/ld: /lib64/lp64d/libSDL2_image.so: undefined reference to `SDL_roundf'
collect2: error: ld returned 1 exit status
```

用不了，只能卸载有问题的SDL2_image包

```bash
rpm -evh SDL2_image-2.8.2-3.fc40.riscv64.rpm
rpm -evh SDL2_image-devel-2.8.2-3.fc40.riscv64.rpm
#或
dnf remove SDL2_image
```

3）暂不深究，最后，希望openeuler能够完善SDL2相关的包，以支持更多的游戏。

## 5 参考文献

https://github.com/sdlpal/sdlpal

http://blog.chinaunix.net/uid-20587912-id-405128.html

https://blog.csdn.net/lianbaixue/article/details/7423610