# 操作手册：Test Ceph for openEuler on QEMU part a

---

## 1 目标

Ceph是一种为优秀的性能、可靠性和可扩展性而设计的统一的、分布式文件系统。Ceph 不仅仅是一个文件系统，还是一个有企业级功能的对象存储生态环境。

本教程验证openeuler riscv的系统环境是否达到运行Ceph的要求。Ceph的配置灵活，功能强大，在riscv环境中，即便执行最简单的单机部署也有一些难度，同时未来也会配合SPDK一同工作，因此测试工作将按最简单机部署，分阶段进行，循序渐进。本篇重在测试Ceph的集群监视器，即监视功能。

## 2 系统环境

系统采用openEuler riscv 2303 qemu版本

[openEuler riscv 2303](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.03-V1-riscv64/QEMU/)，kernel 6.1.19，gcc 10.3.1，Python 3.10.9

## 3 准备工作

准备用于ceph挂载的img。本教程中该img不会使用，但是按流程还是需要准备一下。

```bash
qemu-img create sd.img 512m
```

在start_vm_xfce.sh中加入如下两行，通过qemu加载sd.img

```bash
  -drive file=sd.img,format=raw,id=hd1
  -device virtio-blk-device,drive=hd1 
```

执行start_vm_xfce.sh启动系统后，查看附加的磁盘。

```bash
[root@openeuler-riscv64 ~]# ls /dev/vdb
/dev/vdb
```

```bash
[root@openeuler-riscv64 ~]# parted /dev/vdb
GNU Parted 3.5
Using /dev/vdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) print                                                            
Error: /dev/vdb: unrecognised disk label
Model: Virtio Block Device (virtblk)                                      
Disk /dev/vdb: 537MB
Sector size (logical/physical): 512B/512B
Partition Table: unknown
Disk Flags: 
(parted) quitr                 
......
(parted) quit
```

有些情况会用到ceph用户，因此先创建用户ceph

```bash
useradd ceph
passwd ceph
```

每个节点上添加ceph用户免密执行root权限

```bash
echo "ceph    ALL=(ALL)       NOPASSWD: ALL" >>  /etc/sudoers
```

如需在ceph用户下进行, 进入ceph用户空间

```bash
su - ceph
```

本篇教程主要在root用户下完成部署。

## 4 检查环境

查看ntp已安装和时间同步。

```bash
[root@openeuler-riscv64 ~]# dnf list ntp
Last metadata expiration check: 2:11:23 ago on Mon 25 Sep 2023 03:31:49 PM CST.
Installed Packages
ntp.riscv64                     4.2.8p15-7.oe2303                      @mainline
[root@openeuler-riscv64 ~]# timedatectl 
               Local time: Sun 2023-09-24 13:06:35 CST
           Universal time: Sun 2023-09-24 05:06:35 UTC
                 RTC time: Sun 2023-09-24 05:06:35
                Time zone: Asia/Shanghai (CST, +0800)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

查看ceph软件包可用。

```bash
[root@openeuler-riscv64 ~]# dnf list ceph
Last metadata expiration check: 0:51:32 ago on Sun 24 Sep 2023 12:13:23 PM CST.
Available Packages
ceph.riscv64                    2:16.2.7-15.oe2303                     ceph-user
ceph.src                        2:16.2.7-15.oe2303                     ceph-user
```

查看snappy可用版本为1.1.9, 这个版本禁用RTTI ,会导致集群报警告，也许会出现异常。最好是换成1.1.8版本。目前repo中没有这个版本，考虑到本篇教程不涉及osd的配置，预计不会用到该软件，因此snappy暂不安装。

```bash
[root@openeuler-riscv64 ~]# dnf list snappy
Last metadata expiration check: 0:52:21 ago on Sun 24 Sep 2023 12:13:23 PM CST.
Available Packages
snappy.riscv64                      1.1.9-2.oe2303                      mainline
```

查看lvm2已安装

```bash
[root@openeuler-riscv64 ~]# dnf list lvm2
Last metadata expiration check: 0:53:45 ago on Sun 24 Sep 2023 12:13:23 PM CST.
Installed Packages
lvm2.riscv64                    8:2.03.18-2.oe2303                     @mainline
```

## 5 安装与配置

考虑到openeuler repo中没有ceph-deploy，无法自动化地完成 Ceph 集群的部署、升级、扩容、故障恢复等任务，只能手动部署。

使用ifconfig查询主机的IP地址，并解析主机名，openeuler-riscv64

```bash
vi /etc/hosts
```

加入

```bash
10.0.2.15 openeuler-riscv64 openeuler-riscv64.local
```

安装ceph

```bash
dnf install -y ceph ceph-radosgw
```

查看版本为16.2.7，安装成功。

```bash
[root@openeuler-riscv64 ceph]# ceph --version
ceph version 16.2.7 (dd0603118f56ab514f133c8d2e3adfc983942503) pacific (stable)
```

生成uuid

```bash
[root@openeuler-riscv64 ~]# uuidgen
da7f8f09-4320-468f-9af9-657568f2e626
```

创建配置文件

```bash
sudo vim /etc/ceph/ceph.conf
```

加入以下内容。

```bash
[global]
fsid = da7f8f09-4320-468f-9af9-657568f2e626
mon_initial_members = openeuler-riscv64
mon_host = 10.0.2.15
public_network = 10.0.2.0/24
cluster network = 10.0.2.0/24
auth_cluster_required = cephx
auth_service_required = cephx
auth_client_required = cephx
osd_pool_default_size = 1
osd_pool_default_min_size = 1
osd_crush_chooseleaf_type = 0
```

创建mon的key信息（参考官网）

```bash
chmod 777 -R /tmp
sudo ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
sudo ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
sudo ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
sudo ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
sudo ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
sudo chown ceph:ceph /tmp/ceph.mon.keyring
```

创建mon的map信息

```bash
monmaptool --create --add openeuler-riscv64 10.0.2.15 --fsid da7f8f09-4320-468f-9af9-657568f2e626 /tmp/monmap
```

创建mon的目录信息

```bash
sudo mkdir /var/lib/ceph/mon/ceph-openeuler-riscv64
sudo chown -R ceph:ceph /var/lib/ceph/mon/ceph-openeuler-riscv64
chmod 777 -R /var/lib/ceph/mon/ceph-openeuler-riscv64
```

创建mon

```bash
sudo -u ceph ceph-mon --mkfs -i openeuler-riscv64 --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
```

## 6 运行

启动mon

```bash
sudo systemctl start ceph-mon@openeuler-riscv64
```

查看运行状态，看起来启动运行成功了。

```bash
sudo systemctl -l --no-page status ceph-mon@openeuler-riscv64
```

![Image](./ceph1.png)

设置开机启动

```bash
sudo systemctl enable ceph-mon@openeuler-riscv64
```

观察mon进程，发现没有ceph进程。

```bash
ps axu|grep mon
```

再试试查看ceph状态，长时间没有反应。

```bash
ceph -s
```

再次查看运行状态，发现ceph启动失败。

![Image](./ceph2.png)

![Image](./ceph3.png)

编辑ceph-mon@.service的内容，将MemoryDenyWriteExecute改成false。

```bash
vi /usr/lib/systemd/system/ceph-mon@.service
```

再次启动后，查看mon进程，可以看到ceph进程有了

![Image](./ceph4.png)

再次查看ceph状态，发现没有问题了。

![Image](./ceph5.png)

## 7 总结

1、openeuler的系统环境基本满足ceph的运行，osd_crush_chooseleaf_type这个默认是1表示不允许把数据的不同副本放到1个节点上，单机环境伪分布式测试时应设置为0。

2、从以上运行结果来看，启动运行成功，过了几分钟后，ceph进程自动结束。这应该是修包的时候服务配置文件没改过来，需要手动修改service文件，然后可以正常运行。

## 8 参考资料

https://docs.ceph.com/en/reef/install/manual-deployment/

https://zhuanlan.zhihu.com/p/383846160

https://www.baidu.com/link?url=v0oXw6OnckGnaftFO9PSHA82OfccBUBKDJ76yocI7KjhLkL4SOfTB79AbtLHZ9t9Rg0IrCIeqD8vBGwpVGvbMq&wd=&eqid=e430d1b50005e74f0000000665251a67

https://blog.csdn.net/qq_37382917/article/details/125306379

https://huaweicloud.csdn.net/64f69d8c87b26b6585a1dc2c.html

https://bugs.launchpad.net/ubuntu/hirsute/+source/ceph/+bug/1917414