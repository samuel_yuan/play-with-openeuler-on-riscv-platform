# 操作手册：Build SPDK 2305 for openEuler on QEMU

---

## 1 目标

SPDK (Storage performance development kit, http://spdk.io) 是由Intel发起、用于加速使用NVMe SSD作为后端存储的应用软件加速库。Intel对SPDK的定义是利用用户态、异步、轮询方式的NVMe驱动，用于加速NVMe SSD作为后端存储使用的应用软件的加速库。

本教程验证openeuler riscv的系统环境是否达到编译以及运行SPDK的要求。本篇重在验证环境，暂不涉及讨论RDMA等扩展内容，仅说明最小安装运行的环境。读者如有兴趣可以根据这篇基础教程，尝试提交扩展用例。当然，我后续也会设计并分享扩展用例。

## 2 系统环境

openEuler riscv 2303，kernel 6.1.19，gcc 10.3.1，DPDK 23.07

## 3 下载源码

下载最新发布的分支是v23.05.x，直接下载最新的就是这个版本了。

```bash
git clone https://github.com/spdk/spdk --recursive
```

检查源码的Makefile，发现其中许多包需要用pip包管理来安装，但是openEuler系统没有支持riscv的pip源，一些依赖包主要由dnf包管理。因此，我们还需要安装一些openeuler仓库中的软件包。

```bash
dnf install python3-paramiko python3-pexpect python3-pandas python3-magic  python3-pyparsing meson ninja-build python3-grpcio python3-grpcio-gcp python3-configshell python3-pyyaml autoconf automake libtool help2man numactl-devel nasm python3-scikit*
dnf install uuid uuid-devel
```

进入spdk目录，编辑脚本并注释掉相关库的pip安装。

```bash
cd spdk/
sudo vim scripts/pkgdep/openeuler.sh
```

例如，openeuler仓库中有的包就尽量dnf安装，实在没有的包就不要注释掉，如ijson，在编译过程中这个包可以由pip下载并编译安装。

```bash
#pip3 install ninja
#pip3 install meson
#pip3 install pyelftools
pip3 install ijson
#pip3 install python-magic
if ! [[ $ID == centos && $VERSION_ID == 7 ]]; then
        # Problem with modules compilation on Centos7
        #pip3 install grpcio
        #pip3 install grpcio-toolsi
        pip3 install ijson
fi
#pip3 install pyyaml
```

如下包有点大，可能会影响整体编译，建议单独安装。

```bash
pip3 install grpcio-tools
```

## 4 安装依赖

执行最小依赖安装，不带任何参数。

```bash
sudo scripts/pkgdep.sh
```

## 5 编译

SPDK使用了DPDK的一些功能和机制。上篇教程中，我们把dpdk安装到/usr/local,那么在spdk源码目录执行：

```bash
./configure --with-dpdk=/usr/local
make
```

第一次编译，发现错误，编译无法完成。以下是错误信息：

```bash
CC examples/accel/perf/accel_perf.o
make[3]: *** No rule to make target '/usr/local/lib/librte_bus_pci.a', needed b
y '/root/spdk/build/examples/accel_perf'.  Stop.
rm accel_perf.d
make[2]: *** [/root/spdk/mk/spdk.subdirs.mk:16: perf] Error 2
make[1]: *** [/root/spdk/mk/spdk.subdirs.mk:16: accel] Error 2
```

根据以上错误信息，我们查看/usr/local/lib目录，未发现相关库文件。然后查看/usr/local/lib64目录，发现了librte_bus_pci.a。这说明DPDK安装的库文件目录和SPDK识别的库文件目录不一致。将库文件直接复制到lib目录中。

```bash
sudo cp /usr/local/lib64/*.a /usr/local/lib/
```

再次编译，编译成功。

![Image](./spdk1.png)

## 6 运行

直接运行单元测试

```bash
./test/unit/unittest.sh
```

你会看到一些error信息，这些信息也是测试套件的一部分。最后的信息指明测试的成功或失败。从下图可以看出，单元测试是成功的。

![Image](./spdk2.png)

然后，执行例子：

```bash
[root@openeuler-riscv64 spdk]# ./build/examples/identify
EAL: TSC using RISC-V rdtime.
TELEMETRY: No legacy callbacks, legacy socket not created
[2023-08-18 17:25:03.855872] pci_dpdk.c:  64:dpdk_pci_init: *ERROR*: DPDK version 23.07.0 is not supported.
[2023-08-18 17:25:03.857421] init.c: 512:spdk_env_dpdk_post_init: *ERROR*: pci_env_init() failed
Unable to initialize SPDK env
```

由此可见，目前安装的spdk是23.05不支持dpdk23.07版本。

## 7 总结

1、openeuler的系统环境已经满足SPDK的编译和运行。

2、从以上运行结果来看，spdk 23.05不支持dpdk 23.07，实际上它支持的dpdk版本是23.03。考虑到之前安装的dpdk版本为2307，同时spdk 23.09版本即将到来（平均每4个月更新一个版本）。我们9月份左右再分享一些用例玩。

3、目前的基础用例，没有考虑打开一些特定的扩展开关，如RDMA等。以上对系统环境的验证工作，仅为后续更多高性能用例做了铺垫。

## 8 参考资料

https://spdk.io/doc/getting_started.html

https://blog.csdn.net/weixin_43273308/article/details/127715858

https://blog.csdn.net/weixin_60043341/article/details/126364756