# 操作手册：Build DPDK 2307 for openEuler on QEMU

---

## 1 目标

Intel DPDK全称Intel Data Plane Development Kit，是intel提供的数据平面开发工具集，为Intel architecture（IA）处理器架构下用户空间高效的数据包处理提供库函数和驱动的支持，它不同于Linux系统以通用性设计为目的，而是专注于网络应用中数据包的高性能处理。DPDK应用程序是运行在用户空间上利用自身提供的数据平面库来收发数据包，绕过了Linux内核协议栈对数据包处理过程。Linux内核将DPDK应用程序看作是一个普通的用户态进程，包括它的编译、连接和加载方式和普通程序没有什么两样。DPDK程序启动后只能有一个主线程，然后创建一些子线程并绑定到指定CPU核心上运行。

本教程验证openeuler riscv的系统环境是否达到编译以及运行DPDK的要求。本篇重在验证环境，暂不涉及讨论NUMA和SMP、巨页、Qemu模拟Numa架构等。读者如有兴趣可以根据这篇基础教程，尝试提交优化用例。当然，我后续也会设计并分享优化用例。

## 2 系统环境

```bash
#查系统
[root@openeuler-riscv64 ~]# cat /etc/os-release
NAME="openEuler"
VERSION="23.03"
ID="openEuler"
VERSION_ID="23.03"
PRETTY_NAME="openEuler 23.03"
ANSI_COLOR="0;31"
[root@openeuler-riscv64 ~]# uname -a
Linux openeuler-riscv64 6.1.19-2.oe2303.riscv64 #1 SMP Tue Apr  4 14:17:41 UTC 
2023 riscv64 riscv64 riscv64 GNU/Linux
#dnf install openeuler-lsb
[root@openeuler-riscv64 ~]# lsb_release -a
LSB Version:	n/a
Distributor ID:	openEuler
Description:	openEuler release 23.03
Release:	23.03
Codename:	n/a
#查编译器
[root@openeuler-riscv64 ~]# riscv64-linux-gnu-gcc --version
riscv64-linux-gnu-gcc (GCC) 10.3.1
Copyright (C) 2020 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
[root@openeuler-riscv64 ~]# riscv64-linux-gnu-g++ --version
riscv64-linux-gnu-g++ (GCC) 10.3.1
Copyright (C) 2020 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

## 3 前置准备

```bash
dnf install python3-pyelftools
dnf install python3-libfdt
dnf install dpkg dpkg-devel
dnf install autoconf automake doxygen cmake numactl*
dnf install libbsd libpcap libvirt libbpf libgpg-error-devel bridge-utils ncurses-devel pkgconf-devel
```

## 4 下载源码

下载最新发布的分支

```bash
sudo git clone -b releases https://github.com/DPDK/dpdk
```

查看版本为23.07.0

```bash
cd dpdk/
cat VERSION 
```

查看设置，openeuler已经支持了其中的主要编译器。ar、strip、pkgconfig等并非是必须，无需过分追求，没有也能正常编译。

```bash
[root@openeuler-riscv64 dpdk]# sudo cat ./config/riscv/riscv64_linux_gcc
[binaries]
c = ['ccache', 'riscv64-linux-gnu-gcc']
cpp = ['ccache', 'riscv64-linux-gnu-g++']
ar = 'riscv64-linux-gnu-ar'
strip = 'riscv64-linux-gnu-strip'
pcap-config = ''

[host_machine]
system = 'linux'
cpu_family = 'riscv64'
cpu = 'rv64gc'
endian = 'little'

[properties]
vendor_id = 'generic'
arch_id = 'generic'
pkg_config_libdir = '/usr/lib/riscv64-linux-gnu/pkgconfig'
```

## 5 编译

在dpdk源码目录中，查看配置。

```bash
meson configure
```

构建（本教程需要运行用例，因此将用例作为build目录的一部分，如果不需要用例，直接用meson build）

```bash
meson -Dexamples=all build
```

完成后在DPDK目录中会出现build目录，里面都是构建产生的文件，进入该目录编译。

```bash
cd build
ninja
```

## 6 安装运行

```bash
ninja install
```

执行上述指令，将dpdk安装到/usr/local中，例子程序在/usr/local/bin/，例如：

```bash
./dpdk-test
```

![Image](./dpdk1.png)

或者在dpdk/build/examples/中执行：

```bash
./dpdk-helloworld --no-huge
```

![Image](./dpdk2.png)

如果不带--no-huge，直接执行将出现错误。

![Image](./dpdk3.png)

根据错误提示，系统未能获取到巨页信息。那么我们看看巨页信息。

使用巨页需要在内核编译的时候打开相关的配置选项：CONFIG_HUGETLBFS, CONFIG_HUGETLB_PAGE，让我们看看openeuler 2303的内核配置是否打开了，可确认已打开巨页。

```bash
[root@openeuler-riscv64 boot]# grep HUGETLB /boot/config-6.1.19-2.oe2303.risv64 
CONFIG_CGROUP_HUGETLB=y
CONFIG_ARCH_WANT_GENERAL_HUGETLB=y
CONFIG_ARCH_SUPPORTS_HUGETLBFS=y
CONFIG_HUGETLBFS=y
CONFIG_HUGETLB_PAGE=y
```

命令 cat /proc/filesystems 还应该显示配置的“hugetlbfs”类型的文件系统在内核中。可以查看到如下信息。

```bash
nodev hugetlbfs
```

命令 cat /proc/sys/vm/表示当前配置的hugetlb数量内核中的页面。页面数为0，难怪，我们的dpdk未能获取到任何巨页信息。

```bash
[root@openeuler-riscv64 boot]# cat /proc/sys/vm/nr_hugepages 
0
```

也可通过命令 grep Huge /proc/meminfo查看

```bash
[root@openeuler-riscv64 boot]# grep Huge /proc/meminfo
AnonHugePages:     45056 kB
ShmemHugePages:        0 kB
FileHugePages:         0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:               0 kB
```

可以通过命令sudo sysctl vm.nr_hugepages=128修改巨页的数量为128页。

```bash
[root@openeuler-riscv64 boot]# sudo sysctl vm.nr_hugepages=128
vm.nr_hugepages = 128
[root@openeuler-riscv64 boot]# grep Huge /proc/meminfo
AnonHugePages:     45056 kB
ShmemHugePages:        0 kB
FileHugePages:         0 kB
HugePages_Total:     128
HugePages_Free:      128
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:          262144 kB
```

保留巨页内存

```bash
mkdir -p /mnt/hugepages
#默认2MB
mount -t hugetlbfs nodev /mnt/hugepages
```

再次运行

```bash
./dpdk-helloworld
```

![Image](./dpdk4.png)

这下运行成功了。


## 7 总结

1、从以上运行结果来看，qemu镜像是直接采用的openeuler 2303 riscv 的镜像，执行的qemu启动脚本也没有考虑NUMA架构的相关设置，因此在系统中只存在1个node，共享所有的内存资源，这就是SMP了。后续会视情况，用qemu来模拟NUMA主机来设计用例并验证相关应用。也许等赛昉的8100入手后，大小核的SOC玩这个会比较有意思。当然我也初步尝试了用qemu来模拟numa主机，例如：模拟2个socket槽，每个槽配置1个4核的CPU，再启动openeuler 2303 riscv系统时，会出现卡死的情况，这个后续有空再研究。

```bash
cmd="qemu-system-riscv64 \
  -nographic -machine virt \
  -smp cores="$vcpu",threads=1,sockets=2 -m "$memory"G \
  -object memory-backend-ram,id=mem0,size=4G \
  -object memory-backend-ram,id=mem1,size=4G \
  -numa node,memdev=mem0,cpus=0-3,nodeid=0 \
  -numa node,memdev=mem1,cpus=4-7,nodeid=1 \
  -display sdl \
  -audiodev pa,id=snd0 \
  -bios "$fw" \
  -drive file="$drive",format=qcow2,id=hd0 \
  -object rng-random,filename=/dev/urandom,id=rng0 \
  -device virtio-vga \
  -device virtio-rng-device,rng=rng0 \
  -device virtio-blk-device,drive=hd0 \
  -device virtio-net-device,netdev=usernet \
  -netdev user,id=usernet,hostfwd=tcp::"$ssh_port"-:22 \
  -device qemu-xhci -usb -device usb-kbd -device usb-tablet -device usb-audio,audiodev=snd0"
```

2、目前的基础用例，没有考虑绑定硬件，例如网卡。本教程暂不考虑IGB_UIO的问题。自v20.02版本以后，DPDK就默认关闭igb_uio模块。若构建它，需要配置文件选项CONFIG_RTE_EAL_IGB_UIO设置为enabled。配置文件dpdk/config/common_base中开启该配置CONFIG_RTE_EAL_IGB_UIO=y，注意这个文件是全局配置。

3、在openeuler系统环境下编译和在Ubuntu环境下交叉编译的差别。从dpdk编译目标数量来看，openeuler甚至比ubuntu还略微强大一些，我同时提供两种环境下的编译结果，[openeuler2303编译输出信息](./dpdk-openeuler23.03.txt)和[ubuntu20.04编译输出信息](./dpdk-ubuntu20.04.txt)。

在openeuler23.03环境下，Build targets in project: 702

在ubuntu20.04环境下，Build targets in project: 653

4、以上对系统环境的验证工作，为后续更多高性能用例做了铺垫。

## 8 参考资料

http://core.dpdk.org/doc/quick-start/

https://blog.csdn.net/a65460362/article/details/126509414

https://zhuanlan.zhihu.com/p/545286541?utm_id=0

https://blog.csdn.net/weixin_48101150/article/details/117824214