# 操作手册：Test vscode on openEuler2403 for qemu

---

## 1 目标

在openEuler2403上安装测试vscode，完成helloworld代码的编译和运行。

## 2 准备工作

系统环境，请下载基于UEFI EDK2的openEuler2403 Qemu镜像

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/testing/2403LTS-test/v1/QEMU/openEuler-24.03-V1-xfce-qemu-testing-uefi.qcow2.zst


Qemu的版本为9.02

执行start_vm_xfce_uefi.sh启动openEuler2403系统

## 3 安装

在xfce桌面下运行terminal终端，下载VSCodium软件包

```bash
sudo wget https://github.com/VSCodium/vscodium/releases/download/1.94.2.24286/VSCodium-linux-riscv64-1.94.2.24286.tar.gz
```

解压

```bash
sudo tar -zxvf VSCodium-linux-riscv64-1.94.2.24286.tar.gz
```

解压后，进入bin目录，运行VSCodium执行程序

```bash
cd bin
sudo ./codium --no-sandbox --user-data-dir ./
```

![Image](./vs1.png)

进入到vscode的图形界面

![Image](./vs2.png)

可以看到有许多插件可供下载

![Image](./vs3.png)

## 4 测试

创建helloworld的代码目录

![Image](./vs4.png)

![Image](./vs5.png)

安装插件C/C++和Code Runner,并编写helloworld代码

![Image](./vs6.png)

成功运行

## 5 小结

1）VSCodium已经在主线支持RISC-V架构，可以直接下载到。

2）根据测试情况，能够在openEuler2403 RISCV版本上编译运行基本的C语言代码。可喜可贺，家长们可以买RISCV平台，在上面安装VSCodium，可以放心让小朋友们愉快编程了。

## 6 参考资料

https://github.com/VSCodium/