# 操作手册：Install openEuler 2309V1 on Pioneer and Test SG2042 performance by Geekbench6

---

## 1 目标

在MilkV Pioneer上安装openEuler2309V1，使用Geekbench测试Pioneer CPU SG2042的性能（考虑到之前《Play with OpenEuler on VisionFive 1&2》课程中，已经介绍了多种benchmark软件用于测试。这里不再累述）, 并与其他CPU做简单的比较说明。

## 2 openEuler2309V1的安装

在ubuntu主机上，下载openEuler 2309V1 SG2042镜像:

https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.09-V1-riscv64/SG2042/openEuler-23.09-V1-xfce-sg2042-preview-refreshed.img.zst

说明：镜像的主板版本为1.3，而本文的Pioneer的主板版本为1.2。考虑到主板1.3优化了USB的走线，其他部分没有变化，因此我认为可以使用这个1.3版本的镜像，而实际上的确可行。

解压镜像得到img文件，然后使用balenaEtcher烧录工具，将img文件烧录到sdcard。

下载烧录工具balenaEtcher。

```bash
wget https://github.com/balena-io/etcher/releases/download/v1.7.9/balena-etcher-electron-1.7.9-linux-x64.zip
```

安装解压工具，并解压

```bash
sudo apt install libfuse2
sudo apt install zip unzip
sudo unzip balena-etcher-electron-1.7.9-linux-x64.zip
```

运行balenaEtcher

```bash
sudo ./balenaEtcher-1.7.9-x64.AppImage
```

选择sd卡的分区，完成烧录。

![Image](./sg1.png)

![Image](./sg2.png)

![Image](./sg3.png)

将sd卡插入Pioneer即可完成启动。

![Image](./sg4.png)

简单看了下CPU的情况，有4个NUMA节点，每个节点16核，共计64核。

## 3 Geekbench6的安装和测试

Geekbench已经支持RISCV，可以从官网直接下载，并解压。

```bash
wget https://cdn.geekbench.com/Geekbench-6.2.2-LinuxRISCVPreview.tar.gz
tar -zxvf Geekbench-6.2.2-LinuxRISCVPreview.tar.gz
```

以下是测试过程，显示了测试项目，测试结果是这些测试项目得分的综合评价：

```bash
[root@openeuler-riscv64 Geekbench-6.2.2-LinuxRISCVPreview]# ./geekbench6
Geekbench 6.2.2 Preview : https://www.geekbench.com/
Geekbench 6 for Linux/RISC-V is a preview build. Preview builds require an
active Internet connection and automatically upload benchmark results to the
Geekbench Browser.

System Information
  Operating System              openEuler 23.09
  Kernel                        Linux 6.1.61-4.oe2309.riscv64 riscv64
  Model                         Sophgo Mango
  Motherboard                   N/A

CPU Information
  Name                          rv64imafdcv
  Topology                      1 Processor, 1 Core, 64 Threads
  Base Frequency                0.00 Hz

Memory Information
  Size                          125 GB


Single-Core
  Running File Compression
  Running Navigation
  Running HTML5 Browser
  Running PDF Renderer
  Running Photo Library
  Running Clang
  Running Text Processing
  Running Asset Compression
  Running Object Detection
  Running Background Blur
  Running Horizon Detection
  Running Object Remover
  Running HDR
  Running Photo Filter
  Running Ray Tracer
  Running Structure from Motion

Multi-Core
  Running File Compression
  Running Navigation
  Running HTML5 Browser
  Running PDF Renderer
  Running Photo Library
  Running Clang
  Running Text Processing
  Running Asset Compression
  Running Object Detection
  Running Background Blur
  Running Horizon Detection
  Running Object Remover
  Running HDR
  Running Photo Filter
  Running Ray Tracer
  Running Structure from Motion

Uploading results to the Geekbench Browser. This could take a minute or two
depending on the speed of your internet connection.
Upload succeeded. Visit the following link and view your results online:
  https://browser.geekbench.com/v6/cpu/5114201
Visit the following link and add this result to your profile:
  https://browser.geekbench.com/v6/cpu/5114201/claim?key=624025
```

## 4 测试结果

可以在Geekbench官网上看到测试结果如下：

![Image](./sggk1.png)

![Image](./sggk2.png)

![Image](./sggk3.png)

![Image](./sggk4.png)

再来看看性能得分相近的CPU的测试情况：

1）单核得分和赛扬N2930 2165MHz相近，毕竟SG2042的主频也是2GHz

![Image](./sggk6.png)

2）多核得分和酷睿i7 2630QM 1995MHz相近，很难想象该款4核的CPU的多核得分和64核的SG2042的多核得分差不多。

![Image](./sggk5.png)

3）再来看看华为的8核Kunpeng Desktop Board（https://browser.geekbench.com/v6/cpu/3005695）， 操作系统也是openEuler2309，得分要高出SG2042很多。

![Image](./sggk7.png)

## 5 总结

1）SG2042的生态逐渐完善，系统安装变得非常简单，感谢PLCT实验室的努力。

2）Geekbench测试的结果表明，SG2042还有许多工作要做，才能发挥CPU的最大性能。