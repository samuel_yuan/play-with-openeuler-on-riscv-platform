# 操作手册：Build OvS 3.2 for openEuler on QEMU

---

## 1 目标

OVS(openvswitch)是开源的虚拟交换机。也是当前市场上云环境中部署份额最大的交换机。支持 openflow协议，ovsdb协议管理。一个OVS实例包括，ovsdb-server、ovs-vswitchd、datapath快转模块（linux内核中实现，可选的。dpdk模式是在用户态实现快转，并不需要内核态的datapath模块）。

本教程验证openeuler riscv的系统环境是否达到编译以及运行OvS的要求。本篇重在验证环境，并为后续实验做准备。

## 2 系统环境

openEuler riscv 2303，kernel 6.1.19，gcc 10.3.1，DPDK 23.07

## 3 下载源码

下载最新发布的分支是branch-3.2，直接下载这个版本。

```bash
sudo git clone -b branch-3.2 https://github.com/openvswitch/ovs
```

## 4 编译

构建编译设置

```bash
cd /ovs
./boot.sh
./configure --with-dpdk=yes
```

第一次构建，出现以下错误信息：

```bash
checking whether dpdk is enabled... yes
checking for DPDK... no
configure: error: Package requirements (libdpdk) were not met:
Package 'libdpdk', required by 'virtual:world', not found
Consider adjusting the PKG_CONFIG_PATH environment variable if you
installed software in a non-standard prefix.
Alternatively, you may set the environment variables DPDK_CFLAGS
and DPDK_LIBS to avoid the need to call pkg-config.
See the pkg-config man page for more details.
```

根据以上错误信息，构建脚本没有找到libdpdk，需要调整PKG_CONFIG_PATH环境变量。我们在/usr/local/lib64/pkgconfig/中找到了libdpdk。

```bash
[root@openeuler-riscv64 pkgconfig]# find / -name libdpdk.pc
/root/dpdk/build/meson-private/libdpdk.pc
find: ‘/run/user/996/gvfs’: Permission denied
/usr/local/lib64/pkgconfig/libdpdk.pc
```

设置环境变量。

```bash
export PKG_CONFIG_PATH=/usr/local/lib64/pkgconfig/:$PKG_CONFIG_PATH
```

再次执行脚本，构建成功。

![Image](./ovs1.png)

执行编译

```bash
make
```

编译成功

![Image](./ovs2.png)

## 5 安装运行

默认安装到/usr/local目录中

```bash
make install
```

直接运行测试套件

```bash
make check TESTSUITEFLAGS=-j8
```

运行成功

![Image](./ovs3.png)


运行结果。

![Image](./ovs4.png)

## 6 总结

1、openeuler的系统环境已经满足OvS的编译和运行。

2、从以上运行结果来看，2557个测试用例中，34个用例失败，成功率在99%，这是相当不错的结果。与此同时，我们也获取到了测试报告：ovs/tests/testsuite.log。

3、以上对系统环境的验证工作，仅为后续更多高性能用例做了铺垫。其中OvS可以通过dpdk加速，未来我们也将在这方面的高性能应用进行探索。

## 7 参考资料

https://docs.openvswitch.org/en/latest/topics/testing/#built-in-tooling

https://blog.csdn.net/weixin_60043341/article/details/126267743

https://zhuanlan.zhihu.com/p/590566341?utm_id=0

https://blog.csdn.net/sun172270102/article/details/120646363

https://blog.csdn.net/wayne8910/article/details/107181609/

https://www.jianshu.com/p/0faea1f431ec