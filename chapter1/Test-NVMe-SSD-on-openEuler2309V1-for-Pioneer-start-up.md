# 操作手册：Test NVMe SSD on openEuler2309V1 for Pioneer start-up and SPDK

---

## 1 目标

Pioneer的固态硬盘配置是随机的，主要为江波龙和致态的固态硬盘。很不幸，我手里拿到的是江波龙的固态硬盘，这让我伤神许久，反复测试后，果断换货成了雷克萨的固态硬盘，但是依然有些槽点。

本教程基于openEuler2309V1介绍固态硬盘相关基础操作，并在Pioneer上测试不同品牌固态硬盘的制作启动盘情况和测试SPDK+NVMe，为社区玩家给出固态硬盘的选型建议。

## 2 准备工作

系统环境，请下载基于UEFI EDK2的openEuler2309V1 Qemu镜像（参考之前教程制作的镜像, https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/community/samuel_yuan/openeuler2309/qemu-uefi/ ）

## 3 基础操作

使用SD卡启动openEuler，查看nvme

```bash
[root@openeuler-riscv64 ~]# nvme list
Node                  Generic               SN                   Model                                    Namespace Usage                      Format           FW Rev
--------------------- --------------------- -------------------- ---------------------------------------- --------- -------------------------- ---------------- --------
/dev/nvme0n1          /dev/ng0n1            NFA161Q008129        FORESEE XP1000F001T                      1           1.02  TB /   1.02  TB    512   B +  0 B   V1.28
```

分区

```bash
[root@openeuler-riscv64 ~]# fdisk /dev/nvme0n1
Welcome to fdisk (util-linux 2.39.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.
The device contains 'gpt' signature and it will be removed by a write command. See fdisk(8) man page and --wipe option for more details.
Device does not contain a recognized partition table.
Created a new DOS (MBR) disklabel with disk identifier 0xf117fa69.

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1):
First sector (2048-2000409263, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-2000409263, default 2000409263):

Created a new partition 1 of type 'Linux' and of size 953.9 GiB.

Command (m for help): p
Disk /dev/nvme0n1: 953.87 GiB, 1024209543168 bytes, 2000409264 sectors
Disk model: FORESEE XP1000F001T
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xf117fa69

Device         Boot Start        End    Sectors   Size Id Type
/dev/nvme0n1p1       2048 2000409263 2000407216 953.9G 83 Linux

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

显示分区情况

```bash
[root@openeuler-riscv64 /]# parted -l
Model: FORESEE XP1000F001T (nvme)
Disk /dev/nvme0n1: 1024GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  1024GB  1024GB  primary  ext4


Model: SD SD64G (sd/mmc)
Disk /dev/mmcblk1: 63.9GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  269MB   268MB   primary  fat16        lba
 2      269MB   806MB   537MB   primary  fat32        esp
 3      806MB   63.9GB  63.1GB  primary  ext4
```

格式化

```bash
[root@openeuler-riscv64 ~]# mkfs.ext4 -L files /dev/nvme0n1p1
mke2fs 1.47.0 (5-Feb-2023)
Discarding device blocks: done
Creating filesystem with 250050902 4k blocks and 62513152 inodes
Filesystem UUID: c57d8180-36c5-4e2c-aabe-5707842ad84e
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
        4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968,
        102400000, 214990848

Allocating group tables: done
Writing inode tables: done
Creating journal (262144 blocks):
done
Writing superblocks and filesystem accounting information: done
```

查看磁盘标识符nvme0n1p1的标识为0xf117fa69，启动盘的标识符为0x263b368a

```bash
[root@openeuler-riscv64 ~]# fdisk -l
Disk /dev/nvme0n1: 953.87 GiB, 1024209543168 bytes, 2000409264 sectors
Disk model: FORESEE XP1000F001T
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xf117fa69

Device         Boot Start        End    Sectors   Size Id Type
/dev/nvme0n1p1       2048 2000409263 2000407216 953.9G 83 Linux

Disk /dev/mmcblk1: 59.48 GiB, 63864569856 bytes, 124735488 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x263b368a

Device         Boot   Start       End   Sectors  Size Id Type
/dev/mmcblk1p1         2048    526335    524288  256M  c W95 FAT32 (LBA)
/dev/mmcblk1p2       526336   1574911   1048576  512M ef EFI (FAT-12/16/32)
/dev/mmcblk1p3      1574912 124735487 123160576 58.7G 83 Linux
```

查看磁盘的UUID

```bash
[root@openeuler-riscv64 ~]# lsblk -f
NAME        FSTYPE FSVER LABEL UUID                                 FSAVAIL FSUSE% MOUNTPOINTS
mmcblk1
├─mmcblk1p1 vfat   FAT16       773B-7467
├─mmcblk1p2 vfat   FAT32       773B-F986                             322.3M    37% /boot
└─mmcblk1p3 ext4   1.0         2708cd66-1441-42fd-a106-61244684fba5   12.7G    74% /
nvme0n1
└─nvme0n1p1 ext4   1.0   files c57d8180-36c5-4e2c-aabe-5707842ad84e
```

检查fstab，核对挂载磁盘UUID是否与实际磁盘UUID一致。不一致可能导致桌面启动不了。

```bash
vim /etc/fstab
```

挂载分区

```bash
[root@openeuler-riscv64 /]#mkdir -p /mnt/ssd
[root@openeuler-riscv64 /]# mount /dev/nvme0n1p1 /mnt/ssd
mount: (hint) your fstab has been modified, but systemd still uses
       the old version; use 'systemctl daemon-reload' to reload.
[root@openeuler-riscv64 ~]# umount /mnt/ssd
```

根据错误提示执行

```bash
[root@openeuler-riscv64 ~]# systemctl daemon-reload
```

再次执行，错误提示消失

```bash
[root@openeuler-riscv64 ~]# mount /dev/nvme0n1p1 /mnt/ssd
```

根据之前查看的UUID编辑fstab，实现ssd启动挂载。注意这里不会备份，也不会检查这个磁盘中的文件系统。

```bash
vim /etc/fstab
```

加入

```bash
UUID=c57d8180-36c5-4e2c-aabe-5707842ad84e /mnt/ssd ext4 defaults 0 0
```

存盘退出重启后，查看磁盘情况，nvme ssd已经随着启动正常挂载。

```bash
[root@openeuler-riscv64 ~]# lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
mmcblk1     179:0    0  59.5G  0 disk
├─mmcblk1p1 179:1    0   256M  0 part
├─mmcblk1p2 179:2    0   512M  0 part /boot
└─mmcblk1p3 179:3    0  58.7G  0 part /
nvme0n1     259:0    0 953.9G  0 disk
└─nvme0n1p1 259:1    0 953.9G  0 part /mnt/ssd
```

## 4 测试制作启动盘

方式A：使用SD卡启动系统，在openEuler环境中制作启动盘

```bash
sudo dd if=openEuler-23.09-V1-xfce-sg2042-preview-refreshed.img of=/dev/nvme0n1 bs=512K iflag=fullblock oflag=direct conv=fsync status=progress
```

方式B：使用Ubuntu主机系统制作启动盘

```bash
sudo dd if=openEuler-23.09-V1-xfce-sg2042-preview-refreshed.img of=/dev/sdb bs=512K iflag=fullblock oflag=direct conv=fsync status=progress
```

如果是早期的Pioneer，则需要替换其中的设备树文件mango-milkv-pioneer-v1.2.dtb

在制作启动盘的过程中，我们一共测试了江波龙，雷克萨，三星、海力士等品牌的固态硬盘。测试结果如下：

```bash
                江波龙XP1000F001T   雷克萨NM6A1         三星980     海力士PC711
启动盘制作方式A       Failed            Passed          Passed      Failed
启动盘制作方式B       Failed            Failed          Passed      Failed
```

## 5 测试SPDK+NVMe

接本教材的章节《操作手册：Build SPDK 2305 for openEuler on QEMU》。检查状态，发现NVMe没有成功绑定到PCI dev上。

```bash
[root@openeuler-riscv64 spdk]# HUGEMEM=2048 scripts/setup.sh
0003:c7:00.0 (1d97 5216): Active devices: data@nvme0n1, so not binding PCI dev
[root@openeuler-riscv64 spdk]# ./scripts/setup.sh status
0003:c7:00.0 (1d97 5216): Active devices: data@nvme0n1, so not binding PCI dev
Hugepages
node     hugesize     free /  total
node0   1048576kB        0 /      0
node0      2048kB     1024 /   1024
node1   1048576kB        0 /      0
node1      2048kB        0 /      0
node2   1048576kB        0 /      0
node2      2048kB        0 /      0
node3   1048576kB        0 /      0
node3      2048kB        0 /      0

Type                      BDF             Vendor Device NUMA    Driver           Device     Block devices
NVMe                      0003:c7:00.0    1d97   5216   unknown nvme             nvme0      nvme0n1
```

运行Helloworld用例

```bash
[root@openeuler-riscv64 spdk]# sudo build/examples/hello_world
```

![Image](./spdk_ssd.png)

没有找到NVMe控制器。

几家品牌的固态硬盘均没能找到NVMe控制器。

## 6 小结

1）测试结论：

```bash
                江波龙XP1000F001T   雷克萨NM6A1         三星980     海力士PC711
启动盘制作方式A       Failed            Passed          Passed      Failed
启动盘制作方式B       Failed            Failed          Passed      Failed
SPDK+NVMe            Failed            Failed          Failed      Failed
```

2）内核问题，存在bug，不能正常初始化NVMe。也许openEuler2403LTS版本已解决这个问题，后续再测试看看。

3）根据测试情况，固态硬盘的选择优先级为三星>雷克萨>海力士>江波龙

## 7 参考资料

https://blog.csdn.net/weison_x/article/details/123262060

https://www.cnblogs.com/vlhn/p/7727016.html