# 操作手册：Corsix-th adaption for openEuler2309V1 on Pioneer

---

## 1 目标

在MilkV Pioneer上的openEuler2309V1适配Corsix-th主题医院游戏，并能让游戏真正玩起来，可用于高校开放游园时使用，诱引玩家入坑openEuler2309 RISC-V。

## 2 环境准备

Corsix-th的依赖很多，但本教程仅安装最基本且必要的工具和库，能够正常玩游戏；可选的或非必要的库均不考虑安装。如果有更多追求，如加入声音，请玩家自行补充。

1）安装编译工具

```bash
dnf install autoconf automake cmake git python3 libmpc-devel mpfr-devel gmp-devel gawk bison flex texinfo patchutils gcc gcc-c++ zlib-devel expat-devel curl bc gperf libtool doxygen
```

2）lua：openeuler2309V1支持，版本为5.4.6。但仍然需要安装devel包和lfs包，否则会提示找不到头文件或者链接库。

```bash
dnf install lua-devel lua-filesystem lua-lpeg
```

或者通过luarocks安装LuaFileSystem和LPeg：openeuler2309V1支持，版本为3.9.2。

```bash
dnf install luarocks
```

注意，如果没装好的话，运行游戏会出现如下错误。

![Image](./corsix9.png)

3）SDL：openeuler2309V1支持，版本为2.0.12，安装图像支持库：

```bash
dnf install SDL2*
```

## 3 下载安装主题医院游戏

1）下载源码

```bash
git clone https://github.com/CorsixTH/CorsixTH
```

2）编译安装

进入CorsixTH源码目录，创建构建目录：

```bash
mkdir build
cd build
```

考虑到openeuler2309V1并不支持sdl2-mixer和sdl2-mixer-dev（没声音的原因），所以执行：

```bash
cmake -D WITH_AUDIO:BOOL=OFF -DUSE_SOURCE_DATADIRS=OFF ..
```

![Image](./corsix1.png)

完成检查配置，然后编译：

```bash
make
```

![Image](./corsix2.png)

编译成功
如需安装，可以执行：

```bash
make install
```

游戏被安装到/usr/local/share/corsix-th/，游戏存档目录~/.config/CorsixTH/Saves/。

进入build/CorsixTH目录，执行：./corsix-th, 或可选择直接运行corsix-sh。启动游戏后会看到如下界面：

![Image](./corsix3.png)

虽然游戏正确运行了，但却不能玩，原因是缺少游戏数据文件。

3）获取数据文件

可选择如下两种方式获取游戏数据文件：

--去EA买正版，请前往https://www.ea.com/games/theme/theme-hospital?isLocalized=true 消费30多块钱。买到游戏数据后，将光盘镜像中的HOSP目录复制到本机。

--从corsix主题医院官网 https://th.corsix.org/Demo.zip 下载演示版，或者直接下载本教程为你准备的[Demo数据](../chapter1/Demo.zip)，解压出HOSP目录。

```bash
[root@openeuler-riscv64 ~]# unzip Demo.zip
```

查看HOSP目录：

![Image](./corsix4.png)

## 4 运行游戏

执行corsix-th，或者在桌面环境菜单中选择主题医院的图标，即可开始游戏演示版。由于是演示版，因此只能玩campaign。

![Image](./corsix5.png)

![Image](./corsix6.png)

![Image](./corsix7.png)

![Image](./corsix8.png)

## 5 小结

该游戏的代码仓库中，只有ubuntu和debian的移植适配说明！！！凭什么对openeuler不友好！！！好在现在可以了。

## 6 参考

https://github.com/CorsixTH/CorsixTH/wiki/How-To-Compile

https://github.com/CorsixTH/CorsixTH/wiki/Frequently-Asked-Questions