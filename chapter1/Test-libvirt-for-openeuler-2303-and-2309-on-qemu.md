# 操作手册：Test libvirt for openEuler 2303 and 2309 on QEMU

---

## 1 目标

Libvirt 是一种虚拟化管理工具，由 libvirt API 函数库、libvirtd Daemon 这 2 个关键部分组成，还具有一个默认命令行管理工具 virsh。

![Image](./libvirt01.png)

图中，kvm负责cpu虚拟化+内存虚拟化，实现了cpu和内存的虚拟化，但kvm不能模拟其他设备，且需要cpu支持虚拟化；qemu是模拟IO设备（网卡，磁盘），kvm加上qemu之后就能实现真正意义上服务器虚拟化；通常情况下qemu是可以实现软件虚拟化的，在cpu硬件支持虚拟化的情况下，可以通过kvm实现加速。libvirt则是调用kvm虚拟化技术的接口用于管理的。

本教程验证openeuler riscv的系统环境是否达到运行libvirt的要求，同时分别在openeuler 2303和2309虚拟机上测试libvirt+qemu+kvm。

## 2 系统环境

[openEuler riscv 2303 qemu版本](https://mirror.iscas.ac.cn/openeuler-sig-riscv/openEuler-RISC-V/unstable/openEuler-23.03-V1-riscv64/QEMU/)

[openEuler riscv 2309 qemu版本](http://121.36.84.172/dailybuild/openEuler-23.09-RISC-V/openeuler-2023-10-19-16-53-22/)

考虑到2309版本刚发布，这里顺带提一下系统的配置，其配置基本延续了2303，但是openEuler.repo的内容需要修改，参考如下：

```bash
[OS]
name=OS
baseurl=http://121.36.84.172/dailybuild/openEuler-23.09-RISC-V/openeuler-2023-10-19-16-53-22/OS/riscv64/
enabled=1
gpgcheck=1
gpgkey=http://121.36.84.172/dailybuild/openEuler-23.09-RISC-V/openeuler-2023-10-19-16-53-22/OS/riscv64/RPM-GPG-KEY-openEuler
```

除此之外，本教程中的截图界面为MobaXterm，可使用如下ssh链接到qemu guest主机上。（例如地址为10.0.0.127:12055）

## 3 在openeuler2303上测试

查看逻辑cpu的个数

```bash
[root@openeuler-riscv64 ~]# cat /proc/cpuinfo| grep "processor"| wc -l
4
```

安装libvirt和qemu（据称qemu-kvm在openeuler repo仓中的qemu包中）

```bash
dnf install libvirt virt-manager virt-install bridge-utils
dnf install qemu
```

查看libirt版本

```bash
[root@openeuler-riscv64 ~]# virsh --version
6.2.0
```

查看运行状态，发现因tun没有起来，libvirt被deactive了，加载tun也提示没有找到。根本原因就是kernel没有设置CONFIG_TUN=y，才会导致这种问题。据称openeuler2303这里没相关设置，所以libvirt起不来。

```bash
sudo systemctl -l --no-page status libvirtd
```

![Image](./libvirt02.png)

![Image](./libvirt05.png)

查看图像界面下的virt manager，实际上无法创建虚拟机

![Image](./libvirt03.png)

![Image](./libvirt04.png)

据称openeuler2309的kernel有相关设置为CONFIG_TUN=m，那么让我们在openeuler2309上试试吧。

## 4 在openeuler2309上测试

查看逻辑cpu的个数

```bash
[root@openeuler openeuler]# cat /proc/cpuinfo| grep "processor"| wc -l
4
```

查看cpu是否支持kvm，显示为空

```bash
[root@openeuler openeuler]# grep vmx /proc/cpuinfo

```

查看cpu

```bash
[root@openeuler openeuler]# lscpu
Architecture:          riscv64
  Byte Order:          Little Endian
CPU(s):                4
  On-line CPU(s) list: 0-3
NUMA:
  NUMA node(s):        1
  NUMA node0 CPU(s):   0-3
```

安装libvirt和qemu（由于openeuler2309是base版本，因此没有virt-manager包）

```bash
dnf install libvirt virt-install bridge-utils
dnf install qemu
```

查看libvirt版本及状态，可以看到libvirt已完全启动了。

```bash
[root@openeuler openeuler]# virsh --version
6.2.0
```

![Image](./libvirt07.png)

手动加载tun

```bash
modprobe tun
```

查看tun

```bash
[root@openeuler ~]# lsmod | grep tun
tun                   761856  0
```

重启libvirt服务

```bash
sudo systemctl restart libvirtd
```

手动加载kvm

```bash
modprobe kvm
```

查看kvm，看起来是可用了。

```bash
[root@openeuler openeuler]# lsmod | grep kvm
kvm                  1925120  0
```

![Image](./libvirt06.png)

检查virt-host状态，确保qemu正常状态

```bash
virt-host-validate
```

![Image](./libvirt08.png)

综上，看起来qemu和libvirt已经准备好，且kvm似乎可用，到底是否可用，我们继续测试。

这里，我们将openeuler2309的镜像和bin文件拷贝到host主机的qemu guest虚拟机openeuler2309中。然后在openeuler2309中虚拟一个openeuler2309嵌套虚拟系统。

创建vm-oe2309.xml

```bash
touch vm-oe2309.xml
vim vm-oe2309.xml
```

并向该xml文件中加料

```bash
<domain type='kvm'>
  <name>openEuler 2309</name>
  <memory unit='KiB'>1048576</memory>
  <currentMemory unit='KiB'>1048576</currentMemory>
  <vcpu placement='static'>2</vcpu>
  <os>
    <type arch='riscv64' machine='virt'>hvm</type>
    <loader readonly='yes' secure='yes' type='rom'>/root/openeuler/fw_payload_oe_uboot_2304.bin</loader>
     <boot dev='hd'/>
   </os>
   <clock offset='localtime'/>
   <on_poweroff>destroy</on_poweroff>
   <on_reboot>restart</on_reboot>
   <on_crash>destroy</on_crash>
   <devices>
     <emulator>/usr/libexec/qemu-kvm</emulator>
     <disk type='file' device='disk'>
       <driver name='qemu' type='qcow2'/>
       <source file='/root/openeuler/openEuler-23.09-RISC-V-qemu-riscv64.qcow2'/>
       <target dev='vda' bus='virtio'/>
     </disk>
   </devices>
</domain>
```

使用libvirt定义虚拟机，看起来失败了。

```bash
virsh define vm-oe2309.xml
```

![Image](./libvirt09.png)

这说明kvm实际上并未被支持，即目前的RISC-V指令集H扩展（H (Hypervisor)扩展(RVH): 基础的 H 扩展,通过增加额外的控制寄存器以及二级页表翻译,实现了 CPU 虚拟化、内存虚拟化、Timer 虚拟化。）并未被得到支持。

那我们再看看qemu下的软件虚拟，发现其中也没有qemu-system-riscv的包，这里qemu是个残包，导致无法继续测试工作。

![Image](./libvirt10.png)

## 5 总结

1、对于openeuler2303来说，如果要激活libvirt，建议要更新kernel配置为CONFIG_TUN=y

2、对于openeuler2309来说，没有qemu-system-riscv，又没有H扩展，支持不了qemu-kvm，也就是说没有软件虚拟，也没有硬件加速虚拟，那么这里面放个qemu包也就没有意义了，建议在repo中增加qemu-system-riscv，让libvirt+qemu可以运行起来。

3、最后，如果openeuler无法完美支持libvirt、qemu、kvm的应用，那就只能尝试TeleVM看看。

## 6 参考资料

https://libvirt.org/drvqemu.html#example-domain-xml-config

https://libvirt.org/formatdomain.html#example-configs

https://blog.csdn.net/jmilk/article/details/130326796

https://blog.csdn.net/woailuohui/article/details/84603944

https://blog.csdn.net/weixin_40172997/article/details/87472551

https://www.cnblogs.com/zhongguiyao/p/8075810.html

https://zhuanlan.zhihu.com/p/349878449?utm_id=0
