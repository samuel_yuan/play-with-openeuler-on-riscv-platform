# 《Play with openEuler on RISC-V platform》

---

## 1 课程介绍

- 课程名称：Play with openEuler on RISC-V platform

- 适用范围：计算机专业学员、OpenEuler RISCV SDK软件工程师

- 培训目标：使学员在RISC-V平台上具备熟练部署openEuler软件的能力。

- 课程说明: 随着openEuler硬件生态逐渐丰富，本教程将作为教程[Play with OpenEuler on VisionFive 1&2](https://gitee.com/samuel_yuan/riscv-openeuler-visionfive)的延伸教程，不再仅仅局限于单一硬件，将会支持更多的硬件。与此同时，玩的软件范围将会更广，更复杂，探索RISC-V生态应用的深水区。

## 2 课程内容

- [操作手册：Build EDK2 UEFI firmware to boot openEuler on QEMU](chapter1/Build-edk2-uefi-firmware-to-boot-openeuler-on-qemu-part-a.md)

- [操作手册：Build DPDK 2307 for openEuler on QEMU](chapter1/Build-DPDK-2307-for-openeuler-on-qemu.md)

- [操作手册：Build SPDK 2305 for openEuler on QEMU](chapter1/Build-SPDK-2305-for-openeuler-on-qemu.md)

- [操作手册：Build OvS 3.2 for openEuler on QEMU](chapter1/Build-OvS-3.2-for-openeuler-on-qemu.md)

- [操作手册：Test Ceph for openEuler on QEMU part A](chapter1/Test-ceph-for-openeuler-on-qemu-part-a.md)

- [操作手册：Test Ceph for openEuler on QEMU part B](chapter1/Test-ceph-for-openeuler-on-qemu-part-b.md)

- [操作手册：Test libvirt for openEuler 2303 and 2309 on QEMU](chapter1/Test-libvirt-for-openeuler-2303-and-2309-on-qemu.md)

- [操作手册：Test libvirt for openEuler 2309V1 on QEMU](chapter1/Test-libvirt-for-openeuler-2309V1-on-qemu.md)

- [操作手册：Bring up openEuler 2309V1 on LeapFive Poros by sdcard](chapter1/Bring-up-openeuler-2309V1-on-leapfive-poros-sdcard.md)

- [操作手册：Bring up openEuler 2309V1 on LeapFive Poros by onboard emmc](chapter1/Bring-up-openeuler-2309V1-on-leapfive-poros-onboard-emmc.md)

- [操作手册：Build openEuler2309V1 Qemu Image for UEFI EDK2](chapter1/Build-openEuler2309V1-Qemu-Image-for-UEFI.md)

- [操作手册：Install openEuler 2309V1 on Pioneer and Test SG2042 performance by Geekbench6](chapter1/Install-openEuler-2309V1-on-Pioneer-and-Test-SG2042-performance-by-geekbench6.md)

- [操作手册：Porting openGauss thirty party dependency Masstree on openEuler2309V1](chapter1/Porting-opengauss-thirty-party-dependency-masstree-on-openeuler2309V1.md)

- [操作手册：Corsix-th adaption for openEuler2309V1 on Pioneer](chapter1/corsix-th-adaption-for-openEuler-2309V1-on-Pioneer.md)

- [操作手册：SDLPAL adaption for openEuler2309V1 on Pioneer](chapter1/pal-adaption-for-openEuler-2309V1-on-Pioneer.md)

- [操作手册：Bring up openeuler 2309V1 on leapfive poros by onboard emmc with advanced operation](chapter1/Bring-up-openeuler-2309V1-on-leapfive-poros-onboard-emmc-advance.md)

- [操作手册：Network configuration for openEuler 2309V1 on Pioneer](chapter1/Network-configuration-for-openEuler-2309V1-on-Pioneer.md)

- [操作手册：Test Penglai TEE on openEuler2309V1 Qemu](chapter1/Test-Penglai-TEE-on-openEuler2309V1-Qemu-Image-for-UEFI.md)

- [操作手册：Test secGear on openEuler2309V1 Qemu](chapter1/Test-secGear-on-openEuler2309V1-Qemu-Image-for-UEFI.md)

- [操作手册：Test Kubernetes for openEuler 2309V1 on QEMU](chapter1/Test-kubernetes-for-openeuler-2309V1-on-qemu.md)

- [操作手册：Test NVMe SSD on openEuler2309V1 for Pioneer start-up and SPDK](chapter1/Test-NVMe-SSD-on-openEuler2309V1-for-Pioneer-start-up.md)

- [操作手册：Test vscode on openEuler2403 for qemu](chapter1/Test-vscode-on-openEuler2403-for-qemu.md)

- [操作手册：Enable git-lfs support and image extension for openEuler2403 qemu image](chapter1/Enable-git-lfs-support-and-image-extension-for-openEuler2403-qemu-image.md)

- [操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part A）](chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-a.md)

- [操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part B）](chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-b.md)

- [操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part C）](chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-c.md)

- [操作手册：Test Kubernetes for openEuler 2403 on QEMU（Part D）](chapter1/Test-kubernetes-for-openeuler-2403-on-qemu-d.md)

- [操作手册：Test Ceph Dashboard for openEuler2403 on QEMU](chapter1/Test-ceph-dashboard-for-openeuler2403-on-qemu.md)

- [操作手册：RVV Program and openEuler2403 A](chapter1/RVV-Program-and-openEuler2403-A.md)

- [操作手册：RVV Program and openEuler2403 B](chapter1/RVV-Program-and-openEuler2403-B.md)
